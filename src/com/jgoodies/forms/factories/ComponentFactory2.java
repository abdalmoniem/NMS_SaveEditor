package com.jgoodies.forms.factories;

import javax.swing.JLabel;

public interface ComponentFactory2 extends ComponentFactory {
  JLabel createReadOnlyLabel(String paramString);
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\com\jgoodies\forms\factories\ComponentFactory2.class
 * Java compiler version: 4 (48.0)
 * JD-Core Version:       1.1.3
 */