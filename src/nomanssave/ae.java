package nomanssave;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import nomanssave.structures.e;
import nomanssave.structures.q;

class ae implements ComboBoxModel {
  e aJ = null;
  
  ae(ad paramad) {}
  
  public int getSize() {
    return (ad.a(this.aK) == null) ? 0 : ad.a(this.aK).aZ();
  }
  
  public e k(int paramInt) {
    return ad.a(this.aK).M(paramInt);
  }
  
  public void addListDataListener(ListDataListener paramListDataListener) {}
  
  public void removeListDataListener(ListDataListener paramListDataListener) {}
  
  public void setSelectedItem(Object paramObject) {
    this.aJ = (e)paramObject;
    if (this.aJ == null) {
      ad.b(this.aK).setText("");
      ad.c(this.aK).setText("");
      ad.c(this.aK).setEnabled(false);
    } else {
      q q = this.aJ.be();
      ad.b(this.aK).setText((q == null) ? "" : q.toString());
      ad.c(this.aK).setText(this.aJ.bf());
      ad.c(this.aK).setEnabled(true);
    } 
  }
  
  public Object getSelectedItem() {
    return this.aJ;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ae.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */