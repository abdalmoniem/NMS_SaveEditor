package nomanssave;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import nomanssave.storage.a;
import nomanssave.storage.b;
import nomanssave.storage.e;
import nomanssave.storage.o;
import nomanssave.storage.p;
import nomanssave.structures.b;
import nomanssave.structures.c;
import nomanssave.structures.f;
import nomanssave.structures.g;
import nomanssave.structures.i;
import nomanssave.structures.m;
import nomanssave.structures.o;
import nomanssave.structures.r;
import nomanssave.structures.t;
import nomanssave.structures.u;
import nomanssave.structures.v;
import nomanssave.structures.y;
import nomanssave.util.b;
import nomanssave.util.h;
import nomanssave.util.i;
import nomanssave.util.k;
import nomanssave.util.n;

public class Application {
  public static final String VERSION = "1.6.8";
  
  private static final String z = "https://github.com/goatfungus/NMSSaveEditor";
  
  private static final String A = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
  
  private static Application B;
  
  private static HashMap C = new HashMap<>();
  
  private JFrame D;
  
  private JTabbedPane E;
  
  private JTextField F;
  
  private JComboBox G;
  
  private JComboBox H;
  
  private JTextField I;
  
  private JButton J;
  
  private JButton K;
  
  private JButton L;
  
  private JMenuItem M;
  
  private JMenuItem N;
  
  private JMenuItem O;
  
  private JMenu P;
  
  private aP Q;
  
  private db R;
  
  private dr S;
  
  private bm T;
  
  private bx U;
  
  private dJ V;
  
  private ad W;
  
  private aB X;
  
  private bX Y;
  
  private b Z;
  
  private p[] aa;
  
  private int ab;
  
  private k ac;
  
  private boolean ad;
  
  private e ae = new p(this);
  
  private boolean af = true;
  
  private JFileChooser ag = null;
  
  private JFileChooser ah = null;
  
  private JFileChooser ai;
  
  private b aj = null;
  
  private Runnable ak = null;
  
  public static String a(long paramLong) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a, E MMM d, yyyy");
    return simpleDateFormat.format(new Date(paramLong));
  }
  
  public static String b(long paramLong) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM d, HH:mm");
    return simpleDateFormat.format(new Date(paramLong));
  }
  
  private static String a(String paramString1, String paramString2) {
    if (paramString1 == null)
      return paramString2; 
    StringBuffer stringBuffer = new StringBuffer();
    for (byte b1 = 0; b1 < paramString1.length(); b1++) {
      char c = paramString1.charAt(b1);
      if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_".indexOf(c) >= 0) {
        stringBuffer.append(c);
      } else if (Character.isWhitespace(c)) {
        stringBuffer.append('_');
      } 
    } 
    return (stringBuffer.length() != 0) ? stringBuffer.toString() : paramString2;
  }
  
  public static Application d() {
    return B;
  }
  
  public static void main(String[] paramArrayOfString) {
    boolean bool;
    byte b1 = 0;
    if (paramArrayOfString.length > b1 && paramArrayOfString[b1].equals("-autoupdate")) {
      b1++;
      bool = true;
    } else {
      bool = false;
    } 
    aO.init(!bool);
    n.info("Starting Editor...");
    EventQueue.invokeLater(new A(bool));
  }
  
  public static ImageIcon a(String paramString) {
    BufferedImage bufferedImage = (BufferedImage)C.get(paramString);
    if (bufferedImage == null) {
      InputStream inputStream = Application.class.getResourceAsStream("icons/" + paramString);
      if (inputStream != null)
        try {
          bufferedImage = ImageIO.read(inputStream);
          C.put(paramString, bufferedImage);
        } catch (IOException iOException) {
          n.info("Error loading icon: " + paramString);
        } catch (RuntimeException runtimeException) {
          n.info("Error loading icon: " + paramString);
        }  
    } 
    return (bufferedImage == null) ? null : new ImageIcon(bufferedImage);
  }
  
  public static ImageIcon a(String paramString, int paramInt1, int paramInt2) {
    BufferedImage bufferedImage = (BufferedImage)C.get(paramString);
    if (bufferedImage == null) {
      InputStream inputStream = Application.class.getResourceAsStream("icons/" + paramString);
      if (inputStream != null)
        try {
          bufferedImage = ImageIO.read(inputStream);
          C.put(paramString, bufferedImage);
        } catch (IOException iOException) {
          n.info("Error loading icon: " + paramString);
        } catch (RuntimeException runtimeException) {
          n.info("Error loading icon: " + paramString);
        }  
    } 
    return (bufferedImage == null) ? null : new ImageIcon(bufferedImage.getScaledInstance(paramInt1, paramInt2, 4));
  }
  
  private Application(boolean paramBoolean) {
    String str = aO.getProperty("GameSaveDir");
    this.Z = (str == null) ? null : new b(new File(str), aO.bF);
    if (this.Z != null)
      n.debug("Save Path: " + this.Z.aS().getAbsolutePath()); 
    initialize();
    (new L(this, paramBoolean)).start();
  }
  
  public JFrame e() {
    return this.D;
  }
  
  public void f() {
    this.ad = true;
  }
  
  private JFileChooser g() {
    if (this.ah == null) {
      String str = "Freighter Base Backup File";
      ImageIcon imageIcon = a("UI-FREIGHTERICON.PNG", 16, 16);
      this.ah = new JFileChooser();
      this.ah.setFileSelectionMode(0);
      this.ah.setAcceptAllFileFilterUsed(false);
      this.ah.setFileView(new U(this, imageIcon));
      this.ah.setFileFilter(new V(this));
      this.ah.setDialogTitle("Choose Backup File");
    } 
    return this.ah;
  }
  
  private JFileChooser h() {
    if (this.ag == null) {
      String str = "Planetary Base Backup File";
      ImageIcon imageIcon = a("UI-BASEICON.PNG", 16, 16);
      this.ag = new JFileChooser();
      this.ag.setFileSelectionMode(0);
      this.ag.setAcceptAllFileFilterUsed(false);
      this.ag.setFileView(new W(this, imageIcon));
      this.ag.setFileFilter(new X(this, str));
      this.ag.addChoosableFileFilter(new Y(this));
      this.ag.setDialogTitle("Choose Backup File");
    } 
    return this.ag;
  }
  
  public void a(c paramc) {
    if (!aO.bE.exists())
      aO.bE.mkdir(); 
    JFileChooser jFileChooser = h();
    String str = a(paramc.getName(), "Base");
    jFileChooser.setCurrentDirectory(aO.bE);
    jFileChooser.setSelectedFile(new File(aO.bE, str));
    if (jFileChooser.showSaveDialog(this.D) == 0)
      try {
        File file = jFileChooser.getSelectedFile();
        if (!file.getName().endsWith(".pb3"))
          file = new File(file.getParentFile(), String.valueOf(file.getName()) + ".pb3"); 
        if (file.exists() && JOptionPane.showConfirmDialog(this.D, "Are you sure you want to overwrite this existing backup file?", "Confirm", 2) != 0)
          return; 
        b.a(paramc.bc(), file);
      } catch (RuntimeException runtimeException) {
        n.a("Base backup error", runtimeException);
        c("An error occured during backup.");
      } catch (IOException iOException) {
        n.a("Base backup error", iOException);
        c("An error occured during backup.");
      } catch (GeneralSecurityException generalSecurityException) {
        n.a("Base backup error", generalSecurityException);
        c("An error occured during backup.");
      }  
  }
  
  public boolean b(c paramc) {
    JFileChooser jFileChooser = h();
    jFileChooser.setCurrentDirectory(aO.bE);
    if (jFileChooser.showOpenDialog(this.D) == 0)
      try {
        if (JOptionPane.showConfirmDialog(this.D, "Are you sure you want to overwrite your existing base?", "Confirm", 2) != 0)
          return false; 
        File file = jFileChooser.getSelectedFile();
        b.b(paramc.bc(), file);
        this.ad = true;
        return true;
      } catch (i i) {
        n.a("Base restore error", (Throwable)i);
        c("An error occured during backup.");
      } catch (IOException iOException) {
        n.a("Base restore error", iOException);
        c("An error occured during backup.");
      } catch (GeneralSecurityException generalSecurityException) {
        n.a("Base restore error", generalSecurityException);
        c("An error occured during backup.");
      }  
    return false;
  }
  
  public void a(g paramg) {
    if (!aO.bE.exists())
      aO.bE.mkdir(); 
    JFileChooser jFileChooser = g();
    String str = a(paramg.getName(), "Freighter");
    jFileChooser.setCurrentDirectory(aO.bE);
    jFileChooser.setSelectedFile(new File(aO.bE, str));
    if (jFileChooser.showSaveDialog(this.D) == 0)
      try {
        File file = jFileChooser.getSelectedFile();
        if (!file.getName().endsWith(".fb3"))
          file = new File(file.getParentFile(), String.valueOf(file.getName()) + ".fb3"); 
        if (file.exists() && JOptionPane.showConfirmDialog(this.D, "Are you sure you want to overwrite this existing backup file?", "Confirm", 2) != 0)
          return; 
        b.a(paramg.bc(), file);
      } catch (RuntimeException runtimeException) {
        n.a("Base backup error", runtimeException);
        c("An error occured during backup.");
      } catch (IOException iOException) {
        n.a("Base backup error", iOException);
        c("An error occured during backup.");
      } catch (GeneralSecurityException generalSecurityException) {
        n.a("Base backup error", generalSecurityException);
        c("An error occured during backup.");
      }  
  }
  
  public boolean b(g paramg) {
    JFileChooser jFileChooser = g();
    jFileChooser.setCurrentDirectory(aO.bE);
    if (jFileChooser.showOpenDialog(this.D) == 0)
      try {
        if (JOptionPane.showConfirmDialog(this.D, "Are you sure you want to overwrite your existing base?", "Confirm", 2) != 0)
          return false; 
        File file = jFileChooser.getSelectedFile();
        b.b(paramg.bc(), file);
        this.ad = true;
        return true;
      } catch (i i) {
        n.a("Base restore error", (Throwable)i);
        c("An error occured during backup.");
      } catch (IOException iOException) {
        n.a("Base restore error", iOException);
        c("An error occured during backup.");
      } catch (GeneralSecurityException generalSecurityException) {
        n.a("Base restore error", generalSecurityException);
        c("An error occured during backup.");
      }  
    return false;
  }
  
  private b a(File paramFile) {
    if (this.aj == null || !this.aj.aS().equals(paramFile)) {
      n.info("Loading slots: " + paramFile.getAbsolutePath());
      this.aj = new b(paramFile, aO.bF);
    } 
    return this.aj;
  }
  
  private void i() {
    if (this.ai == null) {
      ImageIcon imageIcon1 = a("UI-FILEICON.PNG", 16, 16);
      ImageIcon imageIcon2 = a("UI-STEAMLOGO.PNG", 16, 16);
      this.ai = new JFileChooser();
      this.ai.setFileSelectionMode(2);
      this.ai.setAcceptAllFileFilterUsed(false);
      this.ai.setFileFilter(new Z(this));
      this.ai.setFileView(new q(this, imageIcon1, imageIcon2));
      this.ai.setDialogTitle("Choose Save Path");
    } 
    if (this.Z != null) {
      this.ai.setCurrentDirectory(this.Z.aS());
    } else {
      File file1 = new File(System.getProperty("user.home"));
      File file2 = new File(file1, "AppData\\Roaming\\HelloGames\\NMS");
      if (file2.exists() && file2.isDirectory())
        file1 = file2; 
      this.ai.setCurrentDirectory(file1);
    } 
    if (this.ai.showOpenDialog(this.D) == 0) {
      String str;
      File file2;
      File file1 = this.ai.getSelectedFile();
      if (file1.isDirectory()) {
        file2 = file1;
        str = null;
      } else {
        file2 = file1.getParentFile();
        str = file1.getName();
      } 
      aO.setProperty("GameSaveDir", file2.getAbsolutePath());
      this.Z = a(file2);
      if (this.Z != null)
        n.debug("Save Path: " + this.Z.aS().getAbsolutePath()); 
      this.F.setText(file2.getAbsolutePath());
      this.G.setEnabled(true);
      this.H.setEnabled(true);
      o o = (str == null) ? null : this.Z.E(str);
      synchronized (this.G) {
        this.af = false;
        this.G.updateUI();
        this.G.setSelectedItem(o);
        if (o == null) {
          this.aa = new p[0];
          this.ab = -1;
        } else {
          this.aa = o.aV();
          this.ab = -1;
          for (byte b1 = 0; b1 < this.aa.length; b1++) {
            if (this.aa[b1].B().equals(str)) {
              this.ab = b1;
              break;
            } 
          } 
          if (this.ab > 0)
            c("The save file you have selected is not the most recent."); 
        } 
        this.H.setSelectedIndex(this.ab);
        this.af = true;
      } 
      k();
    } 
  }
  
  private void j() {
    if (this.Z == null)
      return; 
    if (!this.af)
      return; 
    o o = (o)this.G.getSelectedItem();
    synchronized (this.G) {
      this.af = false;
      if (o == null) {
        this.aa = new p[0];
        this.ab = -1;
      } else {
        this.aa = o.aV();
        this.ab = (this.aa.length > 0) ? 0 : -1;
      } 
      this.af = true;
    } 
    k();
  }
  
  private void e(int paramInt) {
    if (!this.af)
      return; 
    this.ab = paramInt;
    k();
  }
  
  public void b(String paramString) {
    EventQueue.invokeLater(new r(this, paramString));
  }
  
  public void c(String paramString) {
    EventQueue.invokeLater(new s(this, paramString));
  }
  
  private void a(Runnable paramRunnable) {
    if (this.D.isActive()) {
      EventQueue.invokeLater(paramRunnable);
    } else {
      this.ak = paramRunnable;
    } 
  }
  
  private void k() {
    this.H.updateUI();
    this.ad = false;
    this.ac = null;
    if (this.ab < 0) {
      o o = (o)this.G.getSelectedItem();
      this.I.setText("(no file selected)");
      if (o != null) {
        n.info("No current save file found for " + o);
        b("Save file not found for " + o);
      } 
    } else {
      try {
        this.I.setText(a(this.aa[this.ab].lastModified()));
        n.info("Reading save file...");
        n.info("  Slot: " + this.G.getSelectedItem());
        n.info("  Filename: " + this.aa[this.ab].B());
        String str = this.aa[this.ab].C();
        n.info("Parsing JSON...");
        try {
          this.ac = k.b(str, true);
        } catch (i i) {
          n.info("  Error parsing JSON: " + i.getMessage());
          n.info("Attempting file recovery...");
          this.ac = cD.a(this, this.aa[this.ab].B(), str);
          if (this.ac != null)
            this.ad = true; 
        } 
        n.info("Finished.");
        this.ad = this.aa[this.ab] instanceof aa;
      } catch (IOException iOException) {
        n.error("Could not load save file: " + this.aa[this.ab].B(), iOException);
        this.ac = null;
      } 
    } 
    this.E.setSelectedIndex(0);
    k k1;
    if (this.ac == null || (k1 = this.ac.ac("PlayerStateData")) == null) {
      this.J.setEnabled(false);
      this.K.setEnabled(false);
      this.L.setEnabled(false);
      this.M.setEnabled(false);
      this.N.setEnabled(false);
      this.O.setEnabled(false);
      this.P.setEnabled(false);
      this.Q.a((r)null);
      this.R.a(new o[0], (t)null);
      this.S.a(new v[0], (u)null);
      this.T.a((f)null);
      this.U.a(new i[0]);
      this.V.a(new y[0]);
      this.W.a((b)null);
      this.X.a((r)null);
      this.Y.a((r)null);
      this.E.setEnabledAt(1, false);
      this.E.setEnabledAt(2, false);
      this.E.setEnabledAt(3, false);
      this.E.setEnabledAt(4, false);
      this.E.setEnabledAt(5, false);
      this.E.setEnabledAt(6, false);
      this.E.setEnabledAt(7, false);
      this.E.setEnabledAt(8, false);
      this.E.setEnabledAt(9, false);
      if (this.ab >= 0)
        if (this.ac == null) {
          b("There was an error loading the file.");
        } else {
          b("Save file corrupted");
        }  
    } else {
      r r = r.i(k1);
      o[] arrayOfO = o.h(k1);
      t t = t.j(k1);
      v[] arrayOfV = v.l(k1);
      u u = u.k(k1);
      f f = f.b(k1);
      i[] arrayOfI = i.c(k1);
      y[] arrayOfY = y.m(k1);
      b b1 = b.a(k1);
      this.E.setEnabledAt(1, (r != null));
      this.Q.a(r);
      this.E.setEnabledAt(2, (arrayOfO.length > 0));
      this.R.a(arrayOfO, t);
      this.E.setEnabledAt(3, (arrayOfV.length > 0));
      this.S.a(arrayOfV, u);
      this.E.setEnabledAt(4, (f != null));
      this.T.a(f);
      this.E.setEnabledAt(5, (f != null));
      this.U.a(arrayOfI);
      this.E.setEnabledAt(6, (arrayOfY.length > 0));
      this.V.a(arrayOfY);
      this.E.setEnabledAt(7, (b1 != null));
      this.W.a(b1);
      this.E.setEnabledAt(8, (r != null));
      this.X.a(r);
      this.E.setEnabledAt(9, (r != null));
      this.Y.a(r);
      this.J.setEnabled(!(this.aa[this.ab] instanceof aa));
      this.K.setEnabled(true);
      this.L.setEnabled(true);
      this.M.setEnabled(true);
      this.N.setEnabled(true);
      this.O.setEnabled(true);
      this.P.setEnabled(true);
    } 
  }
  
  private void l() {
    k k1;
    if ((k1 = this.ac.ac("PlayerStateData")) != null) {
      h h = k1.d("MultiTools");
      int i = k1.ad("ActiveMultioolIndex");
      if (h != null && i < h.size()) {
        k k2 = h.al(i);
        k1.a("WeaponLayout", k2.ac("Layout").cx());
        k1.a("WeaponInventory", k2.ac("Store").cx());
        k1.a("CurrentWeapon.GenerationSeed", k2.d("Seed").cu());
        n.info("  Updated current multitool data.");
      } 
      if (k1.ah("GrenadeAmmo"))
        n.info("  Removed unused GrenadeAmmo value."); 
    } 
  }
  
  private void m() {
    if (this.ab < 0) {
      b("No save file selected.");
      return;
    } 
    try {
      n.info("Formatting JSON...");
      l();
      String str1 = this.ac.cv();
      o o = (o)this.G.getSelectedItem();
      String str2 = this.aa[this.ab].g(str1);
      synchronized (this.G) {
        this.af = false;
        this.aa = o.aV();
        this.ab = -1;
        for (byte b1 = 0; b1 < this.aa.length; b1++) {
          if (str2.equals(this.aa[b1].B())) {
            this.ab = b1;
            break;
          } 
        } 
        this.af = true;
      } 
      this.ad = false;
      this.H.updateUI();
      if (this.ab < 0) {
        this.I.setText("(no file selected)");
      } else {
        this.I.setText(a(this.aa[this.ab].lastModified()));
      } 
      n.info("Finished.");
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      n.error("Could not write save file: " + this.aa[this.ab].B(), noSuchAlgorithmException);
      b("There was an error saving the file.");
    } catch (IOException iOException) {
      n.error("Could not write save file: " + this.aa[this.ab].B(), iOException);
      b("There was an error saving the file.");
    } 
  }
  
  private void n() {
    o o = (o)this.G.getSelectedItem();
    int i = this.Z.a(o);
    int j = dl.a(this.D, this.Z, i);
    if (j >= 0 && j != i)
      try {
        n.info("Preparing game slot...");
        o o1 = this.Z.K(j);
        n.info("Formatting JSON...");
        l();
        if (i < 0)
          switch (A()[o.x().ordinal()]) {
            case 1:
              this.ac.a("Version", new Integer(4616));
              break;
            case 2:
              this.ac.a("Version", new Integer(5640));
              break;
            case 3:
              this.ac.a("Version", new Integer(5128));
              break;
            case 4:
              this.ac.a("Version", new Integer(6662));
              break;
          }  
        String str1 = this.ac.cv();
        String str2 = o1.H(str1);
        synchronized (this.G) {
          this.af = false;
          this.G.setSelectedItem(o1);
          this.aa = o1.aV();
          this.ab = -1;
          for (byte b1 = 0; b1 < this.aa.length; b1++) {
            if (str2.equals(this.aa[b1].B())) {
              this.ab = b1;
              break;
            } 
          } 
          this.af = true;
        } 
        this.ad = false;
        this.G.updateUI();
        this.H.updateUI();
        if (this.ab < 0) {
          this.I.setText("(no file selected)");
        } else {
          this.I.setText(a(this.aa[this.ab].lastModified()));
        } 
        n.info("Finished.");
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        n.error("Could not write save file", noSuchAlgorithmException);
        b("There was an error saving the file.");
        return;
      } catch (IOException iOException) {
        n.error("Could not write save file", iOException);
        b("There was an error saving the file.");
        return;
      }  
  }
  
  public List f(int paramInt) {
    ArrayList<m> arrayList = new ArrayList();
    r r = this.Q.N();
    if (r != null) {
      m m;
      if ((m = r.F()) != null && m.ad(paramInt))
        arrayList.add(m); 
      if ((m = r.bk()) != null && m.ad(paramInt))
        arrayList.add(m); 
      if ((m = r.ce()) != null && m.ad(paramInt))
        arrayList.add(m); 
    } 
    o[] arrayOfO = this.R.au();
    for (byte b1 = 0; b1 < arrayOfO.length; b1++) {
      m m;
      if ((m = arrayOfO[b1].bk()) != null && m.ad(paramInt))
        arrayList.add(m); 
    } 
    v[] arrayOfV = this.S.av();
    for (byte b2 = 0; b2 < arrayOfV.length; b2++) {
      m m;
      if ((m = arrayOfV[b2].F()) != null && m.ad(paramInt))
        arrayList.add(m); 
      if ((m = arrayOfV[b2].bk()) != null && m.ad(paramInt))
        arrayList.add(m); 
    } 
    f f = this.T.R();
    if (f != null) {
      m m;
      if ((m = f.F()) != null && m.ad(paramInt))
        arrayList.add(m); 
      if ((m = f.bk()) != null && m.ad(paramInt))
        arrayList.add(m); 
    } 
    y[] arrayOfY = this.V.ax();
    for (byte b4 = 0; b4 < arrayOfY.length; b4++) {
      m m;
      if ((m = arrayOfY[b4].F()) != null && m.ad(paramInt))
        arrayList.add(m); 
    } 
    b b3 = this.W.E();
    if (b3 != null) {
      for (byte b5 = 0; b5 < b3.aX(); b5++) {
        m m1;
        if ((m1 = b3.L(b5)) != null && m1.ad(paramInt))
          arrayList.add(m1); 
      } 
      m m;
      if ((m = b3.aY()) != null && m.ad(paramInt))
        arrayList.add(m); 
    } 
    return arrayList;
  }
  
  private void o() {
    k k1 = this.ac.ac("PlayerStateData.UniverseAddress");
    k k2 = k1.ac("GalacticAddress");
    aA aA = new aA(k1.ad("RealityIndex"), k2.ad("VoxelX"), k2.ad("VoxelY"), k2.ad("VoxelZ"), k2.ad("SolarSystemIndex"));
    if ((aA = au.a(this.D, aA)) != null) {
      k1.a("RealityIndex", new Integer(aA.bj));
      k2.a("VoxelX", new Integer(aA.bk));
      k2.a("VoxelY", new Integer(aA.bl));
      k2.a("VoxelZ", new Integer(aA.bm));
      k2.a("PlanetIndex", new Integer(0));
      k2.a("SolarSystemIndex", new Integer(aA.bn));
      this.ac.a("SpawnStateData.LastKnownPlayerState", "InShip");
      this.ad = true;
    } 
  }
  
  private void p() {
    n.info("Starting JSON Editor...");
    l();
    if (cD.a(this, this.aa[this.ab].B(), this.ac)) {
      this.ad = true;
      k k1 = this.ac.ac("PlayerStateData");
      r r = r.i(k1);
      o[] arrayOfO = o.h(k1);
      t t = t.j(k1);
      v[] arrayOfV = v.l(k1);
      u u = u.k(k1);
      f f = f.b(k1);
      i[] arrayOfI = i.c(k1);
      y[] arrayOfY = y.m(k1);
      b b1 = b.a(k1);
      this.E.setEnabledAt(1, (r != null));
      this.Q.a(r);
      this.E.setEnabledAt(2, (arrayOfO.length > 0));
      this.R.a(arrayOfO, t);
      this.E.setEnabledAt(3, (arrayOfV.length > 0));
      this.S.a(arrayOfV, u);
      this.E.setEnabledAt(4, (f != null));
      this.T.a(f);
      this.E.setEnabledAt(5, (f != null));
      this.U.a(arrayOfI);
      this.E.setEnabledAt(6, (arrayOfY.length > 0));
      this.V.a(arrayOfY);
      this.E.setEnabledAt(7, (b1 != null));
      this.W.a(b1);
      this.E.setEnabledAt(8, (r != null));
      this.X.a(r);
      this.E.setEnabledAt(9, (r != null));
      this.Y.a(r);
    } 
  }
  
  private void q() {
    this.Q.q();
    this.R.q();
    this.S.q();
    this.T.q();
    this.V.q();
    this.W.q();
  }
  
  private void r() {
    this.Q.r();
    this.R.r();
    this.S.r();
    this.T.r();
    this.V.r();
    this.W.r();
  }
  
  private void s() {
    this.Q.s();
    this.R.s();
    this.S.s();
    this.T.s();
    this.V.s();
    this.W.s();
  }
  
  private void t() {
    this.R.t();
    this.S.t();
  }
  
  private void u() {
    this.Q.u();
    this.R.u();
    this.S.u();
    this.T.u();
    this.V.u();
    this.W.u();
  }
  
  public void a(m paramm) {
    this.Q.a(paramm);
    this.R.a(paramm);
    this.S.a(paramm);
    this.T.a(paramm);
    this.V.a(paramm);
    this.W.a(paramm);
  }
  
  public void v() {
    this.Y.v();
  }
  
  public void w() {
    this.Y.w();
  }
  
  public h d(String paramString) {
    return this.ac.d(paramString);
  }
  
  public a x() {
    return a.I(this.ac.ad("Version"));
  }
  
  public boolean g(int paramInt) {
    h h = this.ac.d("PlayerStateData.FleetExpeditions");
    for (byte b1 = 0; b1 < h.size(); b1++) {
      h h1 = h.al(b1).d("AllFrigateIndices");
      if (h1.hasValue(new Integer(paramInt)))
        return true; 
    } 
    return false;
  }
  
  public i[] h(int paramInt) {
    h h1 = this.ac.d("PlayerStateData.FleetFrigates");
    h h2 = this.ac.d("PlayerStateData.FleetExpeditions");
    int i;
    for (i = 0; i < h2.size(); i++) {
      h h = h2.al(i).d("AllFrigateIndices");
      if (h.hasValue(new Integer(paramInt))) {
        c("This frigate is currently on a mission and cannot be deleted!");
        return i.a(h1);
      } 
    } 
    if (h1 != null && paramInt < h1.size()) {
      h1.as(paramInt);
      for (byte b1 = 0; b1 < h2.size(); b1++) {
        k k1 = h2.al(b1);
        h h3 = k1.d("ActiveFrigateIndices");
        byte b2;
        for (b2 = 0; b2 < h3.size(); b2++) {
          if ((i = h3.ao(b2)) > paramInt)
            h3.a(b2, Integer.valueOf(i - 1)); 
        } 
        h3 = k1.d("DamagedFrigateIndices");
        for (b2 = 0; b2 < h3.size(); b2++) {
          if ((i = h3.ao(b2)) > paramInt)
            h3.a(b2, Integer.valueOf(i - 1)); 
        } 
        h3 = k1.d("DestroyedFrigateIndices");
        for (b2 = 0; b2 < h3.size(); b2++) {
          if ((i = h3.ao(b2)) > paramInt)
            h3.a(b2, Integer.valueOf(i - 1)); 
        } 
        h3 = k1.d("AllFrigateIndices");
        for (b2 = 0; b2 < h3.size(); b2++) {
          if ((i = h3.ao(b2)) > paramInt)
            h3.a(b2, Integer.valueOf(i - 1)); 
        } 
        h h4 = k1.d("Events");
        for (b2 = 0; b2 < h3.size(); b2++) {
          k k2 = h4.al(b2);
          h3 = k2.d("AffectedFrigateIndices");
          byte b3;
          for (b3 = 0; b3 < h3.size(); b3++) {
            if ((i = h3.ao(b3)) > paramInt)
              h3.a(b3, Integer.valueOf(i - 1)); 
          } 
          h3 = k2.d("RepairingFrigateIndices");
          for (b3 = 0; b3 < h3.size(); b3++) {
            if ((i = h3.ao(b3)) > paramInt)
              h3.a(b3, Integer.valueOf(i - 1)); 
          } 
          h3 = k2.d("AffectedFrigateResponses");
          for (b3 = 0; b3 < h3.size(); b3++) {
            if ((i = h3.ao(b3)) > paramInt)
              h3.a(b3, Integer.valueOf(i - 1)); 
          } 
        } 
      } 
      this.ad = true;
    } 
    return i.a(h1);
  }
  
  public i[] a(int paramInt, String paramString) {
    h h = this.ac.d("PlayerStateData.FleetFrigates");
    if (h != null && paramInt < h.size()) {
      k k1 = h.al(paramInt).cx();
      k1.d("ResourceSeed").a(1, paramString);
      k1.a("CustomName", "");
      h.a(k1);
      this.ad = true;
    } 
    return i.a(h);
  }
  
  private void y() {
    int i = this.ac.ad("PlayerStateData.TotalPlayTime");
    byte b1 = 0;
    h h1 = this.ac.d("PlayerStateData.PersistentPlayerBases");
    int j;
    for (j = 0; j < h1.size(); j++) {
      h h = h1.al(j).d("Objects");
      for (byte b3 = 0; b3 < h.size(); b3++) {
        String str = h.al(b3).getValueAsString("ObjectID");
        if ("^PLANTER".equals(str)) {
          b1++;
        } else if ("^PLANTERMEGA".equals(str)) {
          b1++;
        } 
      } 
    } 
    h h2 = this.ac.d("PlayerStateData.MaintenanceInteractions");
    for (byte b2 = 0; b2 < h2.size(); b2++) {
      k k1 = h2.al(b2);
      h h = k1.d("InventoryContainer.Slots");
      for (byte b3 = 0; b3 < h.size(); b3++) {
        k k2 = h.al(b3);
        if ("^MAINT_FARM5".equals(k2.getValueAsString("Id"))) {
          if ((j = k2.ad("MaxAmount")) > 0 && k2.ad("Amount") < j)
            k2.a("Amount", new Integer(j)); 
          k1.a("LastUpdateTimestamp", new Integer(i));
          this.ad = true;
          b1--;
        } 
      } 
    } 
  }
  
  private void initialize() {
    this.D = new JFrame();
    this.D.setTitle("No Man's Sky Save Editor - 1.6.8 (BEYOND)");
    Rectangle rectangle = new Rectangle(100, 100, 1100, 720);
    Point point = aO.l("MainFrame.Location");
    if (point != null)
      rectangle.setLocation(point); 
    this.D.setBounds(rectangle);
    this.D.setDefaultCloseOperation(3);
    this.D.addWindowListener(new t(this));
    this.D.addComponentListener(new u(this));
    this.E = new JTabbedPane(1);
    this.D.getContentPane().add(this.E, "Center");
    bj bj = new bj(new int[] { aO.bK, aO.bL });
    this.E.addTab("Main", null, bj, null);
    bj.n("File Details");
    this.F = new JTextField();
    this.F.setText((this.Z == null) ? "(no path selected)" : this.Z.aS().getAbsolutePath());
    this.F.setBorder((Border)null);
    this.F.setBackground((Color)null);
    this.F.setEditable(false);
    bj.a("Save Path", this.F, 2);
    this.G = new JComboBox();
    this.G.setModel(new v(this));
    this.G.setEnabled((this.Z != null));
    bj.a("Game Slot", this.G);
    this.ab = -1;
    this.aa = new p[0];
    this.H = new JComboBox();
    this.H.setEditable(false);
    this.H.setModel(new w(this));
    this.H.setEnabled((this.Z != null));
    bj.a("Save File", this.H);
    this.I = new JTextField();
    this.I.setText("(no file selected)");
    this.I.setBorder((Border)null);
    this.I.setBackground((Color)null);
    this.I.setEditable(false);
    bj.a("Modified", this.I, 2);
    bj.Q();
    JPanel jPanel = new JPanel();
    jPanel.setLayout(new FlowLayout(0, 0, 0));
    this.J = new JButton("Reload");
    this.J.setEnabled(false);
    this.J.addActionListener(new x(this));
    jPanel.add(this.J);
    this.K = new JButton("Save Changes");
    this.K.setEnabled(false);
    this.K.addActionListener(new y(this));
    jPanel.add(this.K);
    this.L = new JButton("Save As");
    this.L.setEnabled(false);
    this.L.addActionListener(new z(this));
    jPanel.add(this.L);
    bj.a((String)null, jPanel, 2);
    this.Q = new aP(this);
    this.E.addTab("Exosuit", null, this.Q, null);
    this.E.setEnabledAt(1, false);
    this.R = new db(this);
    this.E.addTab("Multitool", null, this.R, null);
    this.E.setEnabledAt(2, false);
    this.S = new dr(this);
    this.E.addTab("Ships", null, this.S, null);
    this.E.setEnabledAt(3, false);
    this.T = new bm(this);
    this.E.addTab("Freighter", null, this.T, null);
    this.E.setEnabledAt(4, false);
    this.U = new bx(this);
    this.E.addTab("Frigates", null, this.U, null);
    this.E.setEnabledAt(5, false);
    this.V = new dJ(this);
    this.E.addTab("Vehicles", null, this.V, null);
    this.E.setEnabledAt(6, false);
    this.W = new ad(this);
    this.E.addTab("Bases & Storage", null, this.W, null);
    this.E.setEnabledAt(7, false);
    this.X = new aB(this);
    this.E.addTab("Discovery", null, this.X, null);
    this.E.setEnabledAt(8, false);
    this.Y = new bX(this);
    this.E.addTab("Milestones / Reputation", null, this.Y, null);
    this.E.setEnabledAt(9, false);
    this.E.addChangeListener(new B(this));
    JMenuBar jMenuBar = new JMenuBar();
    this.D.setJMenuBar(jMenuBar);
    JMenu jMenu1 = new JMenu("File");
    jMenuBar.add(jMenu1);
    JMenuItem jMenuItem1 = new JMenuItem("Open File/Path");
    jMenuItem1.setAccelerator(KeyStroke.getKeyStroke(79, 2));
    jMenuItem1.addActionListener(new C(this));
    jMenu1.add(jMenuItem1);
    this.M = new JMenuItem("Reload File");
    this.M.setEnabled(false);
    this.M.setAccelerator(KeyStroke.getKeyStroke(82, 2));
    this.M.addActionListener(new D(this));
    jMenu1.add(this.M);
    this.N = new JMenuItem("Save File");
    this.N.setEnabled(false);
    this.N.setAccelerator(KeyStroke.getKeyStroke(83, 2));
    this.N.addActionListener(new E(this));
    jMenu1.add(this.N);
    this.O = new JMenuItem("Save File As");
    this.O.setEnabled(false);
    this.O.addActionListener(new F(this));
    jMenu1.add(this.O);
    jMenu1.addSeparator();
    JMenuItem jMenuItem2 = new JMenuItem("Exit");
    jMenuItem2.addActionListener(new G(this));
    jMenu1.add(jMenuItem2);
    this.P = new JMenu("Edit");
    this.P.setEnabled(false);
    jMenuBar.add(this.P);
    JMenuItem jMenuItem3 = new JMenuItem("Edit Raw JSON");
    jMenuItem3.addActionListener(new H(this));
    this.P.add(jMenuItem3);
    JMenuItem jMenuItem4 = new JMenuItem("Coordinate Viewer");
    jMenuItem4.addActionListener(new I(this));
    this.P.add(jMenuItem4);
    JCheckBoxMenuItem jCheckBoxMenuItem = new JCheckBoxMenuItem("Test Mode");
    jCheckBoxMenuItem.setSelected(dH.aw());
    jCheckBoxMenuItem.addActionListener(new J(this, jCheckBoxMenuItem));
    this.P.add(jCheckBoxMenuItem);
    this.P.addSeparator();
    JMenuItem jMenuItem5 = new JMenuItem("Recharge All Technology");
    jMenuItem5.addActionListener(new K(this));
    this.P.add(jMenuItem5);
    JMenuItem jMenuItem6 = new JMenuItem("Refill All Stacks");
    jMenuItem6.addActionListener(new N(this));
    this.P.add(jMenuItem6);
    JMenuItem jMenuItem7 = new JMenuItem("Recharge Base Planters");
    jMenuItem7.addActionListener(new O(this));
    this.P.add(jMenuItem7);
    JMenuItem jMenuItem8 = new JMenuItem("Expand All Inventories");
    jMenuItem8.addActionListener(new P(this));
    this.P.add(jMenuItem8);
    JMenuItem jMenuItem9 = new JMenuItem("Enable All Slots");
    jMenuItem9.addActionListener(new Q(this));
    this.P.add(jMenuItem9);
    JMenuItem jMenuItem10 = new JMenuItem("Repair All Slots / Technology");
    jMenuItem10.addActionListener(new R(this));
    this.P.add(jMenuItem10);
    jMenuBar.add(Box.createHorizontalGlue());
    JMenu jMenu2 = new JMenu("Help");
    jMenuBar.add(jMenu2);
    JMenuItem jMenuItem11 = new JMenuItem("About");
    jMenuItem11.addActionListener(new S(this));
    jMenu2.add(jMenuItem11);
    if (this.Z == null)
      EventQueue.invokeLater(new T(this)); 
    this.D.pack();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\Application.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */