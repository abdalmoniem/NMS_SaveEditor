package nomanssave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import nomanssave.structures.v;
import nomanssave.util.n;

class dx implements ActionListener {
  dx(dr paramdr, Application paramApplication) {}
  
  public void actionPerformed(ActionEvent paramActionEvent) {
    int i = dr.o(this.gr).getSelectedIndex();
    if (i < 0 || i >= (dr.a(this.gr)).length)
      return; 
    if ((dr.a(this.gr)).length == 1) {
      this.aL.c("You cannot delete the only ship you have!");
      return;
    } 
    if (JOptionPane.showConfirmDialog(this.gr, "Are you sure you want to delete this ship?\nAll items and technology in the ship inventory will be lost!", "Delete", 2) != 0)
      return; 
    v[] arrayOfV = new v[(dr.a(this.gr)).length - 1];
    System.arraycopy(dr.a(this.gr), 0, arrayOfV, 0, i);
    int j = dr.a(this.gr)[i].getIndex();
    n.info("Deleting ship: " + j);
    dr.a(this.gr)[i].bX();
    System.arraycopy(dr.a(this.gr), i + 1, arrayOfV, i, (dr.a(this.gr)).length - i - 1);
    dr.a(this.gr, arrayOfV);
    if (dr.n(this.gr) != null && dr.n(this.gr).cm() == j) {
      j = dr.a(this.gr)[0].getIndex();
      dr.n(this.gr).ak(j);
      n.info("Setting primary ship: " + j);
    } 
    dr.o(this.gr).setSelectedIndex(0);
    dr.o(this.gr).updateUI();
    this.aL.f();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dx.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */