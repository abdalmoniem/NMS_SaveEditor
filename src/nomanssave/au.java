package nomanssave;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.util.Arrays;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;
import nomanssave.util.g;

public class au extends JDialog {
  private static final List aY = Arrays.asList(new String[] { 
        "Euclid", "Hilbert Dimension", "Calypso", "Hesperius Dimension", "Hyades", "Ickjamatew", "Budullangr", "Kikolgallr", "Eltiensleen", "Eissentam", 
        "Elkupalos", "Aptarkaba", "Ontiniangp", "Odiwagiri", "Ogtialabi", "Muhacksonto", "Hitonskyer", "Rerasmutul", "Isdoraijung", "Doctinawyra", 
        "Loychazinq", "Zukasizawa", "Ekwathore", "Yeberhahne", "Twerbetek", "Sivarates", "Eajerandal", "Aldukesci", "Wotyarogii", "Sudzerbal", 
        "Maupenzhay", "Sugueziume", "Brogoweldian", "Ehbogdenbu", "Ijsenufryos", "Nipikulha", "Autsurabin", "Lusontrygiamh", "Rewmanawa", "Ethiophodhe", 
        "Urastrykle", "Xobeurindj", "Oniijialdu", "Wucetosucc", "Ebyeloofdud", "Odyavanta", "Milekistri", "Waferganh", "Agnusopwit", "Teyaypilny", 
        "Zalienkosm", "Ladgudiraf", "Mushonponte", "Amsentisz", "Fladiselm", "Laanawemb", "Ilkerloor", "Davanossi", "Ploehrliou", "Corpinyaya", 
        "Leckandmeram", "Quulngais", "Nokokipsechl", "Rinblodesa", "Loydporpen", "Ibtrevskip", "Elkowaldb", "Heholhofsko", "Yebrilowisod", "Husalvangewi", 
        "Ovna'uesed", "Bahibusey", "Nuybeliaure", "Doshawchuc", "Ruckinarkh", "Thorettac", "Nuponoparau", "Moglaschil", "Uiweupose", "Nasmilete", 
        "Ekdaluskin", "Hakapanasy", "Dimonimba", "Cajaccari", "Olonerovo", "Umlanswick", "Henayliszm", "Utzenmate", "Umirpaiya", "Paholiang", 
        "Iaereznika", "Yudukagath", "Boealalosnj", "Yaevarcko", "Coellosipp", "Wayndohalou", "Smoduraykl", "Apmaneessu", "Hicanpaav", "Akvasanta", 
        "Tuychelisaor", "Rivskimbe", "Daksanquix", "Kissonlin", "Aediabiel", "Ulosaginyik", "Roclaytonycar", "Kichiaroa", "Irceauffey", "Nudquathsenfe", 
        "Getaizakaal", "Hansolmien", "Bloytisagra", "Ladsenlay", "Luyugoslasr", "Ubredhatk", "Cidoniana", "Jasinessa", "Torweierf", "Saffneckm", 
        "Thnistner", "Dotusingg", "Luleukous", "Jelmandan", "Otimanaso", "Enjaxusanto", "Sezviktorew", "Zikehpm", "Bephembah", "Broomerrai", 
        "Meximicka", "Venessika", "Gaiteseling", "Zosakasiro", "Drajayanes", "Ooibekuar", "Urckiansi", "Dozivadido", "Emiekereks", "Meykinunukur", 
        "Kimycuristh", "Roansfien", "Isgarmeso", "Daitibeli", "Gucuttarik", "Enlaythie", "Drewweste", "Akbulkabi", "Homskiw", "Zavainlani", 
        "Jewijkmas", "Itlhotagra", "Podalicess", "Hiviusauer", "Halsebenk", "Puikitoac", "Gaybakuaria", "Grbodubhe", "Rycempler", "Indjalala", 
        "Fontenikk", "Pasycihelwhee", "Ikbaksmit", "Telicianses", "Oyleyzhan", "Uagerosat", "Impoxectin", "Twoodmand", "Hilfsesorbs", "Ezdaranit", 
        "Wiensanshe", "Ewheelonc", "Litzmantufa", "Emarmatosi", "Mufimbomacvi", "Wongquarum", "Hapirajua", "Igbinduina", "Wepaitvas", "Sthatigudi", 
        "Yekathsebehn", "Ebedeagurst", "Nolisonia", "Ulexovitab", "Iodhinxois", "Irroswitzs", "Bifredait", "Beiraghedwe", "Yeonatlak", "Cugnatachh", 
        "Nozoryenki", "Ebralduri", "Evcickcandj", "Ziybosswin", "Heperclait", "Sugiuniam", "Aaseertush", "Uglyestemaa", "Horeroedsh", "Drundemiso", 
        "Ityanianat", "Purneyrine", "Dokiessmat", "Nupiacheh", "Dihewsonj", "Rudrailhik", "Tweretnort", "Snatreetze", "Iwunddaracos", "Digarlewena", 
        "Erquagsta", "Logovoloin", "Boyaghosganh", "Kuolungau", "Pehneldept", "Yevettiiqidcon", "Sahliacabru", "Noggalterpor", "Chmageaki", "Veticueca", 
        "Vittesbursul", "Nootanore", "Innebdjerah", "Kisvarcini", "Cuzcogipper", "Pamanhermonsu", "Brotoghek", "Mibittara", "Huruahili", "Raldwicarn", 
        "Ezdartlic", "Badesclema", "Isenkeyan", "Iadoitesu", "Yagrovoisi", "Ewcomechio", "Inunnunnoda", "Dischiutun", "Yuwarugha", "Ialmendra", 
        "Reponudrle", "Rinjanagrbo", "Zeziceloh", "Oeileutasc", "Zicniijinis", "Dugnowarilda", "Neuxoisan", "Ilmenhorn", "Rukwatsuku", "Nepitzaspru", 
        "Chcehoemig", "Haffneyrin", "Uliciawai", "Tuhgrespod", "Iousongola", "Odyalutai", "Yilsrussimil" });
  
  private JComboBox aZ;
  
  private JTextField ba;
  
  private JTextField bb;
  
  private JLabel[] bc;
  
  private JTextField d;
  
  private ImageIcon[] bd;
  
  private aA be = null;
  
  private aA bf = null;
  
  private static au bg = null;
  
  private au(Frame paramFrame) {
    super(paramFrame);
    setResizable(false);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setTitle("Coordinate Viewer");
    setModal(true);
    JPanel jPanel1 = new JPanel();
    setContentPane(jPanel1);
    jPanel1.setLayout(new BorderLayout(0, 0));
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("100px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("100px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC }, new RowSpec[] { 
            FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, RowSpec.decode("bottom:10px"), FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, 
            FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC }));
    JLabel jLabel1 = new JLabel("Search:");
    jLabel1.setFont(aO.bH);
    jPanel2.add(jLabel1, "2, 2, left, center");
    this.d = new JTextField();
    jPanel2.add(this.d, "4, 2, fill, default");
    JButton jButton1 = new JButton("Search");
    jButton1.addActionListener(new av(this));
    jPanel2.add(jButton1, "6, 2, fill, fill");
    JLabel jLabel2 = new JLabel("Coordinate Location:");
    jLabel2.setFont(aO.bH);
    jPanel2.add(jLabel2, "2, 6, 5, 1, left, center");
    JLabel jLabel3 = new JLabel("Galaxy:");
    jPanel2.add(jLabel3, "2, 8, left, center");
    this.aZ = new JComboBox();
    this.aZ.setModel(new aw(this));
    jPanel2.add(this.aZ, "4, 8, 3, 1, fill, default");
    JLabel jLabel4 = new JLabel("Galactic Addr:");
    jPanel2.add(jLabel4, "2, 10, left, center");
    this.ba = new JTextField();
    this.ba.setEditable(false);
    jPanel2.add(this.ba, "4, 10, 3, 1, fill, default");
    JLabel jLabel5 = new JLabel("Portal Addr:");
    jPanel2.add(jLabel5, "2, 12, left, center");
    this.bb = new JTextField();
    this.bb.setEditable(false);
    jPanel2.add(this.bb, "4, 12, 3, 1, fill, default");
    JPanel jPanel3 = new JPanel();
    jPanel3.setBackground(Color.GRAY);
    jPanel3.setBorder(new LineBorder(Color.DARK_GRAY));
    jPanel3.setLayout(new FlowLayout(1, 5, 5));
    jPanel3.setMinimumSize(new Dimension(449, 42));
    this.bd = new ImageIcon[16];
    byte b;
    for (b = 0; b < 16; b++)
      this.bd[b] = Application.a("UI-GLYPH" + (b + 1) + ".PNG", 32, 32); 
    this.bc = new JLabel[12];
    for (b = 0; b < 12; b++) {
      this.bc[b] = new JLabel(this.bd[0]);
      jPanel3.add(this.bc[b]);
    } 
    jPanel2.add(jPanel3, "2, 14, 5, 1, fill, fill");
    jPanel1.add(jPanel2);
    JPanel jPanel4 = new JPanel();
    jPanel4.setLayout(new FlowLayout(2));
    jPanel1.add(jPanel4, "South");
    JButton jButton2 = new JButton("Save / Warp");
    jButton2.addActionListener(new ax(this));
    jPanel4.add(jButton2);
    getRootPane().setDefaultButton(jButton2);
    JButton jButton3 = new JButton("Cancel");
    jButton3.addActionListener(new ay(this));
    jPanel4.add(jButton3);
    getRootPane().registerKeyboardAction(new az(this), KeyStroke.getKeyStroke(27, 0), 2);
    pack();
  }
  
  private void a(g paramg) {
    this.be = new aA(this.aZ.getSelectedIndex(), paramg, null);
    this.ba.setText(aA.b(this.be));
    long l = aA.c(this.be);
    this.bb.setText(g.f(l));
    for (byte b = 0; b < 12; b++) {
      int i = (int)(l >> (11 - b) * 4) & 0xF;
      this.bc[b].setIcon(this.bd[i]);
    } 
  }
  
  private aA a(aA paramaA) {
    this.be = paramaA;
    this.aZ.setSelectedIndex((paramaA.bj >= aY.size()) ? -1 : paramaA.bj);
    this.aZ.updateUI();
    this.ba.setText(aA.b(paramaA));
    long l = aA.c(paramaA);
    this.bb.setText(g.f(l));
    for (byte b = 0; b < 12; b++) {
      int i = (int)(l >> (11 - b) * 4) & 0xF;
      this.bc[b].setIcon(this.bd[i]);
    } 
    this.d.setText("");
    this.bf = null;
    setLocationRelativeTo(getParent());
    setVisible(true);
    return this.bf;
  }
  
  public static aA a(Container paramContainer, aA paramaA) {
    if (bg == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      bg = new au(frame);
    } 
    return bg.a(paramaA);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\au.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */