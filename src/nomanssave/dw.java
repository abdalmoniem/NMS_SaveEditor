package nomanssave;

import nomanssave.structures.v;
import nomanssave.util.n;
import nomanssave.util.p;

class dw extends ab {
  dw(dr paramdr, Application paramApplication) {}
  
  protected String i(String paramString) {
    v v = (v)dr.o(this.gr).getSelectedItem();
    if (v == null)
      return ""; 
    double d = v.bm();
    try {
      double d1 = p.a(paramString, 0.0D, 1000.0D);
      if (d1 != d) {
        n.info("Setting ship base hyperdrive: " + d1);
        v.a(d1);
        this.aL.f();
      } 
      return Double.toString(d1);
    } catch (RuntimeException runtimeException) {
      return Double.toString(d);
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dw.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */