package nomanssave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import nomanssave.structures.o;
import nomanssave.util.n;

class dk implements ActionListener {
  dk(db paramdb, Application paramApplication) {}
  
  public void actionPerformed(ActionEvent paramActionEvent) {
    int i = db.i(this.fT).getSelectedIndex();
    if (i < 0 || i >= (db.a(this.fT)).length)
      return; 
    if ((db.a(this.fT)).length == 1) {
      this.aL.c("You cannot delete the only multitool you have!");
      return;
    } 
    if (JOptionPane.showConfirmDialog(this.fT, "Are you sure you want to delete this multitool?\nAll technology in the multitool will be lost!", "Delete", 2) != 0)
      return; 
    o[] arrayOfO = new o[(db.a(this.fT)).length - 1];
    System.arraycopy(db.a(this.fT), 0, arrayOfO, 0, i);
    int j = db.a(this.fT)[i].getIndex();
    n.info("Deleting multitool: " + j);
    db.a(this.fT)[i].bX();
    System.arraycopy(db.a(this.fT), i + 1, arrayOfO, i, (db.a(this.fT)).length - i - 1);
    db.a(this.fT, arrayOfO);
    if (db.j(this.fT) != null && db.j(this.fT).cl() == j) {
      j = db.a(this.fT)[0].getIndex();
      db.j(this.fT).aj(j);
      n.info("Setting primary multitool: " + j);
    } 
    db.i(this.fT).setSelectedIndex(0);
    db.i(this.fT).updateUI();
    this.aL.f();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dk.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */