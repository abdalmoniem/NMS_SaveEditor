package nomanssave;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import nomanssave.structures.m;
import nomanssave.structures.o;
import nomanssave.structures.t;

public class db extends dG {
  private static final double fF = 1000.0D;
  
  private static final double fG = 1000.0D;
  
  private static final double fH = 1000.0D;
  
  private final Application cQ;
  
  private JComboBox fI;
  
  private ab fJ;
  
  private JComboBox fK;
  
  private ab fL;
  
  private ab fM;
  
  private ab fN;
  
  private ab fO;
  
  private JButton cS;
  
  private ci fP;
  
  private o[] fQ;
  
  private t fR;
  
  db(Application paramApplication) {
    this.cQ = paramApplication;
    this.fI = new JComboBox();
    this.fI.setModel(new dc(this));
    a("Multitool", true, this.fI);
    this.fJ = new de(this, paramApplication);
    a("Name", this.fJ);
    this.fK = new JComboBox();
    this.fK.setModel(new df(this, paramApplication));
    a("Class", this.fK);
    this.fL = new dg(this, paramApplication);
    a("Seed", this.fL);
    n("Base Stats");
    this.fM = new dh(this, paramApplication);
    a("Damage", this.fM);
    this.fN = new di(this, paramApplication);
    a("Mining", this.fN);
    this.fO = new dj(this, paramApplication);
    a("Scan", this.fO);
    Q();
    JPanel jPanel = new JPanel();
    this.cS = new JButton("Delete Multitool");
    this.cS.addActionListener(new dk(this, paramApplication));
    jPanel.add(this.cS);
    a(jPanel);
    this.fP = new ci(paramApplication);
    b(this.fP);
  }
  
  void q() {
    this.fP.q();
  }
  
  void r() {
    this.fP.r();
  }
  
  void s() {
    this.fP.s();
  }
  
  void t() {
    this.fP.t();
  }
  
  void u() {
    this.fP.u();
  }
  
  void a(m paramm) {
    this.fP.a(paramm);
  }
  
  o[] au() {
    return this.fQ;
  }
  
  void a(o[] paramArrayOfo, t paramt) {
    this.fQ = paramArrayOfo;
    this.fR = paramt;
    if (paramArrayOfo.length == 0) {
      this.fI.setSelectedIndex(-1);
    } else {
      byte b = (paramt == null) ? 0 : paramt.cl();
      if (b >= paramArrayOfo.length)
        b = 0; 
      this.fI.setSelectedIndex(b);
    } 
    this.fI.updateUI();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */