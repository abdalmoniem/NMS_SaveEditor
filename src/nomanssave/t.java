package nomanssave;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

class t extends WindowAdapter {
  t(Application paramApplication) {}
  
  public void windowClosing(WindowEvent paramWindowEvent) {
    if (Application.i(this.am)) {
      int i = JOptionPane.showConfirmDialog(Application.e(this.am), "Save data before closing?", "Save", 0);
      if (i == 0)
        Application.j(this.am); 
    } 
    if (aO.L())
      aO.M(); 
    Application.e(this.am).dispose();
  }
  
  public void windowDeactivated(WindowEvent paramWindowEvent) {
    if (Application.k(this.am) != null)
      Application.k(this.am).a(Application.l(this.am)); 
  }
  
  public void windowActivated(WindowEvent paramWindowEvent) {
    if (Application.k(this.am) != null)
      Application.k(this.am).a(null); 
    if (Application.m(this.am) != null)
      try {
        Application.m(this.am).run();
      } finally {
        Application.b(this.am, (Runnable)null);
      }  
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\t.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */