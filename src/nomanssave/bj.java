package nomanssave;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class bj extends JPanel {
  private final FormLayout cy = new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC }, new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC });
  
  bj() {
    this(new int[] { aO.bK });
  }
  
  bj(int[] paramArrayOfint) {
    for (byte b = 0; b < paramArrayOfint.length; b++) {
      if (paramArrayOfint[b] > 0) {
        this.cy.appendColumn(ColumnSpec.decode(String.valueOf(paramArrayOfint[b]) + "px"));
      } else {
        this.cy.appendColumn(ColumnSpec.decode("default:grow"));
      } 
      this.cy.appendColumn(FormFactory.LABEL_COMPONENT_GAP_COLSPEC);
    } 
    setLayout((LayoutManager)this.cy);
  }
  
  void n(String paramString) {
    a(paramString, (ImageIcon)null);
  }
  
  void a(String paramString, ImageIcon paramImageIcon) {
    if (this.cy.getRowCount() == 1) {
      this.cy.appendRow(FormFactory.DEFAULT_ROWSPEC);
      this.cy.appendRow(FormFactory.LINE_GAP_ROWSPEC);
    } else {
      int j = this.cy.getRowCount();
      this.cy.insertRow(j, FormFactory.DEFAULT_ROWSPEC);
      this.cy.insertRow(j, RowSpec.decode("bottom:25px"));
    } 
    int i = this.cy.getColumnCount() - 2;
    JLabel jLabel = new JLabel(paramString);
    jLabel.setFont(aO.bH);
    if (paramImageIcon == null) {
      add(jLabel, "2, " + (this.cy.getRowCount() - 1) + ", " + i + ", 1, left, default");
    } else {
      JPanel jPanel = new JPanel();
      jPanel.setLayout(new FlowLayout(0, 0, 0));
      jPanel.add(new JLabel(paramImageIcon));
      jPanel.add(jLabel);
      add(jPanel, "2, " + (this.cy.getRowCount() - 1) + ", " + i + ", 1, left, default");
    } 
  }
  
  void Q() {
    this.cy.appendRow(RowSpec.decode("bottom:10px"));
    this.cy.appendRow(FormFactory.LINE_GAP_ROWSPEC);
  }
  
  void a(String paramString, JComponent paramJComponent) {
    a(paramString, false, paramJComponent, 1);
  }
  
  void a(String paramString, JComponent paramJComponent, int paramInt) {
    a(paramString, false, paramJComponent, paramInt);
  }
  
  void a(String paramString, boolean paramBoolean, JComponent paramJComponent) {
    a(paramString, paramBoolean, paramJComponent, 1);
  }
  
  void a(String paramString, boolean paramBoolean, JComponent paramJComponent, int paramInt) {
    paramInt = paramInt * 2 - 1;
    this.cy.appendRow(FormFactory.DEFAULT_ROWSPEC);
    this.cy.appendRow(FormFactory.LINE_GAP_ROWSPEC);
    int i = this.cy.getRowCount() - 1;
    if (paramString != null) {
      JLabel jLabel = new JLabel(String.valueOf(paramString) + ":");
      jLabel.setFont(paramBoolean ? aO.bH : aO.bG);
      add(jLabel, "2, " + i + ", left, default");
    } 
    add(paramJComponent, "4, " + i + ", " + paramInt + ", 1, fill, default");
  }
  
  void a(String paramString, ab paramab) {
    JPanel jPanel1 = new JPanel();
    jPanel1.setLayout(new BorderLayout(0, 0));
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout(new FlowLayout(2, 0, 0));
    JButton jButton = new JButton("Generate");
    jButton.setEnabled(paramab.isEnabled());
    jButton.addActionListener(new bk(this, paramab));
    paramab.addPropertyChangeListener("enabled", new bl(this, jButton, paramab));
    jPanel2.add(jButton);
    jPanel1.add(paramab, "Center");
    jPanel1.add(jPanel2, "South");
    a(paramString, jPanel1);
  }
  
  void a(JComponent paramJComponent) {
    this.cy.appendRow(FormFactory.DEFAULT_ROWSPEC);
    this.cy.appendRow(FormFactory.LINE_GAP_ROWSPEC);
    int i = this.cy.getColumnCount() - 2;
    int j = this.cy.getRowCount() - 1;
    add(paramJComponent, "2, " + j + ", " + i + ", 1, fill, default");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\bj.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */