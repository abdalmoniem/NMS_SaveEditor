package nomanssave;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import nomanssave.structures.n;

class cp extends MouseAdapter {
  cp(cm paramcm, int paramInt1, int paramInt2) {}
  
  public void mouseReleased(MouseEvent paramMouseEvent) {
    if (!ci.b(cm.i(this.eq)).j(this.er, this.es) || ci.b(cm.i(this.eq)).m(this.er, this.es))
      return; 
    int i = this.er + (int)Math.floor(paramMouseEvent.getX() / aO.bO);
    int j = this.es + (int)Math.floor(paramMouseEvent.getY() / aO.bO);
    if (i < 0 || i >= ci.b(cm.i(this.eq)).getWidth())
      return; 
    if (j < 0 || j >= ci.b(cm.i(this.eq)).getHeight())
      return; 
    if (i == this.er && j == this.es)
      return; 
    cm cm1 = ci.a(cm.i(this.eq), i, j);
    if (cm1 == null || !cm.d(cm1) || cm.e(cm1))
      return; 
    if (paramMouseEvent.isControlDown()) {
      ci.b(cm.i(this.eq)).a(this.er, this.es, i, j);
    } else {
      ci.b(cm.i(this.eq)).b(this.er, this.es, i, j);
    } 
    ci.c(cm.i(this.eq)).f();
    cm.c(this.eq);
    cm.c(cm1);
  }
  
  public void mouseClicked(MouseEvent paramMouseEvent) {
    if (paramMouseEvent.getClickCount() == 2) {
      n n = ci.b(cm.i(this.eq)).h(this.er, this.es);
      if (n != null) {
        cy.a(cm.i(this.eq), n);
        cm.c(this.eq);
      } 
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\cp.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */