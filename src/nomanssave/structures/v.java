package nomanssave.structures;

import java.util.ArrayList;
import nomanssave.util.h;
import nomanssave.util.k;

public class v {
  private final int index;
  
  private final k ky;
  
  private final m ec;
  
  private m iK;
  
  public static v[] l(k paramk) {
    h h = paramk.d("ShipOwnership");
    if (h == null || h.size() == 0)
      return new v[0]; 
    ArrayList<v> arrayList = new ArrayList();
    v[] arrayOfV = new v[(h == null) ? 0 : h.size()];
    for (byte b = 0; b < arrayOfV.length; b++) {
      k k1 = h.al(b);
      if (k1.d("Resource.Seed").ar(0))
        arrayList.add(new v(arrayList.size(), k1, k1.ac("Inventory"), k1.ac("Inventory_TechOnly"))); 
    } 
    return arrayList.<v>toArray(new v[0]);
  }
  
  private v(int paramInt, k paramk1, k paramk2, k paramk3) {
    this.index = paramInt;
    this.ky = paramk1;
    this.ec = new m("Ship " + paramInt + " - General", paramk2, 132);
    this.iK = (paramk3 == null) ? null : new m("Ship " + paramInt + " - Technology", paramk3, 4, 7, 3);
  }
  
  public int getIndex() {
    return this.index;
  }
  
  public String getName() {
    return this.ky.getValueAsString("Name");
  }
  
  public void setName(String paramString) {
    this.ky.a("Name", paramString);
  }
  
  public w cn() {
    return w.T(this.ky.getValueAsString("Resource.Filename"));
  }
  
  public void a(w paramw) {
    this.ky.a("Resource.Filename", paramw.B());
  }
  
  public String bf() {
    return this.ky.d("Resource.Seed").an(1);
  }
  
  public void J(String paramString) {
    this.ky.d("Resource.Seed").a(1, paramString);
  }
  
  public void bX() {
    this.ky.a("Resource.Filename", "");
    this.ky.d("Resource.Seed").a(0, Boolean.FALSE);
    this.ky.d("Resource.Seed").a(1, "0x0");
  }
  
  public x bj() {
    return x.U(this.ky.getValueAsString("Inventory.Class.InventoryClass"));
  }
  
  public void a(x paramx) {
    this.ky.a("Inventory.Class.InventoryClass", paramx.toString());
    k k1 = this.ky.ac("Inventory_TechOnly.Class");
    if (k1 != null)
      k1.a("InventoryClass", paramx.toString()); 
  }
  
  public m F() {
    return this.ec;
  }
  
  public m bk() {
    return this.iK;
  }
  
  public m bl() {
    return e(8, 6);
  }
  
  public m e(int paramInt1, int paramInt2) {
    if (this.iK != null)
      throw new RuntimeException("Technology already exists"); 
    k k1 = new k();
    k1.a("Slots", new h());
    k1.a("ValidSlotIndices", new h());
    k1.a("Class", this.ky.ac("Inventory.Class").cx());
    k1.a("SubstanceMaxStorageMultiplier", new Integer(1));
    k1.a("ProductMaxStorageMultiplier", new Integer(1));
    k1.a("BaseStatValues", this.ky.d("Inventory.BaseStatValues").cu());
    k1.a("SpecialSlots", new h());
    k1.a("Width", new Integer(paramInt1));
    k1.a("Height", new Integer(paramInt2));
    k1.a("IsCool", Boolean.FALSE);
    k1.a("Version", new Integer(1));
    this.ky.a("Inventory_TechOnly", k1, new String[] { "Inventory" });
    this.iK = new m("Ship " + this.index + " - Technology", k1, 4);
    return this.iK;
  }
  
  private double M(String paramString) {
    double d = this.ec.M(paramString);
    if (d == 0.0D && this.iK != null)
      d = this.iK.M(paramString); 
    return d;
  }
  
  private void a(String paramString, double paramDouble) {
    this.ec.a(paramString, paramDouble);
    if (this.iK != null)
      this.iK.a(paramString, paramDouble); 
  }
  
  public double bU() {
    return M("^SHIP_DAMAGE");
  }
  
  public void c(double paramDouble) {
    a("^SHIP_DAMAGE", paramDouble);
  }
  
  public double co() {
    return M("^SHIP_SHIELD");
  }
  
  public void g(double paramDouble) {
    a("^SHIP_SHIELD", paramDouble);
  }
  
  public double bm() {
    return M("^SHIP_HYPERDRIVE");
  }
  
  public void a(double paramDouble) {
    a("^SHIP_HYPERDRIVE", paramDouble);
  }
  
  public String toString() {
    String str = getName();
    if (str != null && str.length() != 0)
      return str; 
    w w = cn();
    return (w == null) ? ("Unknown [" + this.index + "]") : (w + " [" + this.index + "]");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\v.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */