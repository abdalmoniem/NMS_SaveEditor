package nomanssave.structures;

import java.awt.Dimension;
import java.io.PrintStream;
import nomanssave.Application;
import nomanssave.dH;
import nomanssave.db.d;
import nomanssave.db.e;
import nomanssave.db.k;
import nomanssave.storage.a;
import nomanssave.util.h;
import nomanssave.util.k;
import nomanssave.util.n;

public class m {
  public static final int jR = 1;
  
  public static final int jS = 2;
  
  public static final int jT = 4;
  
  public static final int jU = 8;
  
  public static final int jV = 16;
  
  public static final int jW = 32;
  
  public static final int jX = 63;
  
  public static final int jY = 128;
  
  public static final int jZ = 256;
  
  public static final int ka = 8;
  
  public static final int kb = 6;
  
  private final String name;
  
  private final k kc;
  
  private final int j;
  
  private int width;
  
  private int height;
  
  private int aW;
  
  private int aX;
  
  private k[][] kd;
  
  private boolean[][] ke;
  
  m(String paramString, k paramk, int paramInt) {
    this(paramString, paramk, paramInt, 8, 6);
  }
  
  m(String paramString, k paramk, int paramInt1, int paramInt2, int paramInt3) {
    this.name = paramString;
    this.kc = paramk;
    this.j = paramInt1;
    this.width = paramk.ad("Width");
    this.height = paramk.ad("Height");
    switch (A()[Application.d().x().ordinal()]) {
      case 1:
      case 3:
        this.aW = 9999;
        this.aX = paramk.ad("ProductMaxStorageMultiplier");
        break;
      default:
        this.aW = 250 * paramk.ad("SubstanceMaxStorageMultiplier");
        this.aX = paramk.ad("ProductMaxStorageMultiplier");
        break;
    } 
    int i = Math.max(this.height, paramInt3);
    int j = Math.max(this.width, paramInt2);
    this.kd = new k[i][];
    this.ke = new boolean[i][];
    for (byte b1 = 0; b1 < i; b1++) {
      this.kd[b1] = new k[j];
      this.ke[b1] = new boolean[j];
    } 
    h h1 = paramk.d("ValidSlotIndices");
    for (byte b2 = 0; b2 < h1.size(); b2++) {
      k k1 = h1.al(b2);
      int n = k1.ad("X");
      int i1 = k1.ad("Y");
      if (n >= 0 && n < j && i1 >= 0 && i1 < i)
        this.ke[i1][n] = true; 
    } 
    h h2 = paramk.d("Slots");
    for (byte b3 = 0; b3 < h2.size(); b3++) {
      k k1 = h2.al(b3);
      int n = k1.ad("Index.X");
      int i1 = k1.ad("Index.Y");
      if (n >= 0 && n < j && i1 >= 0 && i1 < i)
        this.kd[i1][n] = k1; 
    } 
  }
  
  public int bD() {
    return this.j;
  }
  
  public boolean ad(int paramInt) {
    return ((this.j & paramInt) == paramInt);
  }
  
  public int getWidth() {
    return this.width;
  }
  
  public int getHeight() {
    return this.height;
  }
  
  double M(String paramString) {
    h h = this.kc.d("BaseStatValues");
    if (h == null)
      return 0.0D; 
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (k1.getValueAsString("BaseStatID").equals(paramString))
        return k1.af("Value"); 
    } 
    return 0.0D;
  }
  
  void a(String paramString, double paramDouble) {
    h h = this.kc.d("BaseStatValues");
    if (h == null)
      throw new RuntimeException("Could not set base stat"); 
    boolean bool = false;
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (k1.getValueAsString("BaseStatID").equals(paramString)) {
        k1.a("Value", Double.valueOf(paramDouble));
        bool = true;
        break;
      } 
    } 
    if (!bool) {
      k k1 = new k();
      k1.a("BaseStatID", paramString);
      k1.a("Value", Double.valueOf(paramDouble));
      h.a(k1);
      bool = true;
    } 
  }
  
  public Dimension getSize() {
    return new Dimension(this.width, this.height);
  }
  
  public boolean a(Dimension paramDimension) {
    int i = 1;
    int j = 1;
    byte b;
    for (b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        if (this.ke[b][b1]) {
          i = Math.max(i, b1 + 1);
          j = Math.max(j, b + 1);
        } 
      } 
    } 
    if (paramDimension.width < i)
      throw new RuntimeException("Cannot resize less than min width"); 
    if (paramDimension.height < j)
      throw new RuntimeException("Cannot resize less than min height"); 
    if (dH.aw()) {
      if (paramDimension.width > (this.ke[0]).length)
        for (b = 0; b < this.kd.length; b++) {
          k[] arrayOfK = new k[paramDimension.width];
          boolean[] arrayOfBoolean = new boolean[paramDimension.width];
          System.arraycopy(this.kd[b], 0, arrayOfK, 0, (this.ke[b]).length);
          System.arraycopy(this.ke[b], 0, arrayOfBoolean, 0, (this.ke[b]).length);
          this.kd[b] = arrayOfK;
          this.ke[b] = arrayOfBoolean;
        }  
      if (paramDimension.height > this.ke.length) {
        k[][] arrayOfK = new k[paramDimension.height][];
        boolean[][] arrayOfBoolean = new boolean[paramDimension.height][];
        System.arraycopy(this.kd, 0, arrayOfK, 0, this.ke.length);
        System.arraycopy(this.ke, 0, arrayOfBoolean, 0, this.ke.length);
        for (int n = this.ke.length; n < paramDimension.height; n++) {
          arrayOfK[n] = new k[paramDimension.width];
          arrayOfBoolean[n] = new boolean[paramDimension.width];
        } 
        this.kd = arrayOfK;
        this.ke = arrayOfBoolean;
      } 
    } else {
      if (paramDimension.width > (this.ke[0]).length)
        throw new RuntimeException("Cannot resize width greater than " + (this.ke[0]).length); 
      if (paramDimension.height > this.ke.length)
        throw new RuntimeException("Cannot resize height greater than " + this.ke.length); 
    } 
    b = 0;
    if (this.width != paramDimension.width) {
      this.width = paramDimension.width;
      this.kc.a("Width", new Integer(this.width));
      b = 1;
    } 
    if (this.height != paramDimension.height) {
      this.height = paramDimension.height;
      this.kc.a("Height", new Integer(this.height));
      b = 1;
    } 
    return b;
  }
  
  public boolean bE() {
    boolean bool = false;
    if (this.width < (this.ke[0]).length) {
      this.width = (this.ke[0]).length;
      this.kc.a("Width", new Integer(this.width));
      bool = true;
    } 
    if (this.height < this.ke.length) {
      this.height = this.ke.length;
      this.kc.a("Height", new Integer(this.height));
      bool = true;
    } 
    return bool;
  }
  
  public Dimension bF() {
    int i = 1;
    int j = 1;
    for (byte b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        if (this.ke[b][b1]) {
          i = Math.max(i, b1 + 1);
          j = Math.max(j, b + 1);
        } 
      } 
    } 
    return new Dimension(i, j);
  }
  
  public Dimension bG() {
    return new Dimension((this.ke[0]).length, this.ke.length);
  }
  
  public int bH() {
    return this.aW;
  }
  
  public int bI() {
    char c = 'ú';
    for (byte b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        if (this.kd[b][b1] != null) {
          String str = this.kd[b][b1].getValueAsString("Type.InventoryType");
          if ("Substance".equals(str)) {
            int i = this.kd[b][b1].ad("Amount");
            e e = e.v(this.kd[b][b1].getValueAsString("Id"));
            if (e != null && e.aK() > 1)
              i = (int)Math.ceil(i / e.aK()); 
            while (i > c)
              c += 'ú'; 
          } 
        } 
      } 
    } 
    return c;
  }
  
  public int bJ() {
    return this.aX;
  }
  
  public int bK() {
    int i = 1;
    for (byte b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        if (this.kd[b][b1] != null) {
          String str = this.kd[b][b1].getValueAsString("Type.InventoryType");
          if ("Product".equals(str)) {
            int j = this.kd[b][b1].ad("Amount");
            e e = e.v(this.kd[b][b1].getValueAsString("Id"));
            if (e != null && e.aK() > 1)
              j = (int)Math.ceil(j / e.aK()); 
            if (j > i)
              i = j; 
          } 
        } 
      } 
    } 
    return i;
  }
  
  public boolean g(int paramInt1, int paramInt2) {
    char c = 'ú';
    int i = 1;
    byte b1;
    for (b1 = 0; b1 < this.kd.length; b1++) {
      for (byte b = 0; b < (this.kd[b1]).length; b++) {
        if (this.kd[b1][b] != null) {
          String str = this.kd[b1][b].getValueAsString("Type.InventoryType");
          if ("Substance".equals(str)) {
            int j = this.kd[b1][b].ad("Amount");
            e e = e.v(this.kd[b1][b].getValueAsString("Id"));
            if (e != null && e.aK() > 1)
              j = (int)Math.ceil(j / e.aK()); 
            while (j > c)
              c += 'ú'; 
          } else if ("Product".equals(str)) {
            int j = this.kd[b1][b].ad("Amount");
            e e = e.v(this.kd[b1][b].getValueAsString("Id"));
            if (e != null && e.aK() > 1)
              j = (int)Math.ceil(j / e.aK()); 
            if (j > i)
              i = j; 
          } 
        } 
      } 
    } 
    if (paramInt1 < c)
      throw new RuntimeException("Cannot change substance max less than stack size"); 
    if (paramInt2 < i)
      throw new RuntimeException("Cannot change product max less than stack size"); 
    if (paramInt1 % 250 != 0)
      throw new RuntimeException("Substance max must be multiple of 250"); 
    b1 = 0;
    byte b2 = 0;
    if (paramInt1 != this.aW) {
      this.kc.a("SubstanceMaxStorageMultiplier", Integer.valueOf(paramInt1 / 250));
      this.aW = paramInt1;
      b1 = 1;
    } 
    if (paramInt2 != this.aX) {
      this.kc.a("ProductMaxStorageMultiplier", Integer.valueOf(paramInt2));
      this.aX = paramInt2;
      b2 = 1;
    } 
    for (byte b3 = 0; b3 < this.kd.length; b3++) {
      for (byte b = 0; b < (this.kd[b3]).length; b++) {
        if (this.kd[b3][b] != null) {
          String str = this.kd[b3][b].getValueAsString("Type.InventoryType");
          if (b1 != 0 && "Substance".equals(str)) {
            int j = paramInt1;
            e e = e.v(this.kd[b3][b].getValueAsString("Id"));
            if (e != null && e.aK() > 1)
              j *= e.aK(); 
            this.kd[b3][b].a("MaxAmount", new Integer(j));
          } else if (b2 && "Product".equals(str)) {
            int j = paramInt2;
            e e = e.v(this.kd[b3][b].getValueAsString("Id"));
            if (e != null && e.aK() > 1)
              j *= e.aK(); 
            this.kd[b3][b].a("MaxAmount", new Integer(j));
          } 
        } 
      } 
    } 
    return b1 | b2;
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!this.ke[paramInt2][paramInt1])
      throw new RuntimeException("Old slot not enabled"); 
    if (!this.ke[paramInt4][paramInt3])
      throw new RuntimeException("New slot not enabled"); 
    h h = this.kc.d("Slots");
    if (this.kd[paramInt4][paramInt3] != null)
      h.b(this.kd[paramInt4][paramInt3]); 
    if (this.kd[paramInt2][paramInt1] == null) {
      this.kd[paramInt4][paramInt3] = null;
    } else {
      k k1 = this.kd[paramInt2][paramInt1].cx();
      k1.a("Index.X", Integer.valueOf(paramInt3));
      k1.a("Index.Y", Integer.valueOf(paramInt4));
      h.a(k1);
      this.kd[paramInt4][paramInt3] = k1;
    } 
  }
  
  public void b(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!this.ke[paramInt2][paramInt1])
      throw new RuntimeException("Old slot not enabled"); 
    if (!this.ke[paramInt4][paramInt3])
      throw new RuntimeException("New slot not enabled"); 
    k k1 = this.kd[paramInt2][paramInt1];
    k k2 = this.kd[paramInt4][paramInt3];
    if (k1 != null && k2 != null && k1.getValueAsString("Id").equals(k2.getValueAsString("Id"))) {
      int i = k2.ad("MaxAmount");
      int j = k1.ad("Amount") + k2.ad("Amount");
      if (j <= i) {
        k2.a("Amount", Integer.valueOf(j));
        i(paramInt1, paramInt2);
      } else {
        k2.a("Amount", Integer.valueOf(i));
        k1.a("Amount", Integer.valueOf(j - i));
      } 
      return;
    } 
    if (k1 != null) {
      k1.a("Index.X", Integer.valueOf(paramInt3));
      k1.a("Index.Y", Integer.valueOf(paramInt4));
    } 
    this.kd[paramInt4][paramInt3] = k1;
    if (k2 != null) {
      k2.a("Index.X", Integer.valueOf(paramInt1));
      k2.a("Index.Y", Integer.valueOf(paramInt2));
    } 
    this.kd[paramInt2][paramInt1] = k2;
  }
  
  public void c(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!this.ke[paramInt2][paramInt1])
      throw new RuntimeException("Old slot not enabled"); 
    if (!this.ke[paramInt4][paramInt3])
      throw new RuntimeException("New slot not enabled"); 
    k k1 = this.kd[paramInt2][paramInt1];
    k k2 = this.kd[paramInt4][paramInt3];
    if (k1 != null) {
      k1.a("Index.X", Integer.valueOf(paramInt3));
      k1.a("Index.Y", Integer.valueOf(paramInt4));
    } 
    this.kd[paramInt4][paramInt3] = k1;
    if (k2 != null) {
      k2.a("Index.X", Integer.valueOf(paramInt1));
      k2.a("Index.Y", Integer.valueOf(paramInt2));
    } 
    this.kd[paramInt2][paramInt1] = k2;
  }
  
  public n h(int paramInt1, int paramInt2) {
    return (this.kd[paramInt2][paramInt1] == null) ? null : new n(this, this.kd[paramInt2][paramInt1], null);
  }
  
  public boolean i(int paramInt1, int paramInt2) {
    if (this.kd[paramInt2][paramInt1] == null)
      return false; 
    h h = this.kc.d("Slots");
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (paramInt1 == k1.ad("Index.X") && paramInt2 == k1.ad("Index.Y"))
        h.as(b); 
    } 
    this.kd[paramInt2][paramInt1] = null;
    return true;
  }
  
  public boolean a(int paramInt1, int paramInt2, m paramm) {
    if (this.kd[paramInt2][paramInt1] == null)
      return false; 
    String str1 = this.kd[paramInt2][paramInt1].getValueAsString("Type.InventoryType");
    String str2 = this.kd[paramInt2][paramInt1].getValueAsString("Id");
    int i = this.kd[paramInt2][paramInt1].ad("Amount");
    double d = this.kd[paramInt2][paramInt1].af("DamageFactor");
    boolean bool = this.kd[paramInt2][paramInt1].ag("FullyInstalled");
    boolean bool1 = false;
    if (str1.equals("Technology")) {
      int j = this.kd[paramInt2][paramInt1].ad("MaxAmount");
      for (byte b = 0; b < paramm.kd.length; b++) {
        for (byte b1 = 0; b1 < (paramm.kd[b]).length; b1++) {
          if (paramm.kd[b][b1] == null && paramm.ke[b][b1] && !paramm.m(b1, b)) {
            paramm.a(b1, b, str1, str2, i, j, d, bool);
            i = 0;
            bool1 = true;
            break;
          } 
        } 
        if (i == 0)
          break; 
      } 
    } 
    if (i > 0 && !str1.equals("Technology"))
      for (byte b = 0; b < paramm.kd.length; b++) {
        for (byte b1 = 0; b1 < (paramm.kd[b]).length; b1++) {
          if (paramm.kd[b][b1] != null && str1.equals(paramm.kd[b][b1].getValueAsString("Type.InventoryType")) && str2.equals(paramm.kd[b][b1].getValueAsString("Id"))) {
            int j = paramm.kd[b][b1].ad("Amount");
            int n = paramm.kd[b][b1].ad("MaxAmount");
            if (j < n) {
              int i1 = (i > n - j) ? (n - j) : i;
              n.info("  added to existing stack: " + i1);
              paramm.kd[b][b1].a("Amount", new Integer(j + i1));
              i -= i1;
              bool1 = true;
              if (i == 0)
                break; 
            } 
          } 
        } 
        if (i == 0)
          break; 
      }  
    if (i > 0 && !str1.equals("Technology")) {
      byte b1;
      e e = e.v(this.kd[paramInt2][paramInt1].getValueAsString("Id"));
      if (str1.equals("Technology")) {
        b1 = this.kd[paramInt2][paramInt1].ad("MaxAmount");
      } else if (str1.equals("Substance")) {
        b1 = paramm.aW;
        if (e != null)
          b1 *= e.aK(); 
      } else if (str1.equals("Product")) {
        if (e != null && e.aE() == d.gS && e.getID().startsWith("^U_")) {
          b1 = 1;
        } else {
          b1 = paramm.aX;
          if (e != null)
            b1 *= e.aK(); 
        } 
      } else {
        b1 = 1;
      } 
      for (byte b2 = 0; b2 < paramm.kd.length; b2++) {
        for (byte b = 0; b < (paramm.kd[b2]).length; b++) {
          if (paramm.kd[b2][b] == null && paramm.ke[b2][b] && !paramm.m(b, b2)) {
            byte b3 = (i > b1) ? b1 : i;
            n.info("  new stack: " + b3);
            paramm.a(b, b2, str1, str2, b3, b1, d, bool);
            i -= b3;
            bool1 = true;
            if (i == 0)
              break; 
          } 
        } 
        if (i == 0)
          break; 
      } 
    } 
    if (!bool1)
      return false; 
    if (i == 0) {
      h h = this.kc.d("Slots");
      for (byte b = 0; b < h.size(); b++) {
        k k1 = h.al(b);
        if (paramInt1 == k1.ad("Index.X") && paramInt2 == k1.ad("Index.Y"))
          h.as(b); 
      } 
      this.kd[paramInt2][paramInt1] = null;
    } else {
      n.info("  remainder: " + i);
      this.kd[paramInt2][paramInt1].a("Amount", new Integer(i));
    } 
    return true;
  }
  
  public boolean a(int paramInt1, int paramInt2, e parame) {
    int i;
    int j;
    Integer integer;
    if (this.kd[paramInt2][paramInt1] != null)
      return false; 
    switch (bP()[parame.aC().ordinal()]) {
      case 1:
        integer = parame.aH();
        if (integer == null) {
          byte b = -1;
          boolean bool = true;
          break;
        } 
        i = integer.intValue();
        j = integer.intValue();
        break;
      case 3:
        j = this.aW * parame.aK();
        i = this.aW * parame.aK();
        break;
      case 2:
        if (parame.aD()) {
          j = 1;
          i = 1;
          break;
        } 
        if (parame.aE() == d.gS && parame.getID().startsWith("^U_")) {
          j = 1;
          i = 1;
          break;
        } 
        j = this.aX * parame.aK();
        i = this.aX * parame.aK();
        break;
      default:
        return false;
    } 
    String str = parame.getID();
    if (parame.aD())
      str = String.valueOf(str) + "#" + (int)Math.floor(Math.random() * 100000.0D); 
    a(paramInt1, paramInt2, parame.aC().toString(), str, i, j, 0.0D, true);
    return true;
  }
  
  private void a(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, int paramInt4, double paramDouble, boolean paramBoolean) {
    h h = this.kc.d("Slots");
    k k1 = new k();
    k k2 = new k();
    k2.a("InventoryType", paramString1);
    k1.a("Type", k2);
    k1.a("Id", paramString2);
    k1.a("Amount", new Integer(paramInt3));
    k1.a("MaxAmount", new Integer(paramInt4));
    k1.a("DamageFactor", new Double(paramDouble));
    k1.a("FullyInstalled", new Boolean(paramBoolean));
    k k3 = new k();
    k3.a("X", Integer.valueOf(paramInt1));
    k3.a("Y", Integer.valueOf(paramInt2));
    k1.a("Index", k3);
    h.a(k1);
    this.kd[paramInt2][paramInt1] = k1;
  }
  
  public boolean j(int paramInt1, int paramInt2) {
    return this.ke[paramInt2][paramInt1];
  }
  
  public void k(int paramInt1, int paramInt2) {
    if (!this.ke[paramInt2][paramInt1]) {
      k k1 = new k();
      k1.a("X", Integer.valueOf(paramInt1));
      k1.a("Y", Integer.valueOf(paramInt2));
      this.kc.d("ValidSlotIndices").a(k1);
      this.ke[paramInt2][paramInt1] = true;
    } 
  }
  
  public void l(int paramInt1, int paramInt2) {
    if (this.ke[paramInt2][paramInt1]) {
      if (this.kd[paramInt2][paramInt1] != null)
        throw new RuntimeException("Cannot disable slot in use"); 
      h h = this.kc.d("ValidSlotIndices");
      for (byte b = 0; b < h.size(); b++) {
        k k1 = h.al(b);
        if (paramInt1 == k1.ad("X") && paramInt2 == k1.ad("Y"))
          h.as(b); 
      } 
      this.ke[paramInt2][paramInt1] = false;
    } 
  }
  
  public boolean m(int paramInt1, int paramInt2) {
    h h = this.kc.d("SpecialSlots");
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if ("Broken".equals(k1.getValueAsString("Type.InventorySpecialSlotType")) && paramInt1 == k1.ad("Index.X") && paramInt2 == k1.ad("Index.Y"))
        return true; 
      if ("BlockedByBrokenTech".equals(k1.getValueAsString("Type.InventorySpecialSlotType")) && paramInt1 == k1.ad("Index.X") && paramInt2 == k1.ad("Index.Y")) {
        n n;
        if ((n = h(paramInt1, paramInt2)) == null || n.bS() == 0.0D) {
          n.info(String.valueOf(this.name) + " slot[" + paramInt1 + "," + paramInt2 + "] appears to be broken, ignoring");
          return false;
        } 
        return true;
      } 
    } 
    return false;
  }
  
  public void n(int paramInt1, int paramInt2) {
    h h = this.kc.d("SpecialSlots");
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if ("Broken".equals(k1.getValueAsString("Type.InventorySpecialSlotType")) && paramInt1 == k1.ad("Index.X") && paramInt2 == k1.ad("Index.Y"))
        h.as(b); 
      if ("BlockedByBrokenTech".equals(k1.getValueAsString("Type.InventorySpecialSlotType")) && paramInt1 == k1.ad("Index.X") && paramInt2 == k1.ad("Index.Y")) {
        n n;
        if ((n = h(paramInt1, paramInt2)) != null && n.bS() != 0.0D)
          i(paramInt1, paramInt2); 
        h.as(b);
      } 
    } 
  }
  
  public boolean bL() {
    boolean bool = false;
    h h = this.kc.d("SpecialSlots");
    byte b;
    for (b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if ("Broken".equals(k1.getValueAsString("Type.InventorySpecialSlotType"))) {
        h.as(b--);
        bool = true;
      } 
      if ("BlockedByBrokenTech".equals(k1.getValueAsString("Type.InventorySpecialSlotType"))) {
        n n;
        if ((n = h(k1.ad("Index.X"), k1.ad("Index.Y"))) != null && n.bS() != 0.0D)
          i(k1.ad("Index.X"), k1.ad("Index.Y")); 
        h.as(b--);
        bool = true;
      } 
    } 
    for (b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        if (this.kd[b][b1] != null && this.kd[b][b1].af("DamageFactor") != 0.0D) {
          this.kd[b][b1].a("DamageFactor", new Double(0.0D));
          this.kd[b][b1].a("FullyInstalled", new Boolean(true));
          bool = true;
        } 
      } 
    } 
    return bool;
  }
  
  private static String Q(String paramString) {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(' ');
    if (paramString != null)
      stringBuffer.append(paramString); 
    if (stringBuffer.length() > 10)
      stringBuffer.delete(10, stringBuffer.length()); 
    while (stringBuffer.length() < 11)
      stringBuffer.append(' '); 
    return stringBuffer.toString();
  }
  
  public void a(PrintStream paramPrintStream) {
    paramPrintStream.print("\t|");
    byte b;
    for (b = 0; b < (this.kd[0]).length; b++)
      paramPrintStream.print("-----------|"); 
    paramPrintStream.println();
    for (b = 0; b < this.kd.length; b++) {
      paramPrintStream.print("\t|");
      byte b1;
      for (b1 = 0; b1 < (this.kd[0]).length; b1++) {
        if (!this.ke[b][b1]) {
          paramPrintStream.print("###########|");
        } else if (this.kd[b][b1] != null) {
          paramPrintStream.print(String.valueOf(Q(d(this.kd[b][b1]))) + "|");
        } else {
          paramPrintStream.print("           |");
        } 
      } 
      paramPrintStream.println();
      paramPrintStream.print("\t|");
      for (b1 = 0; b1 < (this.kd[b]).length; b1++) {
        if (!this.ke[b][b1]) {
          paramPrintStream.print("###########|");
        } else if (this.kd[b][b1] != null) {
          paramPrintStream.print(String.valueOf(Q(e(this.kd[b][b1]))) + "|");
        } else {
          paramPrintStream.print("           |");
        } 
      } 
      paramPrintStream.println();
      paramPrintStream.print("\t|");
      for (b1 = 0; b1 < (this.kd[0]).length; b1++) {
        if (!this.ke[b][b1]) {
          paramPrintStream.print("###########|");
        } else if (this.kd[b][b1] != null) {
          int i = f(this.kd[b][b1]);
          if (i < 0) {
            paramPrintStream.print("           |");
          } else {
            paramPrintStream.print(String.valueOf(Q(String.valueOf(Integer.toString(i)) + "/" + Integer.toString(g(this.kd[b][b1])))) + "|");
          } 
        } else {
          paramPrintStream.print("           |");
        } 
      } 
      paramPrintStream.println();
      paramPrintStream.print("\t|");
      for (b1 = 0; b1 < (this.kd[0]).length; b1++)
        paramPrintStream.print("-----------|"); 
      paramPrintStream.println();
    } 
  }
  
  private static String d(k paramk) {
    return paramk.getValueAsString("Type.InventoryType");
  }
  
  private static String e(k paramk) {
    return paramk.getValueAsString("Id");
  }
  
  private static int f(k paramk) {
    return paramk.ad("Amount");
  }
  
  private static int g(k paramk) {
    return paramk.ad("MaxAmount");
  }
  
  public boolean bM() {
    boolean bool = false;
    for (byte b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        int i;
        if (this.kd[b][b1] != null && "Technology".equals(this.kd[b][b1].getValueAsString("Type.InventoryType")) && this.kd[b][b1].ad("Amount") >= 0 && (i = this.kd[b][b1].ad("MaxAmount")) > 0) {
          this.kd[b][b1].a("Amount", new Integer(i));
          bool = true;
        } 
      } 
    } 
    return bool;
  }
  
  public boolean bN() {
    boolean bool = false;
    for (byte b = 0; b < this.kd.length; b++) {
      for (byte b1 = 0; b1 < (this.kd[b]).length; b1++) {
        int i;
        if (this.kd[b][b1] != null && !"Technology".equals(this.kd[b][b1].getValueAsString("Type.InventoryType")) && (i = this.kd[b][b1].ad("MaxAmount")) > 1) {
          this.kd[b][b1].a("Amount", new Integer(i));
          bool = true;
        } 
      } 
    } 
    return bool;
  }
  
  public boolean bO() {
    boolean bool = false;
    for (byte b = 0; b < this.height; b++) {
      for (byte b1 = 0; b1 < this.width; b1++) {
        if (!this.ke[b][b1]) {
          k k1 = new k();
          k1.a("X", Integer.valueOf(b1));
          k1.a("Y", Integer.valueOf(b));
          this.kc.d("ValidSlotIndices").a(k1);
          this.ke[b][b1] = true;
          bool = true;
        } 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    return this.name;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\m.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */