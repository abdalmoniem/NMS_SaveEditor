package nomanssave.structures;

import java.util.ArrayList;
import java.util.List;
import nomanssave.util.f;
import nomanssave.util.k;

public class c {
  final k iE;
  
  private c(b paramb, k paramk) {
    this.iE = paramk;
  }
  
  public String I() {
    Object object = this.iE.getValue("GalacticAddress");
    return (object instanceof String) ? (String)object : ((object instanceof Number) ? ("0x" + Long.toHexString(((Number)object).longValue())) : null);
  }
  
  public String getName() {
    return this.iE.getValueAsString("Name");
  }
  
  public void setName(String paramString) {
    this.iE.a("Name", paramString);
  }
  
  public int bb() {
    return this.iE.d("Objects").size();
  }
  
  public k bc() {
    return this.iE;
  }
  
  public List bd() {
    ArrayList<d> arrayList = new ArrayList();
    for (k k1 : f.o(this.iE))
      arrayList.add(new d(this, k1)); 
    return arrayList;
  }
  
  public boolean a(d paramd) {
    return f.a(this.iE, paramd.iG);
  }
  
  public String toString() {
    return this.iE.getValueAsString("Name");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\c.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */