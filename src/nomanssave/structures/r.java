package nomanssave.structures;

import java.util.ArrayList;
import nomanssave.db.n;
import nomanssave.db.p;
import nomanssave.util.h;
import nomanssave.util.k;
import nomanssave.util.n;

public class r {
  private final k iJ;
  
  private final h kt;
  
  private final h ku;
  
  private final m ec;
  
  private m iK;
  
  private m kv;
  
  public static r i(k paramk) {
    h h1 = null;
    h h2 = paramk.d("Stats");
    if (h2 != null)
      for (byte b = 0; b < h2.size(); b++) {
        k k1 = h2.al(b);
        if ("^GLOBAL_STATS".equals(k1.getValueAsString("GroupId"))) {
          h1 = k1.d("Stats");
          break;
        } 
      }  
    return new r(paramk, h1, paramk.ac("Inventory"), paramk.ac("Inventory_TechOnly"), paramk.ac("Inventory_Cargo"));
  }
  
  private r(k paramk1, h paramh, k paramk2, k paramk3, k paramk4) {
    this.iJ = paramk1;
    this.kt = paramh;
    h h1 = paramk1.d("KnownWords");
    h h2 = paramk1.d("KnownWordGroups");
    if (h2 == null) {
      h2 = new h();
      paramk1.a("KnownWordGroups", h2);
    } 
    if (h1.size() > 0) {
      byte b = 0;
      while (b < h1.size()) {
        k k1 = h1.al(b);
        n n = n.A(k1.getValueAsString("id"));
        if (n == null) {
          n.warn("Could not build word groups: " + k1.getValueAsString("id"));
          b++;
          continue;
        } 
        for (String str : n.aP()) {
          p p = n.z(str);
          if (p != null) {
            k k2 = new k();
            k2.a("Group", str);
            h h3 = new h();
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(Boolean.FALSE);
            h3.a(p.ordinal(), Boolean.TRUE);
            k2.a("Races", h3);
            h2.a(k2);
            n.debug("Creating word: " + str + "[" + p.ordinal() + "] = true");
          } 
        } 
        h1.as(b);
        n.debug("Removed old word: " + k1.getValueAsString("id"));
      } 
    } 
    this.ku = h2;
    this.ec = new m("Exosuit - General", paramk2, 129);
    this.iK = (paramk3 == null) ? null : new m("Exosuit - Technology", paramk3, 1, 7, 2);
    this.kv = (paramk4 == null) ? null : new m("Exosuit - Cargo", paramk4, 128);
  }
  
  public long bY() {
    return this.iJ.ae("Units") & 0xFFFFFFFFL;
  }
  
  public void c(long paramLong) {
    this.iJ.a("Units", new Integer((int)paramLong));
  }
  
  public long bZ() {
    return this.iJ.ae("Nanites") & 0xFFFFFFFFL;
  }
  
  public void d(long paramLong) {
    this.iJ.a("Nanites", new Integer((int)paramLong));
  }
  
  public long ca() {
    return this.iJ.ae("Specials") & 0xFFFFFFFFL;
  }
  
  public void e(long paramLong) {
    this.iJ.a("Specials", new Integer((int)paramLong));
  }
  
  public int cb() {
    return this.iJ.ad("Health");
  }
  
  public void af(int paramInt) {
    this.iJ.a("Health", new Integer(paramInt));
  }
  
  public int cc() {
    return this.iJ.ad("Shield");
  }
  
  public void ag(int paramInt) {
    this.iJ.a("Shield", new Integer(paramInt));
  }
  
  public int cd() {
    return this.iJ.ad("Energy");
  }
  
  public void ah(int paramInt) {
    this.iJ.a("Energy", new Integer(paramInt));
  }
  
  public m F() {
    return this.ec;
  }
  
  public m bk() {
    return this.iK;
  }
  
  public m bl() {
    return e(8, 6);
  }
  
  public m e(int paramInt1, int paramInt2) {
    if (this.iK != null)
      throw new RuntimeException("Technology already exists"); 
    k k1 = new k();
    k1.a("Slots", new h());
    k1.a("ValidSlotIndices", new h());
    k1.a("Class", this.iJ.ac("Inventory.Class").cx());
    k1.a("SubstanceMaxStorageMultiplier", new Integer(1));
    k1.a("ProductMaxStorageMultiplier", new Integer(1));
    k1.a("BaseStatValues", new h());
    k1.a("SpecialSlots", new h());
    k1.a("Width", new Integer(paramInt1));
    k1.a("Height", new Integer(paramInt2));
    k1.a("IsCool", Boolean.FALSE);
    k1.a("Version", new Integer(1));
    this.iJ.a("Inventory_TechOnly", k1, new String[] { "Inventory" });
    this.iK = new m("Exosuit - Technology", k1, 1);
    return this.iK;
  }
  
  public m ce() {
    return this.kv;
  }
  
  public m cf() {
    return o(8, 6);
  }
  
  public m o(int paramInt1, int paramInt2) {
    if (this.kv != null)
      throw new RuntimeException("Technology already exists"); 
    k k1 = new k();
    k1.a("Slots", new h());
    k1.a("ValidSlotIndices", new h());
    k1.a("Class", this.iJ.ac("Inventory.Class").cx());
    k1.a("SubstanceMaxStorageMultiplier", new Integer(2));
    k1.a("ProductMaxStorageMultiplier", new Integer(5));
    k1.a("BaseStatValues", new h());
    k1.a("SpecialSlots", new h());
    k1.a("Width", new Integer(paramInt1));
    k1.a("Height", new Integer(paramInt2));
    k1.a("IsCool", Boolean.FALSE);
    k1.a("Version", new Integer(1));
    this.iJ.a("Inventory_Cargo", k1, new String[] { "Inventory_TechOnly", "Inventory" });
    this.kv = new m("Exosuit - Cargo", k1, 1);
    return this.kv;
  }
  
  public int cg() {
    return this.iJ.ad("KnownPortalRunes");
  }
  
  public void ai(int paramInt) {
    this.iJ.a("KnownPortalRunes", new Integer(paramInt));
  }
  
  public h ch() {
    return this.iJ.d("KnownTech");
  }
  
  public h ci() {
    return this.iJ.d("KnownProducts");
  }
  
  public h cj() {
    return this.iJ.d("KnownSpecials");
  }
  
  public int aQ() {
    boolean bool = false;
    ArrayList<String> arrayList = new ArrayList();
    for (byte b = 0; b < this.ku.size(); b++) {
      k k1 = this.ku.al(b);
      n n = n.B(k1.getValueAsString("Group"));
      if (n != null && !arrayList.contains(n.getID()))
        arrayList.add(n.getID()); 
    } 
    return bool;
  }
  
  public int b(p paramp) {
    byte b1 = 0;
    for (byte b2 = 0; b2 < this.ku.size(); b2++) {
      k k1 = this.ku.al(b2);
      if (k1.d("Races").ar(paramp.ordinal()))
        b1++; 
    } 
    return b1;
  }
  
  public s a(n paramn) {
    return new s(this, paramn, null);
  }
  
  private boolean a(String paramString, int paramInt) {
    for (byte b = 0; b < this.ku.size(); b++) {
      k k1 = this.ku.al(b);
      if (paramString.equals(k1.getValueAsString("Group")))
        return k1.d("Races").ar(paramInt); 
    } 
    return false;
  }
  
  private void a(String paramString, int paramInt, boolean paramBoolean) {
    for (byte b = 0; b < this.ku.size(); b++) {
      k k1 = this.ku.al(b);
      if (paramString.equals(k1.getValueAsString("Group"))) {
        n.debug("Updating word: " + paramString + "[" + paramInt + "] = " + paramBoolean);
        h h1 = k1.d("Races");
        h1.a(paramInt, new Boolean(paramBoolean));
        for (byte b1 = 0; b1 < h1.size(); b1++)
          paramBoolean |= h1.ar(b1); 
        if (!paramBoolean) {
          n.debug("Removing word: " + paramString);
          this.ku.as(b);
        } 
        return;
      } 
    } 
    if (paramBoolean) {
      n.debug("Creating word: " + paramString + "[" + paramInt + "] = " + paramBoolean);
      k k1 = new k();
      k1.a("Group", paramString);
      h h1 = new h();
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(Boolean.FALSE);
      h1.a(paramInt, new Boolean(paramBoolean));
      k1.a("Races", h1);
      this.ku.a(k1);
    } 
  }
  
  public double ck() {
    return (new Double(Math.round(this.iJ.ad("HazardTimeAlive") / 90.0D) / 10.0D)).doubleValue();
  }
  
  public void f(double paramDouble) {
    long l = Math.round(paramDouble * 900.0D);
    if (l < 0L || l > 2147483647L)
      throw new RuntimeException("Stat value out of range"); 
    this.iJ.a("HazardTimeAlive", new Integer((int)l));
  }
  
  public int a(l paraml) {
    for (byte b = 0; b < this.kt.size(); b++) {
      k k1 = this.kt.al(b);
      if (k1.getValueAsString("Id").equals(paraml.id))
        return k1.ad("Value.IntValue"); 
    } 
    return 0;
  }
  
  public void a(l paraml, int paramInt) {
    if (paramInt < 0)
      throw new RuntimeException("Stat value out of range"); 
    for (byte b = 0; b < this.kt.size(); b++) {
      k k3 = this.kt.al(b);
      if (k3.getValueAsString("Id").equals(paraml.id)) {
        k3.a("Value.IntValue", new Integer(paramInt));
        return;
      } 
    } 
    k k1 = new k();
    k1.a("Id", paraml.id);
    k k2 = new k();
    k2.a("IntValue", new Integer(paramInt));
    k2.a("FloatValue", new Double(0.0D));
    k2.a("Denominator", new Double(0.0D));
    k1.a("Value", k2);
    this.kt.a(k1);
  }
  
  public double b(l paraml) {
    for (byte b = 0; b < this.kt.size(); b++) {
      k k1 = this.kt.al(b);
      if (k1.getValueAsString("Id").equals(paraml.id))
        return k1.af("Value.FloatValue"); 
    } 
    return 0.0D;
  }
  
  public void a(l paraml, double paramDouble) {
    if (paramDouble < 0.0D)
      throw new RuntimeException("Stat value out of range"); 
    for (byte b = 0; b < this.kt.size(); b++) {
      k k3 = this.kt.al(b);
      if (k3.getValueAsString("Id").equals(paraml.id)) {
        k3.a("Value.FloatValue", new Double(paramDouble));
        return;
      } 
    } 
    k k1 = new k();
    k1.a("Id", paraml.id);
    k k2 = new k();
    k2.a("IntValue", new Integer(0));
    k2.a("FloatValue", new Double(paramDouble));
    k2.a("Denominator", new Double(0.0D));
    k1.a("Value", k2);
    this.kt.a(k1);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\r.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */