package nomanssave.structures;

import nomanssave.util.k;

public class p extends o {
  private final k iJ;
  
  p(k paramk1, k paramk2) {
    super(0, null, paramk2);
    this.iJ = paramk1;
  }
  
  public String getName() {
    return this.iJ.getValueAsString("PlayerWeaponName");
  }
  
  public void setName(String paramString) {
    this.iJ.a("PlayerWeaponName", paramString);
  }
  
  public String bf() {
    return this.iJ.d("CurrentWeapon.GenerationSeed").an(1);
  }
  
  public void J(String paramString) {
    this.iJ.d("CurrentWeapon.GenerationSeed").a(1, paramString);
  }
  
  public x bj() {
    return x.U(this.iJ.getValueAsString("WeaponInventory.Class.InventoryClass"));
  }
  
  public void a(x paramx) {
    this.iJ.a("WeaponInventory.Class.InventoryClass", paramx.toString());
  }
  
  public String toString() {
    String str = getName();
    return (str != null && str.length() != 0) ? str : "Multitool";
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\p.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */