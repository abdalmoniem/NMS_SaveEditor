package nomanssave.structures;

public enum h {
  iN("Tiny", "MODELS/COMMON/SPACECRAFT/INDUSTRIAL/FREIGHTERTINY_PROC.SCENE.MBIN"),
  iO("Small", "MODELS/COMMON/SPACECRAFT/INDUSTRIAL/FREIGHTERSMALL_PROC.SCENE.MBIN"),
  iP("Normal", "MODELS/COMMON/SPACECRAFT/INDUSTRIAL/FREIGHTER_PROC.SCENE.MBIN"),
  iQ("Capital", "MODELS/COMMON/SPACECRAFT/INDUSTRIAL/CAPITALFREIGHTER_PROC.SCENE.MBIN");
  
  private String name;
  
  private String filename;
  
  h(String paramString1, String paramString2) {
    this.name = paramString1;
    this.filename = paramString2;
  }
  
  public String B() {
    return this.filename;
  }
  
  public String toString() {
    return this.name;
  }
  
  public static h N(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equals((values()[b]).filename))
        return values()[b]; 
    } 
    return null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\h.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */