package nomanssave.structures;

import nomanssave.db.n;
import nomanssave.db.p;

public class s {
  private final n kw;
  
  private s(r paramr, n paramn) {
    this.kw = paramn;
  }
  
  public String getID() {
    return this.kw.getID();
  }
  
  public boolean c(p paramp) {
    for (String str : this.kw.aP()) {
      if (this.kw.z(str) == paramp)
        return r.a(this.kx, str, paramp.ordinal()); 
    } 
    return false;
  }
  
  public void a(p paramp, boolean paramBoolean) {
    for (String str : this.kw.aP()) {
      if (this.kw.z(str) == paramp)
        r.a(this.kx, str, paramp.ordinal(), paramBoolean); 
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\s.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */