package nomanssave.structures;

public enum l {
  jl("^TRA_STANDING", "Gek Standing"),
  jm("^TDONE_MISSIONS", "Gek Missions"),
  jn("^TSEEN_SYSTEMS", "Gek Systems"),
  jo("^TGUILD_STAND", "Traders Guild Standing"),
  jp("^TGDONE_MISSIONS", "Traders Guild Missions"),
  jq("^PLANTS_PLANTED", "Plants Farmed"),
  jr("^WAR_STANDING", "Vy'keen Standing"),
  js("^WDONE_MISSIONS", "Vy'keen Missions"),
  jt("^WSEEN_SYSTEMS", "Vy'keen Systems"),
  ju("^WGUILD_STAND", "Warriors Guild Standing"),
  jv("^WGDONE_MISSIONS", "Warriors Guild Missions"),
  jw("^EXP_STANDING", "Korvax Standing"),
  jx("^EDONE_MISSIONS", "Korvax Missions"),
  jy("^ESEEN_SYSTEMS", "Korvax Systems"),
  jz("^EGUILD_STAND", "Explorers Guild Standing"),
  jA("^EGDONE_MISSIONS", "Explorers Guild Missions"),
  jB("^RARE_SCANNED", "Rare Creatures Scanned"),
  jC("^PREDS_KILLED", "Predators Killed"),
  jD("^DRONES_KILLED", "Drones Killed"),
  jE("^WALKERS_KILLED", "Walkers Killed"),
  jF("^QUADS_KILLED", "Quads Killed"),
  jG("^PIRATES_KILLED", "Pirates Killed"),
  jH("^POLICE_KILLED", "Police Killed"),
  jI("^DIST_WALKED", "Distance Walked"),
  jJ("^ALIENS_MET", "Aliens Met"),
  jK("^WORDS_LEARNT", "Words Learnt"),
  jL("^MONEY", "Money"),
  jM("^ENEMIES_KILLED", "Ships Destroyed"),
  jN("^SENTINEL_KILLS", "Sentinels Destroyed"),
  jO("^DIST_WARP", "Distance Warped"),
  jP("^DISC_ALL_CREATU", "Planet Zoology Scanned");
  
  final String id;
  
  final String name;
  
  l(String paramString1, String paramString2) {
    this.id = paramString1;
    this.name = paramString2;
  }
  
  public String getID() {
    return this.id;
  }
  
  public String toString() {
    return this.name;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\l.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */