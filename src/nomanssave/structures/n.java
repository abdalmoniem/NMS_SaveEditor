package nomanssave.structures;

import nomanssave.util.k;

public class n {
  private final k kg;
  
  private n(m paramm, k paramk) {
    this.kg = paramk;
  }
  
  public String getType() {
    return this.kg.getValueAsString("Type.InventoryType");
  }
  
  public String getId() {
    return this.kg.getValueAsString("Id");
  }
  
  public void setId(String paramString) {
    this.kg.a("Id", paramString);
  }
  
  public int bQ() {
    return this.kg.ad("Amount");
  }
  
  public void ae(int paramInt) {
    this.kg.a("Amount", new Integer(paramInt));
  }
  
  public int bR() {
    return this.kg.ad("MaxAmount");
  }
  
  public double bS() {
    return this.kg.af("DamageFactor");
  }
  
  public void b(double paramDouble) {
    this.kg.a("DamageFactor", new Double(paramDouble));
  }
  
  public boolean bT() {
    return this.kg.ag("FullyInstalled");
  }
  
  public void d(boolean paramBoolean) {
    this.kg.a("FullyInstalled", new Boolean(paramBoolean));
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\n.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */