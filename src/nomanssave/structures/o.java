package nomanssave.structures;

import java.util.ArrayList;
import nomanssave.util.h;
import nomanssave.util.k;

public class o {
  private final int index;
  
  private final k ki;
  
  private final m iK;
  
  public static o[] h(k paramk) {
    h h = paramk.d("MultiTools");
    if (h == null || h.size() == 0)
      return new o[] { new p(paramk, paramk.ac("WeaponInventory")) }; 
    ArrayList<o> arrayList = new ArrayList();
    o[] arrayOfO = new o[(h == null) ? 0 : h.size()];
    for (byte b = 0; b < arrayOfO.length; b++) {
      k k1 = h.al(b);
      if (k1.d("Seed").ar(0))
        arrayList.add(new o(arrayList.size(), k1, k1.ac("Store"))); 
    } 
    return arrayList.<o>toArray(new o[0]);
  }
  
  o(int paramInt, k paramk1, k paramk2) {
    this.index = paramInt;
    this.ki = paramk1;
    this.iK = new m("Multitool " + paramInt, paramk2, 2);
  }
  
  public int getIndex() {
    return this.index;
  }
  
  public String getName() {
    return this.ki.getValueAsString("Name");
  }
  
  public void setName(String paramString) {
    this.ki.a("Name", paramString);
  }
  
  public String bf() {
    return this.ki.d("Seed").an(1);
  }
  
  public void J(String paramString) {
    this.ki.d("Seed").a(1, paramString);
  }
  
  public x bj() {
    return x.U(this.ki.getValueAsString("Store.Class.InventoryClass"));
  }
  
  public void a(x paramx) {
    this.ki.a("Store.Class.InventoryClass", paramx.toString());
  }
  
  public m bk() {
    return this.iK;
  }
  
  private double M(String paramString) {
    return this.iK.M(paramString);
  }
  
  private void a(String paramString, double paramDouble) {
    this.iK.a(paramString, paramDouble);
  }
  
  public double bU() {
    return M("^WEAPON_DAMAGE");
  }
  
  public void c(double paramDouble) {
    a("^WEAPON_DAMAGE", paramDouble);
  }
  
  public double bV() {
    return M("^WEAPON_MINING");
  }
  
  public void d(double paramDouble) {
    a("^WEAPON_MINING", paramDouble);
  }
  
  public double bW() {
    return M("^WEAPON_SCAN");
  }
  
  public void e(double paramDouble) {
    a("^WEAPON_SCAN", paramDouble);
  }
  
  public void bX() {
    this.ki.d("Seed").a(0, Boolean.FALSE);
    this.ki.d("Seed").a(1, "0x0");
  }
  
  public String toString() {
    String str = getName();
    return (str != null && str.length() != 0) ? str : ("Multitool [" + this.index + "]");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\o.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */