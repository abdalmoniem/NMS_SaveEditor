package nomanssave.structures;

import java.util.ArrayList;
import nomanssave.db.a;
import nomanssave.util.h;
import nomanssave.util.k;

public class i {
  private final int index;
  
  private final k iS;
  
  public static i[] c(k paramk) {
    h h = paramk.d("FleetFrigates");
    return (h == null) ? new i[0] : a(h);
  }
  
  public static i[] a(h paramh) {
    if (paramh.size() == 0)
      return new i[0]; 
    ArrayList<i> arrayList = new ArrayList();
    i[] arrayOfI = new i[(paramh == null) ? 0 : paramh.size()];
    for (byte b = 0; b < arrayOfI.length; b++) {
      k k1 = paramh.al(b);
      arrayList.add(new i(b, k1));
    } 
    return arrayList.<i>toArray(new i[0]);
  }
  
  private i(int paramInt, k paramk) {
    this.index = paramInt;
    this.iS = paramk;
  }
  
  public int getIndex() {
    return this.index;
  }
  
  public String getName() {
    return this.iS.getValueAsString("CustomName");
  }
  
  public void setName(String paramString) {
    this.iS.a("CustomName", paramString);
  }
  
  public k bo() {
    return k.P(this.iS.getValueAsString("FrigateClass.FrigateClass"));
  }
  
  public void c(k paramk) {
    this.iS.a("FrigateClass.FrigateClass", paramk.getAlias());
  }
  
  public x bj() {
    byte b = -2;
    h h = this.iS.d("TraitIDs");
    for (byte b1 = 0; b1 < h.size(); b1++) {
      String str = h.an(b1);
      a a = a.u(str);
      if (a != null && a.aA())
        b++; 
    } 
    if (b < 0)
      b = 0; 
    if (b > 3)
      b = 3; 
    return x.values()[b];
  }
  
  public String bh() {
    return this.iS.d("HomeSystemSeed").an(1);
  }
  
  public void K(String paramString) {
    this.iS.d("HomeSystemSeed").a(1, paramString);
  }
  
  public String bi() {
    return this.iS.d("ResourceSeed").an(1);
  }
  
  public void L(String paramString) {
    this.iS.d("ResourceSeed").a(1, paramString);
  }
  
  public a bp() {
    return a.I(this.iS.getValueAsString("Race.AlienRace"));
  }
  
  public void a(a parama) {
    this.iS.a("Race.AlienRace", parama.toString());
  }
  
  private int O(int paramInt) {
    return this.iS.d("Stats").ao(paramInt);
  }
  
  private void f(int paramInt1, int paramInt2) {
    this.iS.d("Stats").a(paramInt1, new Integer(paramInt2));
  }
  
  public int bq() {
    return O(0);
  }
  
  public void P(int paramInt) {
    f(0, paramInt);
  }
  
  public int br() {
    return O(1);
  }
  
  public void Q(int paramInt) {
    f(1, paramInt);
  }
  
  public int bs() {
    return O(2);
  }
  
  public void R(int paramInt) {
    f(2, paramInt);
  }
  
  public int bt() {
    return O(3);
  }
  
  public void S(int paramInt) {
    f(3, paramInt);
  }
  
  public int bu() {
    return O(4);
  }
  
  public void T(int paramInt) {
    f(4, paramInt);
  }
  
  public int bv() {
    return O(5);
  }
  
  public void U(int paramInt) {
    f(5, paramInt);
  }
  
  public int bw() {
    return O(6);
  }
  
  public void V(int paramInt) {
    f(6, paramInt);
  }
  
  public a W(int paramInt) {
    h h = this.iS.d("TraitIDs");
    if (paramInt < h.size()) {
      String str = h.an(paramInt);
      return a.u(str);
    } 
    return null;
  }
  
  public void a(int paramInt, a parama) {
    h h = this.iS.d("TraitIDs");
    if (paramInt < h.size())
      h.a(paramInt, (parama == null) ? "^" : parama.getID()); 
  }
  
  public int bx() {
    return this.iS.ad("TotalNumberOfExpeditions");
  }
  
  public void X(int paramInt) {
    this.iS.a("TotalNumberOfExpeditions", new Integer(paramInt));
  }
  
  public int by() {
    return this.iS.ad("TotalNumberOfSuccessfulEvents");
  }
  
  public void Y(int paramInt) {
    this.iS.a("TotalNumberOfSuccessfulEvents", new Integer(paramInt));
  }
  
  public int bz() {
    return this.iS.ad("TotalNumberOfFailedEvents");
  }
  
  public void Z(int paramInt) {
    this.iS.a("TotalNumberOfFailedEvents", new Integer(paramInt));
  }
  
  public int bA() {
    return this.iS.ad("NumberOfTimesDamaged");
  }
  
  public void aa(int paramInt) {
    this.iS.a("NumberOfTimesDamaged", new Integer(paramInt));
  }
  
  public int bB() {
    return this.iS.ad("RepairsMade");
  }
  
  public void ab(int paramInt) {
    this.iS.a("RepairsMade", new Integer(paramInt));
  }
  
  public int bC() {
    return this.iS.ad("DamageTaken");
  }
  
  public void ac(int paramInt) {
    this.iS.a("DamageTaken", new Integer(paramInt));
  }
  
  public String toString() {
    String str = getName();
    if (str != null && str.length() != 0)
      return str; 
    k k1 = bo();
    return (k1 == null) ? ("Unknown [" + this.index + "]") : (k1 + " [" + this.index + "]");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\i.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */