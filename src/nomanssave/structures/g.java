package nomanssave.structures;

import nomanssave.util.k;

public class g {
  final k iE;
  
  private g(f paramf, k paramk) {
    this.iE = paramk;
  }
  
  public String I() {
    Object object = this.iE.getValue("GalacticAddress");
    return (object instanceof String) ? (String)object : ((object instanceof Number) ? ("0x" + Long.toHexString(((Number)object).longValue())) : null);
  }
  
  public String getName() {
    return this.iE.getValueAsString("Name");
  }
  
  public void setName(String paramString) {
    this.iE.a("Name", paramString);
  }
  
  public int bb() {
    return this.iE.d("Objects").size();
  }
  
  public k bc() {
    return this.iE;
  }
  
  public String toString() {
    return this.iE.getValueAsString("Name");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\g.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */