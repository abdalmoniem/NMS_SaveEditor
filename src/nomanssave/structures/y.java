package nomanssave.structures;

import java.util.ArrayList;
import nomanssave.util.h;
import nomanssave.util.k;

public class y {
  private final int index;
  
  private final k kK;
  
  private final m ec;
  
  public static y[] m(k paramk) {
    h h = paramk.d("VehicleOwnership");
    if (h == null || h.size() == 0)
      return new y[0]; 
    ArrayList<y> arrayList = new ArrayList();
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (!(new Integer(0)).equals(k1.getValue("Location")))
        arrayList.add(new y(b, k1, k1.ac("Inventory"))); 
    } 
    return arrayList.<y>toArray(new y[0]);
  }
  
  private y(int paramInt, k paramk1, k paramk2) {
    this.index = paramInt;
    this.kK = paramk1;
    int i = 128;
    if (paramInt == 5) {
      i |= 0x20;
    } else {
      i |= 0x10;
    } 
    this.ec = new m(getType(), paramk2, i);
  }
  
  public String getType() {
    return (this.index == 0) ? "Roamer" : ((this.index == 1) ? "Nomad" : ((this.index == 2) ? "Colossus" : ((this.index == 3) ? "Pilgrim" : ((this.index == 5) ? "Nautilon" : ("Vehicle " + this.index)))));
  }
  
  public m F() {
    return this.ec;
  }
  
  public String toString() {
    return getType();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\y.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */