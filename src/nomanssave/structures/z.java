package nomanssave.structures;

public enum z {
  kL("Pistol"),
  kM("Rifle"),
  kN("Experimental"),
  kO("Alien");
  
  private String name;
  
  z(String paramString1) {
    this.name = paramString1;
  }
  
  public String toString() {
    return this.name;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\z.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */