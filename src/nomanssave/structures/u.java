package nomanssave.structures;

import nomanssave.util.k;

public class u {
  private final k iJ;
  
  public static u k(k paramk) {
    return new u(paramk);
  }
  
  private u(k paramk) {
    this.iJ = paramk;
  }
  
  public int cm() {
    return this.iJ.ad("PrimaryShip");
  }
  
  public void ak(int paramInt) {
    this.iJ.a("PrimaryShip", Integer.valueOf(paramInt));
  }
  
  public int cb() {
    return this.iJ.ad("ShipHealth");
  }
  
  public void af(int paramInt) {
    this.iJ.a("ShipHealth", new Integer(paramInt));
  }
  
  public int cc() {
    return this.iJ.ad("ShipShield");
  }
  
  public void ag(int paramInt) {
    this.iJ.a("ShipShield", new Integer(paramInt));
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structure\\u.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */