package nomanssave.structures;

import java.util.ArrayList;
import nomanssave.util.h;
import nomanssave.util.k;

public class b {
  private final m[] iA = new m[10];
  
  private final m iB;
  
  private final e[] iC;
  
  private final c[] iD;
  
  public static b a(k paramk) {
    return new b(paramk);
  }
  
  private b(k paramk) {
    for (byte b1 = 0; b1 < 10; b1++)
      this.iA[b1] = new m("Chest " + b1, paramk.ac("Chest" + (b1 + 1) + "Inventory"), 128); 
    k k1 = paramk.ac("CookingIngredientsInventory");
    this.iB = (k1 == null) ? null : new m("Ingredient Storage", k1, 384);
    ArrayList<e> arrayList = new ArrayList();
    h h1 = paramk.d("NPCWorkers");
    String str = "";
    for (byte b2 = 0; b2 < h1.size() && b2 < 5; b2++) {
      k k2 = h1.al(b2);
      if (k2.ag("HiredWorker")) {
        switch (b2) {
          case 0:
            str = "Armorer";
            break;
          case 1:
            str = "Farmer";
            break;
          case 2:
            str = "Overseer";
            break;
          case 3:
            str = "Technician";
            break;
          case 4:
            str = "Scientist";
            break;
        } 
        arrayList.add(new e(this, str, k2, null));
      } 
    } 
    this.iC = arrayList.<e>toArray(new e[0]);
    ArrayList<c> arrayList1 = new ArrayList();
    h h2 = paramk.d("PersistentPlayerBases");
    for (byte b3 = 0; b3 < h2.size(); b3++) {
      k k2 = h2.al(b3);
      if ("HomePlanetBase".equals(k2.getValueAsString("BaseType.PersistentBaseTypes")))
        switch (k2.ad("BaseVersion")) {
          case 3:
          case 4:
            arrayList1.add(new c(this, k2, null));
            break;
        }  
    } 
    this.iD = arrayList1.<c>toArray(new c[0]);
  }
  
  public int aX() {
    return this.iA.length;
  }
  
  public m L(int paramInt) {
    return this.iA[paramInt];
  }
  
  public m aY() {
    return this.iB;
  }
  
  public int aZ() {
    return this.iC.length;
  }
  
  public e M(int paramInt) {
    return this.iC[paramInt];
  }
  
  public int ba() {
    return this.iD.length;
  }
  
  public c N(int paramInt) {
    return this.iD[paramInt];
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\b.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */