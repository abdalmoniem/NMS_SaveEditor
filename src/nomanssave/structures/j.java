package nomanssave.structures;

public enum j {
  iT("Combat"),
  iU("Exploration"),
  iV("Industry"),
  iW("Trading"),
  iX("Cost Per Warp"),
  iY("Expedition Fuel Cost"),
  iZ("Expedition Duration"),
  ja("Loot"),
  jb("Repair"),
  jc("Damage Reduction");
  
  private String name;
  
  j(String paramString1) {
    this.name = paramString1;
  }
  
  public String toString() {
    return this.name;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\j.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */