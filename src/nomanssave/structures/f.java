package nomanssave.structures;

import nomanssave.util.h;
import nomanssave.util.k;

public class f {
  private final k iJ;
  
  private final m ec;
  
  private m iK;
  
  private g iL;
  
  public static f b(k paramk) {
    return (paramk.d("CurrentFreighter.Seed").ar(0) && !isEmpty(paramk.getValueAsString("CurrentFreighter.Filename"))) ? new f(paramk, paramk.ac("FreighterInventory"), paramk.ac("FreighterInventory_TechOnly")) : null;
  }
  
  private static boolean isEmpty(String paramString) {
    return !(paramString != null && paramString.length() != 0);
  }
  
  private f(k paramk1, k paramk2, k paramk3) {
    this.iJ = paramk1;
    this.ec = new m("Freighter - General", paramk2, 128);
    this.iK = (paramk3 == null) ? null : new m("Freighter - Technology", paramk3, 8);
    h h = paramk1.d("PersistentPlayerBases");
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (k1.ad("BaseVersion") == 3 && "FreighterBase".equals(k1.getValueAsString("BaseType.PersistentBaseTypes")))
        this.iL = new g(this, k1, null); 
    } 
  }
  
  public String getName() {
    return this.iJ.getValueAsString("PlayerFreighterName");
  }
  
  public void setName(String paramString) {
    this.iJ.a("PlayerFreighterName", paramString);
  }
  
  public h bg() {
    return h.N(this.iJ.getValueAsString("CurrentFreighter.Filename"));
  }
  
  public void a(h paramh) {
    this.iJ.a("CurrentFreighter.Filename", paramh.B());
  }
  
  public String bh() {
    h h = this.iJ.d("CurrentFreighterHomeSystemSeed");
    if (h == null || !h.ar(0))
      return ""; 
    String str = h.an(1);
    return "0x0".equals(str) ? "" : str;
  }
  
  public void K(String paramString) {
    h h = this.iJ.d("CurrentFreighterHomeSystemSeed");
    if (h == null) {
      h = new h(new Object[] { Boolean.FALSE, "0x0" });
      this.iJ.a("CurrentFreighterHomeSystemSeed", h);
    } 
    h.a(0, Boolean.TRUE);
    h.a(1, (paramString.length() == 0) ? "0x0" : paramString);
    k k1 = this.iJ.ac("CurrentFreighterNPC");
    if (k1 != null) {
      k1.a("Filename", "");
      k1.a("Seed", new h(new Object[] { Boolean.FALSE, "0x0" }));
    } 
  }
  
  public String bi() {
    return this.iJ.d("CurrentFreighter.Seed").an(1);
  }
  
  public void L(String paramString) {
    this.iJ.d("CurrentFreighter.Seed").a(1, paramString);
  }
  
  public x bj() {
    return x.U(this.iJ.getValueAsString("FreighterInventory.Class.InventoryClass"));
  }
  
  public void a(x paramx) {
    this.iJ.a("FreighterInventory.Class.InventoryClass", paramx.toString());
    k k1 = this.iJ.ac("FreighterInventory_TechOnly.Class");
    if (k1 != null)
      k1.a("InventoryClass", paramx.toString()); 
  }
  
  public m F() {
    return this.ec;
  }
  
  public m bk() {
    return this.iK;
  }
  
  public m bl() {
    return e(8, 6);
  }
  
  public m e(int paramInt1, int paramInt2) {
    if (this.iK != null)
      throw new RuntimeException("Technology already exists"); 
    k k1 = new k();
    k1.a("Slots", new h());
    k1.a("ValidSlotIndices", new h());
    k1.a("Class", this.iJ.ac("FreighterInventory.Class").cx());
    k1.a("SubstanceMaxStorageMultiplier", new Integer(1));
    k1.a("ProductMaxStorageMultiplier", new Integer(1));
    k1.a("BaseStatValues", this.iJ.d("FreighterInventory.BaseStatValues").cu());
    k1.a("SpecialSlots", new h());
    k1.a("Width", new Integer(paramInt1));
    k1.a("Height", new Integer(paramInt2));
    k1.a("IsCool", Boolean.FALSE);
    k1.a("Version", new Integer(1));
    this.iJ.a("FreighterInventory_TechOnly", k1, new String[] { "FreighterInventory" });
    this.iK = new m("Freighter - Technology", k1, 4);
    return this.iK;
  }
  
  private double M(String paramString) {
    double d = this.ec.M(paramString);
    if (d == 0.0D && this.iK != null)
      d = this.iK.M(paramString); 
    return d;
  }
  
  private void a(String paramString, double paramDouble) {
    this.ec.a(paramString, paramDouble);
    if (this.iK != null)
      this.iK.a(paramString, paramDouble); 
  }
  
  public double bm() {
    return M("^FREI_HYPERDRIVE");
  }
  
  public void a(double paramDouble) {
    a("^FREI_HYPERDRIVE", paramDouble);
  }
  
  public g bn() {
    return this.iL;
  }
  
  public String toString() {
    String str = getName();
    return (str != null && str.length() != 0) ? str : "Freighter";
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\f.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */