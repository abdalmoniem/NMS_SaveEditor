package nomanssave.structures;

public enum w {
  kz("Hauler", "MODELS/COMMON/SPACECRAFT/DROPSHIPS/DROPSHIP_PROC.SCENE.MBIN"),
  kA("Explorer", "MODELS/COMMON/SPACECRAFT/SCIENTIFIC/SCIENTIFIC_PROC.SCENE.MBIN"),
  kB("Shuttle", "MODELS/COMMON/SPACECRAFT/SHUTTLE/SHUTTLE_PROC.SCENE.MBIN"),
  kC("Fighter", "MODELS/COMMON/SPACECRAFT/FIGHTERS/FIGHTER_PROC.SCENE.MBIN"),
  kD("Exotic", "MODELS/COMMON/SPACECRAFT/S-CLASS/S-CLASS_PROC.SCENE.MBIN");
  
  private String name;
  
  private String filename;
  
  w(String paramString1, String paramString2) {
    this.name = paramString1;
    this.filename = paramString2;
  }
  
  public String B() {
    return this.filename;
  }
  
  public String toString() {
    return this.name;
  }
  
  public static w T(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equals((values()[b]).filename))
        return values()[b]; 
    } 
    return null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\w.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */