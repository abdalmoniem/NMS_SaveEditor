package nomanssave.structures;

public enum q {
  kj("Vy’keen", "Warriors", "MODELS/COMMON/PLAYER/PLAYERCHARACTER/NPCVYKEEN.SCENE.MBIN"),
  kk("Korvax", "Explorers", "MODELS/COMMON/PLAYER/PLAYERCHARACTER/NPCKORVAX.SCENE.MBIN"),
  kl("Gek", "Traders", "MODELS/COMMON/PLAYER/PLAYERCHARACTER/NPCGEK.SCENE.MBIN"),
  km("Fourth Race", null, "MODELS/COMMON/PLAYER/PLAYERCHARACTER/NPCFOURTH.SCENE.MBIN"),
  kn("Vy’keen (Old)", null, "MODELS/PLANETS/NPCS/WARRIOR/WARRIOR.SCENE.MBIN"),
  ko("Korvax (Old)", null, "MODELS/PLANETS/NPCS/EXPLORER/EXPLORERIPAD.SCENE.MBIN"),
  kp("Gek (Old)", null, "MODELS/PLANETS/NPCS/LOWERORDER/LOWERORDER.SCENE.MBIN"),
  kq("Fourth Race (Old)", null, "MODELS/PLANETS/NPCS/FOURTHRACE/FOURTHRACE.SCENE.MBIN");
  
  private String name;
  
  private String kr;
  
  private String filename;
  
  q(String paramString1, String paramString2, String paramString3) {
    this.name = paramString1;
    this.kr = paramString2;
    this.filename = paramString3;
  }
  
  public String B() {
    return this.filename;
  }
  
  public String toString() {
    return this.name;
  }
  
  public static q R(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equals((values()[b]).filename))
        return values()[b]; 
    } 
    return null;
  }
  
  public static q S(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equals((values()[b]).kr))
        return values()[b]; 
    } 
    return null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\q.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */