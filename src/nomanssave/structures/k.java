package nomanssave.structures;

public enum k {
  je("Combat", "Combat"),
  jf("Exploration", "Exploration"),
  jg("Industrial", "Mining"),
  jh("Trade", "Diplomacy"),
  ji("Support", "Support");
  
  private String name;
  
  private String jj;
  
  k(String paramString1, String paramString2) {
    this.name = paramString1;
    this.jj = paramString2;
  }
  
  public String getAlias() {
    return this.jj;
  }
  
  public String toString() {
    return this.name;
  }
  
  public static k O(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equalsIgnoreCase((values()[b]).name))
        return values()[b]; 
    } 
    return null;
  }
  
  public static k P(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equals((values()[b]).jj))
        return values()[b]; 
    } 
    return null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\k.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */