package nomanssave.structures;

public enum a {
  iw("Traders"),
  ix("Warriors"),
  iy("Explorers");
  
  private String name;
  
  a(String paramString1) {
    this.name = paramString1;
  }
  
  public String toString() {
    return this.name;
  }
  
  public static a I(String paramString) {
    for (byte b = 0; b < (values()).length; b++) {
      if (paramString.equals((values()[b]).name))
        return values()[b]; 
    } 
    return null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\structures\a.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */