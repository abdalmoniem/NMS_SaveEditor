package nomanssave;

import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListDataListener;
import nomanssave.storage.p;

class w implements ComboBoxModel {
  w(Application paramApplication) {}
  
  public int getSize() {
    return (Application.c(this.am)).length;
  }
  
  public p j(int paramInt) {
    return Application.c(this.am)[paramInt];
  }
  
  public void addListDataListener(ListDataListener paramListDataListener) {}
  
  public void removeListDataListener(ListDataListener paramListDataListener) {}
  
  public void setSelectedItem(Object paramObject) {
    if (Application.i(this.am)) {
      Application.d(this.am).hidePopup();
      int i = JOptionPane.showConfirmDialog(Application.e(this.am), "Are you sure you want to load a different file and lose current changes?", "Save", 2);
      if (i != 0)
        return; 
      Application.a(this.am, false);
    } 
    byte b = -1;
    synchronized (Application.a(this.am)) {
      byte b1 = 0;
      for (byte b2 = 0; b2 < (Application.c(this.am)).length; b2++) {
        if (Application.c(this.am)[b2] == paramObject) {
          b = b1;
          Application.c(this.am)[b1++] = Application.c(this.am)[b2];
        } else if (!(Application.c(this.am)[b2] instanceof aa)) {
          Application.c(this.am)[b1++] = Application.c(this.am)[b2];
        } 
      } 
      if (b1 < (Application.c(this.am)).length) {
        p[] arrayOfP = new p[b1];
        System.arraycopy(Application.c(this.am), 0, arrayOfP, 0, b1);
        Application.a(this.am, arrayOfP);
      } 
    } 
    Application.b(this.am, b);
  }
  
  public Object getSelectedItem() {
    return (Application.b(this.am) < 0) ? null : Application.c(this.am)[Application.b(this.am)];
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\w.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */