package nomanssave.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import nomanssave.db.l;

public class k {
  private final List hM = new ArrayList();
  
  private final List values = new ArrayList();
  
  public static Object b(String paramString1, String paramString2) {
    m m = new m(paramString1);
    char c = m.cy();
    if (c != '{')
      throw new i("invalid object start: " + c, m.lh, m.li); 
    boolean bool = false;
    l l = null;
    if (!m.a('}'))
      while (true) {
        String str = (String)a(m, (l)null);
        if (!bool) {
          l = l.w(str);
          bool = true;
        } 
        if (l != null)
          str = l.x(str); 
        for (c = m.cy(); Character.isWhitespace(c); c = m.cy());
        if (c != ':')
          throw new i("invalid separator: " + c, m.lh, m.li); 
        Object object = a(m, (l)null);
        if (paramString2.equals(str))
          return object; 
        for (c = m.cy(); Character.isWhitespace(c); c = m.cy());
        if (c != ',') {
          if (c != '}')
            throw new i("invalid end of object: " + c, m.lh, m.li); 
          break;
        } 
      }  
    if (m.li != m.chars.length)
      throw new i("additional data not read!", m.lh, m.li); 
    return null;
  }
  
  public static Object Z(String paramString) {
    m m = new m(String.valueOf(paramString) + " ");
    Object object = a(m, (l)null);
    if (m.li != m.chars.length - 1)
      throw new i("additional data not read!", m.lh, m.li); 
    return object;
  }
  
  public static k aa(String paramString) {
    return b(paramString, false);
  }
  
  public static k b(String paramString, boolean paramBoolean) {
    m m = new m(paramString);
    char c = m.cy();
    if (c != '{')
      throw new i("invalid object start: " + c, m.lh, m.li); 
    k k1 = paramBoolean ? null : new k();
    l l = null;
    if (!m.a('}'))
      while (true) {
        String str = (String)a(m, (l)null);
        if (k1 == null) {
          l = l.w(str);
          if (l != null)
            n.info("  using name mappings: " + l.toString()); 
          k1 = (l == null) ? new k() : new l(l, null);
        } 
        if (l != null)
          str = l.x(str); 
        for (c = m.cy(); Character.isWhitespace(c); c = m.cy());
        if (c != ':')
          throw new i("invalid separator: " + c, m.lh, m.li); 
        Object object = a(m, l);
        k1.put(str, object);
        for (c = m.cy(); Character.isWhitespace(c); c = m.cy());
        if (c != ',') {
          if (c != '}')
            throw new i("invalid end of object: " + c, m.lh, m.li); 
          break;
        } 
      }  
    if (m.li != m.chars.length)
      throw new i("additional data not read!", m.lh, m.li); 
    return (k1 == null) ? new k() : k1;
  }
  
  public static h ab(String paramString) {
    m m = new m(paramString);
    char c = m.cy();
    if (c != '[')
      throw new i("invalid array start: " + c, m.lh, m.li); 
    h h = new h();
    if (!m.a(']'))
      while (true) {
        Object object = a(m, (l)null);
        h.add(object);
        for (c = m.cy(); Character.isWhitespace(c); c = m.cy());
        if (c != ',') {
          if (c != ']')
            throw new i("invalid end of array: " + c, m.lh, m.li); 
          break;
        } 
      }  
    if (m.li != m.chars.length)
      throw new i("additional data not read!", m.lh, m.li); 
    return h;
  }
  
  private static Object a(m paramm, l paraml) {
    char c;
    for (c = paramm.cy(); Character.isWhitespace(c); c = paramm.cy());
    if (c == '"') {
      StringBuffer stringBuffer = new StringBuffer();
      while (true) {
        c = paramm.cy();
        if (c == '"')
          return stringBuffer.toString(); 
        if (c == '\\') {
          c = paramm.cy();
          switch (c) {
            case 'r':
              stringBuffer.append('\r');
              continue;
            case 'n':
              stringBuffer.append('\n');
              continue;
            case 't':
              stringBuffer.append('\t');
              continue;
            case '"':
              stringBuffer.append('"');
              continue;
            case '\\':
              stringBuffer.append('\\');
              continue;
            case '/':
              stringBuffer.append('/');
              continue;
            case 'u':
              try {
                c = (char)(Integer.parseInt(Character.toString(paramm.cy()), 16) << 12 | Integer.parseInt(Character.toString(paramm.cy()), 16) << 8 | Integer.parseInt(Character.toString(paramm.cy()), 16) << 4 | Integer.parseInt(Character.toString(paramm.cy()), 16));
                stringBuffer.append(c);
              } catch (NumberFormatException numberFormatException) {
                throw new i("invalid unicode hex", paramm.lh, paramm.li);
              } 
              continue;
          } 
          throw new i("invalid switch: " + c, paramm.lh, paramm.li);
        } 
        stringBuffer.append(c);
      } 
    } 
    if (c == '{') {
      k k1 = new k();
      if (!paramm.a('}'))
        while (true) {
          String str = (String)a(paramm, (l)null);
          if (paraml != null)
            str = paraml.x(str); 
          for (c = paramm.cy(); Character.isWhitespace(c); c = paramm.cy());
          if (c != ':')
            throw new i("invalid separator: " + c, paramm.lh, paramm.li); 
          Object object = a(paramm, paraml);
          k1.put(str, object);
          for (c = paramm.cy(); Character.isWhitespace(c); c = paramm.cy());
          if (c != ',') {
            if (c != '}')
              throw new i("invalid end of object: " + c, paramm.lh, paramm.li); 
            break;
          } 
        }  
      return k1;
    } 
    if (c == '[') {
      h h = new h();
      if (!paramm.a(']'))
        while (true) {
          Object object = a(paramm, paraml);
          h.add(object);
          for (c = paramm.cy(); Character.isWhitespace(c); c = paramm.cy());
          if (c != ',') {
            if (c != ']')
              throw new i("invalid end of array: " + c, paramm.lh, paramm.li); 
            break;
          } 
        }  
      return h;
    } 
    if (c == 't') {
      if (paramm.cy() != 'r')
        throw new i("invalid constant: t" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 'u')
        throw new i("invalid constant: tr" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 'e')
        throw new i("invalid constant: tru" + c, paramm.lh, paramm.li); 
      return Boolean.TRUE;
    } 
    if (c == 'f') {
      if (paramm.cy() != 'a')
        throw new i("invalid constant: f" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 'l')
        throw new i("invalid constant: fa" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 's')
        throw new i("invalid constant: fal" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 'e')
        throw new i("invalid constant: fals" + c, paramm.lh, paramm.li); 
      return Boolean.FALSE;
    } 
    if (c == 'n') {
      if (paramm.cy() != 'u')
        throw new i("invalid constant: n" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 'l')
        throw new i("invalid constant: nu" + c, paramm.lh, paramm.li); 
      if (paramm.cy() != 'l')
        throw new i("invalid constant: nul" + c, paramm.lh, paramm.li); 
      return null;
    } 
    if (c == '-' || (c >= '0' && c <= '9')) {
      StringBuffer stringBuffer = new StringBuffer();
      if (c == '-') {
        stringBuffer.append('-');
        int j = paramm.ak("0123456789");
        if (j < 0)
          throw new i("invalid number!", paramm.lh, paramm.li); 
        stringBuffer.append(j);
      } else {
        stringBuffer.append(c - 48);
      } 
      int i;
      for (i = paramm.ak("0123456789.eE"); i >= 0 && i < 10; i = paramm.ak("0123456789.eE"))
        stringBuffer.append(i); 
      boolean bool = true;
      if (i == 10) {
        stringBuffer.append('.');
        bool = false;
        while (true) {
          i = paramm.ak("0123456789eE");
          if (i < 0)
            break; 
          if (i == 10 || i == 11) {
            i++;
            break;
          } 
          stringBuffer.append(i);
        } 
      } 
      if (i == 11 || i == 12) {
        stringBuffer.append((i == 11) ? 101 : 69);
        bool = false;
        c = paramm.cy();
        if (c == '+') {
          stringBuffer.append('+');
          i = paramm.ak("0123456789");
          if (i < 0)
            throw new i("invalid exponent!", paramm.lh, paramm.li); 
        } else if (c == '-') {
          stringBuffer.append('-');
          i = paramm.ak("0123456789");
          if (i < 0)
            throw new i("invalid exponent!", paramm.lh, paramm.li); 
        } else {
          i = "0123456789".indexOf(c);
          if (i < 0)
            throw new i("invalid exponent!", paramm.lh, paramm.li); 
        } 
        stringBuffer.append(i);
        for (i = paramm.ak("0123456789"); i >= 0; i = paramm.ak("0123456789"))
          stringBuffer.append(i); 
      } 
      if (bool) {
        BigInteger bigInteger = new BigInteger(stringBuffer.toString());
        try {
          int j = bigInteger.intValueExact();
          return new Integer(j);
        } catch (ArithmeticException arithmeticException) {
          try {
            long l1 = bigInteger.longValueExact();
            return new Long(l1);
          } catch (ArithmeticException arithmeticException1) {
            return new j(stringBuffer.toString());
          } 
        } 
      } 
      return new j(stringBuffer.toString());
    } 
    throw new i("unknown token: " + c, paramm.lh, paramm.li);
  }
  
  private void put(String paramString, Object paramObject) {
    if (this.hM.indexOf(paramString) >= 0)
      throw new RuntimeException(String.valueOf(paramString) + " already exists"); 
    this.hM.add(paramString);
    this.values.add(paramObject);
  }
  
  public List names() {
    return Collections.unmodifiableList(this.hM);
  }
  
  private Object get(String paramString) {
    int i = this.hM.indexOf(paramString);
    return (i < 0) ? null : this.values.get(i);
  }
  
  public Object getValue(String paramString) {
    int i = 0;
    k k1 = this;
    int j;
    while ((j = paramString.indexOf(".", i)) >= 0) {
      Object object = k1.get(paramString.substring(i, j));
      if (object instanceof k) {
        k1 = (k)object;
        i = j + 1;
        continue;
      } 
      return null;
    } 
    return k1.get(paramString.substring(i));
  }
  
  public k ac(String paramString) {
    return (k)getValue(paramString);
  }
  
  public h d(String paramString) {
    return (h)getValue(paramString);
  }
  
  public String getValueAsString(String paramString) {
    return (String)getValue(paramString);
  }
  
  public int ad(String paramString) {
    Object object = getValue(paramString);
    return (object == null) ? 0 : ((Number)object).intValue();
  }
  
  public long ae(String paramString) {
    Object object = getValue(paramString);
    return (object == null) ? 0L : ((Number)object).longValue();
  }
  
  public double af(String paramString) {
    Object object = getValue(paramString);
    return (object == null) ? 0.0D : ((Number)object).doubleValue();
  }
  
  public boolean ag(String paramString) {
    Object object = getValue(paramString);
    return (object == null) ? false : ((Boolean)object).booleanValue();
  }
  
  void set(String paramString, Object paramObject) {
    int i = this.hM.indexOf(paramString);
    if (i >= 0) {
      this.values.set(i, paramObject);
    } else {
      this.hM.add(paramString);
      this.values.add(paramObject);
    } 
  }
  
  void remove(String paramString) {
    int i = this.hM.indexOf(paramString);
    if (i >= 0) {
      this.hM.remove(i);
      this.values.remove(i);
    } 
  }
  
  public boolean a(String paramString, Object paramObject) {
    int i = 0;
    k k1 = this;
    int j;
    while ((j = paramString.indexOf(".", i)) >= 0) {
      Object object = k1.get(paramString.substring(i, j));
      if (object instanceof k) {
        k1 = (k)object;
        i = j + 1;
        continue;
      } 
      return false;
    } 
    k1.set(paramString.substring(i), paramObject);
    return true;
  }
  
  public boolean ah(String paramString) {
    int i = 0;
    k k1 = this;
    int j;
    while ((j = paramString.indexOf(".", i)) >= 0) {
      Object object = k1.get(paramString.substring(i, j));
      if (object instanceof k) {
        k1 = (k)object;
        i = j + 1;
        continue;
      } 
      return false;
    } 
    k1.remove(paramString.substring(i));
    return true;
  }
  
  public boolean a(String paramString, Object paramObject, String[] paramArrayOfString) {
    int i = -1;
    for (byte b = 0; b < paramArrayOfString.length; b++) {
      i = this.hM.indexOf(paramArrayOfString[b]);
      if (i >= 0)
        break; 
    } 
    if (i < 0 || this.hM.indexOf(paramString) >= 0)
      return false; 
    this.hM.add(i + 1, paramString);
    this.values.add(i + 1, paramObject);
    return true;
  }
  
  k[] ai(String paramString) {
    h h = (h)getValue(paramString);
    k[] arrayOfK = new k[h.size()];
    for (byte b = 0; b < h.size(); b++) {
      Object object = h.getValue(b);
      if (object instanceof k) {
        arrayOfK[b] = (k)object;
      } else {
        arrayOfK[b] = null;
      } 
    } 
    return arrayOfK;
  }
  
  static void a(Object paramObject, StringBuffer paramStringBuffer, l paraml) {
    if (paramObject instanceof k) {
      ((k)paramObject).a(paramStringBuffer, paraml);
    } else if (paramObject instanceof h) {
      ((h)paramObject).a(paramStringBuffer, paraml);
    } else if (paramObject instanceof String) {
      char[] arrayOfChar = ((String)paramObject).toCharArray();
      paramStringBuffer.append('"');
      for (byte b = 0; b < arrayOfChar.length; b++) {
        switch (arrayOfChar[b]) {
          case '\r':
            paramStringBuffer.append("\\r");
            break;
          case '\n':
            paramStringBuffer.append("\\n");
            break;
          case '\t':
            paramStringBuffer.append("\\t");
            break;
          case '\\':
            paramStringBuffer.append("\\\\");
            break;
          case '"':
            paramStringBuffer.append("\\\"");
            break;
          default:
            if (arrayOfChar[b] < ' ') {
              paramStringBuffer.append("\\u");
              paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] >> 12 & 0xF).toUpperCase());
              paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] >> 8 & 0xF).toUpperCase());
              paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] >> 4 & 0xF).toUpperCase());
              paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] & 0xF).toUpperCase());
              break;
            } 
            paramStringBuffer.append(arrayOfChar[b]);
            break;
        } 
      } 
      paramStringBuffer.append('"');
    } else {
      paramStringBuffer.append(paramObject);
    } 
  }
  
  void a(StringBuffer paramStringBuffer, l paraml) {
    paramStringBuffer.append('{');
    for (byte b = 0; b < this.values.size(); b++) {
      if (b > 0)
        paramStringBuffer.append(','); 
      String str = this.hM.get(b);
      if (paraml != null)
        str = paraml.y(str); 
      Object object = this.values.get(b);
      a(str, paramStringBuffer, paraml);
      paramStringBuffer.append(":");
      a(object, paramStringBuffer, paraml);
    } 
    paramStringBuffer.append('}');
  }
  
  public String cv() {
    return a(null);
  }
  
  public String a(l paraml) {
    if (paraml != null)
      n.info("  using name mappings: " + paraml.toString()); 
    StringBuffer stringBuffer = new StringBuffer();
    a(stringBuffer, paraml);
    return stringBuffer.toString();
  }
  
  public static String aj(String paramString) {
    StringBuffer stringBuffer = new StringBuffer();
    b(paramString, stringBuffer);
    return stringBuffer.toString();
  }
  
  static void b(String paramString, StringBuffer paramStringBuffer) {
    char[] arrayOfChar = paramString.toCharArray();
    paramStringBuffer.append('"');
    for (byte b = 0; b < arrayOfChar.length; b++) {
      switch (arrayOfChar[b]) {
        case '\r':
          paramStringBuffer.append("\\r");
          break;
        case '\n':
          paramStringBuffer.append("\\n");
          break;
        case '\t':
          paramStringBuffer.append("\\t");
          break;
        case '\\':
          paramStringBuffer.append("\\\\");
          break;
        case '"':
          paramStringBuffer.append("\\\"");
          break;
        default:
          if (arrayOfChar[b] < ' ') {
            paramStringBuffer.append("\\u");
            paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] >> 12 & 0xF).toUpperCase());
            paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] >> 8 & 0xF).toUpperCase());
            paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] >> 4 & 0xF).toUpperCase());
            paramStringBuffer.append(Integer.toHexString(arrayOfChar[b] & 0xF).toUpperCase());
            break;
          } 
          paramStringBuffer.append(arrayOfChar[b]);
          break;
      } 
    } 
    paramStringBuffer.append('"');
  }
  
  static void a(String paramString, Object paramObject, StringBuffer paramStringBuffer) {
    if (paramObject instanceof k) {
      ((k)paramObject).a(String.valueOf(paramString) + '\t', paramStringBuffer);
    } else if (paramObject instanceof h) {
      ((h)paramObject).a(String.valueOf(paramString) + '\t', paramStringBuffer);
    } else if (paramObject instanceof String) {
      b((String)paramObject, paramStringBuffer);
    } else {
      paramStringBuffer.append(paramObject);
    } 
  }
  
  public void p(k paramk) {
    this.hM.clear();
    this.values.clear();
    this.hM.addAll(paramk.hM);
    this.values.addAll(paramk.values);
  }
  
  public k cx() {
    k k1 = new k();
    for (byte b = 0; b < this.values.size(); b++) {
      String str = this.hM.get(b);
      k k2 = (k)this.values.get(b);
      if (k2 instanceof k) {
        k1.put(str, ((k)k2).cx());
      } else if (k2 instanceof h) {
        k1.put(str, ((h)k2).cu());
      } else {
        k1.put(str, k2);
      } 
    } 
    return k1;
  }
  
  void a(String paramString, StringBuffer paramStringBuffer) {
    paramStringBuffer.append('{');
    for (byte b = 0; b < this.values.size(); b++) {
      if (b > 0)
        paramStringBuffer.append(','); 
      String str = this.hM.get(b);
      Object object = this.values.get(b);
      paramStringBuffer.append('\n').append(paramString).append('\t');
      a(paramString, str, paramStringBuffer);
      paramStringBuffer.append(':');
      a(paramString, object, paramStringBuffer);
    } 
    if (this.values.size() > 0)
      paramStringBuffer.append('\n').append(paramString); 
    paramStringBuffer.append('}');
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof k) {
      k k1 = (k)paramObject;
      boolean[] arrayOfBoolean = new boolean[k1.hM.size()];
      byte b;
      for (b = 0; b < this.hM.size(); b++) {
        String str = this.hM.get(b);
        Object object = this.values.get(b);
        int i = k1.hM.indexOf(str);
        if (i < 0)
          return false; 
        arrayOfBoolean[i] = true;
        if (object == null) {
          if (k1.values.get(i) != null)
            return false; 
        } else if (!object.equals(k1.values.get(i))) {
          return false;
        } 
      } 
      for (b = 0; b < arrayOfBoolean.length; b++) {
        if (!arrayOfBoolean[b])
          return false; 
      } 
      return true;
    } 
    return false;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    a("", stringBuffer);
    return stringBuffer.toString();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\k.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */