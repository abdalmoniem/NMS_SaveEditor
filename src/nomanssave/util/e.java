package nomanssave.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class e {
  private static final String kZ = "test_in.txt";
  
  private static final String la = "test_coords.txt";
  
  private static final String lb = "test_template.txt";
  
  private static final String lc = "test_out.txt";
  
  private static double[] a(k paramk, String paramString) {
    h h = paramk.d(paramString);
    if (h.size() != 3)
      throw new RuntimeException("Invalid " + paramString + " coordinates"); 
    return new double[] { h.aq(0), h.aq(1), h.aq(2) };
  }
  
  private static void a(k paramk, String paramString, double[] paramArrayOfdouble) {
    h h = new h();
    h.a(new Double(Double.isNaN(paramArrayOfdouble[0]) ? 0.0D : paramArrayOfdouble[0]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[1]) ? 0.0D : paramArrayOfdouble[1]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[2]) ? 0.0D : paramArrayOfdouble[2]));
    paramk.a(paramString, h);
  }
  
  private static void a(k paramk, File paramFile) {
    double[] arrayOfDouble1 = a(paramk, "Position");
    double[] arrayOfDouble2 = a(paramk, "Forward");
    c c = new c(arrayOfDouble1, arrayOfDouble2);
    h h = paramk.d("Objects").cu();
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      double[] arrayOfDouble3 = a(k1, "Position");
      double[] arrayOfDouble4 = a(k1, "Up");
      double[] arrayOfDouble5 = a(k1, "At");
      a(k1, "Position", c.d(arrayOfDouble3));
      a(k1, "Up", c.d(arrayOfDouble4));
      a(k1, "At", c.d(arrayOfDouble5));
    } 
    FileOutputStream fileOutputStream = new FileOutputStream(paramFile);
    try {
      fileOutputStream.write(h.cv().getBytes());
      fileOutputStream.flush();
    } finally {
      fileOutputStream.close();
    } 
  }
  
  private static void b(k paramk, File paramFile) {
    h h;
    FileInputStream fileInputStream = new FileInputStream(paramFile);
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte = new byte[8096];
      int i;
      while ((i = fileInputStream.read(arrayOfByte)) >= 0)
        byteArrayOutputStream.write(arrayOfByte, 0, i); 
      String str = new String(byteArrayOutputStream.toByteArray());
      h = k.ab(str);
    } finally {
      fileInputStream.close();
    } 
    double[] arrayOfDouble1 = a(paramk, "Position");
    double[] arrayOfDouble2 = a(paramk, "Forward");
    c c = new c(arrayOfDouble1, arrayOfDouble2);
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      double[] arrayOfDouble3 = a(k1, "Position");
      double[] arrayOfDouble4 = a(k1, "Up");
      double[] arrayOfDouble5 = a(k1, "At");
      a(k1, "Position", c.c(arrayOfDouble3));
      a(k1, "Up", c.c(arrayOfDouble4));
      a(k1, "At", c.c(arrayOfDouble5));
    } 
    paramk.a("Objects", h);
  }
  
  public static void main(String[] paramArrayOfString) {
    k k;
    File file = new File("D:\\Projects\\TestProjects\\NoMansSkySaveEditor\\build\\bases");
    FileInputStream fileInputStream = new FileInputStream(new File(file, "test_in.txt"));
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte = new byte[8096];
      int i;
      while ((i = fileInputStream.read(arrayOfByte)) >= 0)
        byteArrayOutputStream.write(arrayOfByte, 0, i); 
      String str = new String(byteArrayOutputStream.toByteArray());
      k = k.aa(str);
    } finally {
      fileInputStream.close();
    } 
    a(k, new File(file, "test_coords.txt"));
    fileInputStream = new FileInputStream(new File(file, "test_template.txt"));
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte = new byte[8096];
      int i;
      while ((i = fileInputStream.read(arrayOfByte)) >= 0)
        byteArrayOutputStream.write(arrayOfByte, 0, i); 
      String str = new String(byteArrayOutputStream.toByteArray());
      k = k.aa(str);
    } finally {
      fileInputStream.close();
    } 
    b(k, new File(file, "test_coords.txt"));
    FileOutputStream fileOutputStream = new FileOutputStream(new File(file, "test_out.txt"));
    try {
      fileOutputStream.write(k.toString().getBytes());
    } finally {
      fileOutputStream.close();
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\e.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */