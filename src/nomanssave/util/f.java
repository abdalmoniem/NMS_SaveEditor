package nomanssave.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class f {
  private static double[] a(k paramk, String paramString) {
    h h = paramk.d(paramString);
    if (h.size() != 3)
      throw new RuntimeException("Invalid " + paramString + " coordinates"); 
    return new double[] { h.aq(0), h.aq(1), h.aq(2) };
  }
  
  private static void a(k paramk, String paramString, double[] paramArrayOfdouble) {
    h h = new h();
    h.a(new Double(Double.isNaN(paramArrayOfdouble[0]) ? 0.0D : paramArrayOfdouble[0]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[1]) ? 0.0D : paramArrayOfdouble[1]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[2]) ? 0.0D : paramArrayOfdouble[2]));
    paramk.a(paramString, h);
  }
  
  public static boolean n(k paramk) {
    return b(paramk, "^BUILDSIGNAL");
  }
  
  public static boolean b(k paramk, String paramString) {
    h h = paramk.d("Objects");
    k k1 = null;
    k k2 = null;
    for (byte b = 0; b < h.size(); b++) {
      k k3 = h.al(b);
      if ("^BASE_FLAG".equals(k3.getValueAsString("ObjectID"))) {
        if (k1 == null) {
          k1 = k3;
        } else {
          n.warn("  multiple base computers found");
          return false;
        } 
      } else if (paramString.equals(k3.getValueAsString("ObjectID"))) {
        if (k2 == null) {
          k2 = k3;
        } else {
          n.warn("  multiple " + paramString + " objects found");
          return false;
        } 
      } 
    } 
    if (k1 == null) {
      n.warn("  no base computer found");
      return false;
    } 
    if (k2 == null) {
      n.warn("  no " + paramString + " object found");
      return false;
    } 
    a(paramk, k1, k2);
    return true;
  }
  
  public static List o(k paramk) {
    ArrayList<k> arrayList = new ArrayList();
    boolean bool = false;
    h h = paramk.d("Objects");
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      String str = k1.getValueAsString("ObjectID");
      if ("^BASE_FLAG".equals(k1.getValueAsString("ObjectID"))) {
        bool = true;
      } else if ("^BUILDSIGNAL".equals(str)) {
        arrayList.add(k1);
      } else if ("^BP_ANALYSER".equals(str)) {
        arrayList.add(k1);
      } else if ("^BUILDBEACON".equals(str)) {
        arrayList.add(k1);
      } 
    } 
    return bool ? arrayList : Collections.emptyList();
  }
  
  public static boolean a(k paramk1, k paramk2) {
    h h = paramk1.d("Objects");
    boolean bool = false;
    k k1 = null;
    for (byte b = 0; b < h.size(); b++) {
      k k2 = h.al(b);
      if ("^BASE_FLAG".equals(k2.getValueAsString("ObjectID"))) {
        if (k1 == null) {
          k1 = k2;
        } else {
          n.warn("  multiple base computers found");
          return false;
        } 
      } else if (k2 == paramk2) {
        bool = true;
      } 
    } 
    if (k1 == null) {
      n.warn("  no base computer found");
      return false;
    } 
    if (!bool) {
      n.warn("  replacement object found");
      return false;
    } 
    a(paramk1, k1, paramk2);
    return true;
  }
  
  private static void a(k paramk1, k paramk2, k paramk3) {
    double[] arrayOfDouble1 = a(paramk1, "Position");
    double[] arrayOfDouble2 = a(paramk1, "Forward");
    double[] arrayOfDouble3 = a(paramk3, "Position");
    c c = new c(arrayOfDouble1, arrayOfDouble2);
    double[] arrayOfDouble4 = c.c(arrayOfDouble3);
    arrayOfDouble4[0] = arrayOfDouble4[0] + arrayOfDouble1[0];
    arrayOfDouble4[1] = arrayOfDouble4[1] + arrayOfDouble1[1];
    arrayOfDouble4[2] = arrayOfDouble4[2] + arrayOfDouble1[2];
    a(paramk1, "Position", arrayOfDouble4);
    arrayOfDouble4 = a(paramk2, "At");
    double[] arrayOfDouble5 = a(paramk3, "At");
    a(paramk2, "At", arrayOfDouble5);
    a(paramk3, "At", arrayOfDouble4);
    arrayOfDouble4 = new double[] { -arrayOfDouble3[0], -arrayOfDouble3[1], -arrayOfDouble3[2] };
    a(paramk3, "Position", arrayOfDouble4);
    h h = paramk1.d("Objects");
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (k1 != paramk2 && k1 != paramk3) {
        arrayOfDouble4 = a(k1, "Position");
        arrayOfDouble4[0] = arrayOfDouble4[0] - arrayOfDouble3[0];
        arrayOfDouble4[1] = arrayOfDouble4[1] - arrayOfDouble3[1];
        arrayOfDouble4[2] = arrayOfDouble4[2] - arrayOfDouble3[2];
        a(k1, "Position", arrayOfDouble4);
      } 
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\f.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */