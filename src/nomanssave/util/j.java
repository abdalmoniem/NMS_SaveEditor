package nomanssave.util;

import java.math.BigDecimal;

public class j extends Number {
  private static final long serialVersionUID = -3517277627548149822L;
  
  private final String value;
  
  j(String paramString) {
    this.value = paramString;
  }
  
  public int intValue() {
    throw new ArithmeticException("Cannot cast number to int");
  }
  
  public long longValue() {
    throw new ArithmeticException("Cannot cast number to long");
  }
  
  public float floatValue() {
    return (new BigDecimal(this.value)).floatValue();
  }
  
  public double doubleValue() {
    return (new BigDecimal(this.value)).doubleValue();
  }
  
  public boolean equals(Object paramObject) {
    return (paramObject instanceof j && this.value.equals(((j)paramObject).value));
  }
  
  public String toString() {
    return this.value;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\j.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */