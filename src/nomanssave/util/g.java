package nomanssave.util;

public class g {
  private int x;
  
  private int y;
  
  private int ld;
  
  private int bn;
  
  private int le;
  
  private static int b(String paramString, int paramInt) {
    int i = 0;
    for (byte b = 0; b < 4; b++) {
      i *= 16;
      char c = paramString.charAt(paramInt + b);
      if (c >= '0' && c <= '9') {
        i += c - 48;
      } else if (c >= 'A' && c <= 'F') {
        i += c - 55;
      } else if (c >= 'a' && c <= 'f') {
        i += c - 87;
      } else {
        throw new RuntimeException("Invalid galactic coordinates");
      } 
    } 
    return i;
  }
  
  private static long V(String paramString) {
    long l = 0L;
    for (byte b = 0; b < paramString.length(); b++) {
      l <<= 4L;
      char c = paramString.charAt(b);
      if (c >= '0' && c <= '9') {
        l += (c - 48);
      } else if (c >= 'A' && c <= 'F') {
        l += (c - 55);
      } else if (c >= 'a' && c <= 'f') {
        l += (c - 87);
      } else {
        throw new RuntimeException("Invalid galactic coordinates");
      } 
    } 
    return l;
  }
  
  public static g W(String paramString) {
    int n;
    if (paramString.length() == 19) {
      n = -1;
    } else {
      n = paramString.indexOf(':');
      if (paramString.length() != n + 20)
        throw new RuntimeException("Invalid galactic coordinates"); 
    } 
    if (paramString.charAt(n + 5) != ':')
      throw new RuntimeException("Invalid galactic coordinates"); 
    if (paramString.charAt(n + 10) != ':')
      throw new RuntimeException("Invalid galactic coordinates"); 
    if (paramString.charAt(n + 15) != ':')
      throw new RuntimeException("Invalid galactic coordinates"); 
    int i = b(paramString, n + 1) - 2047;
    if (i > 2047)
      throw new RuntimeException("Invalid galactic coordinates"); 
    int j = b(paramString, n + 6) - 127;
    if (j > 127)
      throw new RuntimeException("Invalid galactic coordinates"); 
    int k = b(paramString, n + 11) - 2047;
    if (k > 2047)
      throw new RuntimeException("Invalid galactic coordinates"); 
    int m = b(paramString, n + 16);
    if (m > 4095)
      throw new RuntimeException("Invalid galactic coordinates"); 
    return new g(i, j, k, m, 0);
  }
  
  public static String d(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(Integer.toString(paramInt1 + 2047 & 0xFFF, 16));
    while (stringBuffer.length() < 4)
      stringBuffer.insert(0, '0'); 
    stringBuffer.append(':');
    stringBuffer.append(Integer.toString(paramInt2 + 127 & 0xFF, 16));
    while (stringBuffer.length() < 9)
      stringBuffer.insert(5, '0'); 
    stringBuffer.append(':');
    stringBuffer.append(Integer.toString(paramInt3 + 2047 & 0xFFF, 16));
    while (stringBuffer.length() < 14)
      stringBuffer.insert(10, '0'); 
    stringBuffer.append(':');
    stringBuffer.append(Integer.toString(paramInt4 & 0xFFFF, 16));
    while (stringBuffer.length() < 19)
      stringBuffer.insert(15, '0'); 
    return stringBuffer.toString().toUpperCase();
  }
  
  public static g X(String paramString) {
    if (paramString.length() != 12)
      throw new RuntimeException("Invalid portal coordinates"); 
    long l = V(paramString);
    int i = (int)(l >> 44L & 0xFL);
    if (i == 0)
      i = 16; 
    int j = (int)(l >> 32L & 0xFFFL);
    int k = (int)(l >> 24L & 0xFFL);
    int m = (int)(l >> 12L & 0xFFFL);
    int n = (int)(l & 0xFFFL);
    return new g(n, k, m, j, (new Integer(i)).intValue());
  }
  
  public static long a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    long l = 0L;
    l |= (paramInt5 & 0xFL) << 44L;
    l |= (paramInt4 & 0xFFFL) << 32L;
    l |= (paramInt2 & 0xFFL) << 24L;
    l |= (paramInt3 & 0xFFFL) << 12L;
    l |= paramInt1 & 0xFFFL;
    return l;
  }
  
  public static String b(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    long l = a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    return f(l);
  }
  
  public static String f(long paramLong) {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(Long.toString(paramLong, 16).toUpperCase());
    while (stringBuffer.length() < 12)
      stringBuffer.insert(0, '0'); 
    return stringBuffer.toString();
  }
  
  public static g Y(String paramString) {
    int i;
    if ((i = paramString.indexOf(':')) > 0) {
      if (paramString.length() == 19) {
        i = -1;
      } else if (paramString.length() != i + 20) {
        throw new RuntimeException("Invalid galactic coordinates");
      } 
      if (paramString.charAt(i + 5) != ':')
        throw new RuntimeException("Invalid galactic coordinates"); 
      if (paramString.charAt(i + 10) != ':')
        throw new RuntimeException("Invalid galactic coordinates"); 
      if (paramString.charAt(i + 15) != ':')
        throw new RuntimeException("Invalid galactic coordinates"); 
      int j = b(paramString, i + 1) - 2047;
      if (j > 2047)
        throw new RuntimeException("Invalid galactic coordinates"); 
      int k = b(paramString, i + 6) - 127;
      if (k > 127)
        throw new RuntimeException("Invalid galactic coordinates"); 
      int m = b(paramString, i + 11) - 2047;
      if (m > 2047)
        throw new RuntimeException("Invalid galactic coordinates"); 
      int n = b(paramString, i + 16);
      if (n > 4095)
        throw new RuntimeException("Invalid galactic coordinates"); 
      return new g(j, k, m, n, 0);
    } 
    if (paramString.length() == 12) {
      long l = V(paramString);
      int i1 = (int)(l >> 44L & 0xFL);
      if (i1 == 0)
        i1 = 16; 
      int n = (int)(l >> 32L & 0xFFFL);
      int k = (int)(l >> 24L & 0xFFL);
      int m = (int)(l >> 12L & 0xFFFL);
      int j = (int)(l & 0xFFFL);
      return new g(j, k, m, n, i1);
    } 
    throw new RuntimeException("Unsupported coordinates");
  }
  
  public g(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this(paramInt1, paramInt2, paramInt3, paramInt4, 0);
  }
  
  private g(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.x = paramInt1;
    this.y = paramInt2;
    this.ld = paramInt3;
    this.bn = paramInt4;
    this.le = paramInt5;
  }
  
  public int cp() {
    return this.x;
  }
  
  public int cq() {
    return this.y;
  }
  
  public int cr() {
    return this.ld;
  }
  
  public int cs() {
    return this.bn;
  }
  
  public int ct() {
    return this.le;
  }
  
  public String I() {
    return d(this.x, this.y, this.ld, this.bn);
  }
  
  public long J() {
    return a(this.x, this.y, this.ld, this.bn, this.le);
  }
  
  public String toString() {
    return (this.le == 0) ? d(this.x, this.y, this.ld, this.bn) : b(this.x, this.y, this.ld, this.bn, this.le);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\g.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */