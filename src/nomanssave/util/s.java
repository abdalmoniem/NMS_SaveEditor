package nomanssave.util;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

public class s {
  private static final String lC = "9710BD8FCF192837DC6DEF6037AB2837";
  
  private static HashMap lD = new HashMap<>();
  
  public static String g(long paramLong) {
    String str;
    if (lD.containsKey(Long.valueOf(paramLong)))
      return (String)lD.get(Long.valueOf(paramLong)); 
    try {
      str = h(paramLong);
    } catch (IOException iOException) {
      str = null;
    } catch (i i) {
      str = null;
    } 
    lD.put(Long.valueOf(paramLong), str);
    return str;
  }
  
  private static String h(long paramLong) {
    k k = am("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=9710BD8FCF192837DC6DEF6037AB2837&steamids=" + paramLong);
    h h = k.d("players");
    if (h.size() == 0)
      return null; 
    for (byte b = 0; b < h.size(); b++) {
      k k1 = h.al(b);
      if (Long.toString(paramLong).equals(k1.getValueAsString("steamid")))
        return k1.getValueAsString("personaname"); 
    } 
    return null;
  }
  
  private static k am(String paramString) {
    URLConnection uRLConnection = (new URL(paramString)).openConnection();
    int i = uRLConnection.getContentLength();
    int j = 0;
    InputStream inputStream = uRLConnection.getInputStream();
    byte[] arrayOfByte = new byte[i];
    int k;
    while ((k = inputStream.read(arrayOfByte, j, i)) >= 0) {
      j += k;
      i -= k;
    } 
    if (i > 0)
      throw new EOFException(); 
    String str1 = uRLConnection.getContentEncoding();
    String str2 = new String(arrayOfByte, (str1 == null) ? "UTF-8" : str1);
    k k1 = k.aa(str2);
    return k1.ac("response");
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\s.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */