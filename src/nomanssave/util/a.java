package nomanssave.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class a {
  private static double[] a(k paramk, String paramString) {
    h h = paramk.d(paramString);
    if (h.size() != 3)
      throw new RuntimeException("Invalid " + paramString + " coordinates"); 
    return new double[] { h.aq(0), h.aq(1), h.aq(2) };
  }
  
  private static void a(k paramk, String paramString, double[] paramArrayOfdouble) {
    h h = new h();
    h.a(new Double(Double.isNaN(paramArrayOfdouble[0]) ? 0.0D : paramArrayOfdouble[0]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[1]) ? 0.0D : paramArrayOfdouble[1]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[2]) ? 0.0D : paramArrayOfdouble[2]));
    paramk.a(paramString, h);
  }
  
  public static void main(String[] paramArrayOfString) {
    k k1;
    File file = new File("D:\\Projects\\TestProjects\\NoMansSkySaveEditor\\build\\test");
    FileInputStream fileInputStream = new FileInputStream(new File(file, "test_in.txt"));
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte = new byte[8096];
      int i;
      while ((i = fileInputStream.read(arrayOfByte)) >= 0)
        byteArrayOutputStream.write(arrayOfByte, 0, i); 
      String str = new String(byteArrayOutputStream.toByteArray());
      k1 = k.aa(str);
    } finally {
      fileInputStream.close();
    } 
    h h = k1.d("Objects");
    double[] arrayOfDouble1 = a(k1, "Position");
    double[] arrayOfDouble2 = a(k1, "Forward");
    k k2 = null;
    k k3 = null;
    for (byte b1 = 0; b1 < h.size(); b1++) {
      k k = h.al(b1);
      if ("^BASE_FLAG".equals(k.getValueAsString("ObjectID"))) {
        k2 = k;
      } else if ("^BUILDSIGNAL".equals(k.getValueAsString("ObjectID"))) {
        k3 = k;
      } 
    } 
    if (k2 == null)
      throw new RuntimeException("Base computer not found"); 
    if (k3 == null)
      throw new RuntimeException("Signal booster not found"); 
    double[] arrayOfDouble3 = a(k3, "Position");
    c c = new c(arrayOfDouble1, arrayOfDouble2);
    double[] arrayOfDouble4 = c.c(arrayOfDouble3);
    arrayOfDouble4[0] = arrayOfDouble4[0] + arrayOfDouble1[0];
    arrayOfDouble4[1] = arrayOfDouble4[1] + arrayOfDouble1[1];
    arrayOfDouble4[2] = arrayOfDouble4[2] + arrayOfDouble1[2];
    a(k1, "Position", arrayOfDouble4);
    for (byte b2 = 0; b2 < h.size(); b2++) {
      k k = h.al(b2);
      if (!"^BASE_FLAG".equals(k.getValueAsString("ObjectID")))
        if ("^BUILDSIGNAL".equals(k.getValueAsString("ObjectID"))) {
          double[] arrayOfDouble = { -arrayOfDouble3[0], -arrayOfDouble3[1], -arrayOfDouble3[2] };
          a(k, "Position", arrayOfDouble);
        } else {
          double[] arrayOfDouble = a(k, "Position");
          arrayOfDouble[0] = arrayOfDouble[0] - arrayOfDouble3[0];
          arrayOfDouble[1] = arrayOfDouble[1] - arrayOfDouble3[1];
          arrayOfDouble[2] = arrayOfDouble[2] - arrayOfDouble3[2];
          a(k, "Position", arrayOfDouble);
        }  
    } 
    FileOutputStream fileOutputStream = new FileOutputStream(new File(file, "test_out.txt"));
    try {
      fileOutputStream.write(k1.toString().getBytes());
    } finally {
      fileOutputStream.close();
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\a.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */