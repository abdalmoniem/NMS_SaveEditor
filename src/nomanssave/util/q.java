package nomanssave.util;

import java.security.SecureRandom;

public class q {
  private static final SecureRandom lp = new SecureRandom();
  
  private final long lq;
  
  public static q al(String paramString) {
    paramString = paramString.trim();
    if (!paramString.startsWith("0x"))
      throw new RuntimeException("Invalid seed: " + paramString); 
    long l = Long.parseUnsignedLong(paramString.substring(2), 16);
    return new q(l);
  }
  
  public static q cB() {
    return new q(lp.nextLong());
  }
  
  public q(long paramLong) {
    this.lq = paramLong;
  }
  
  public String toString() {
    return "0x" + Long.toHexString(this.lq).toUpperCase();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\q.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */