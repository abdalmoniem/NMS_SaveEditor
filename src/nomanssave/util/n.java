package nomanssave.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class n {
  private static final PrintStream lj = System.out;
  
  private static final PrintStream lk = System.err;
  
  private static PrintStream ll;
  
  public static void h(File paramFile) {
    OutputStream outputStream;
    try {
      outputStream = new FileOutputStream(paramFile);
      Runtime.getRuntime().addShutdownHook(new Thread(() -> close()));
      System.setOut(new PrintStream(new o(lj, "[STDOUT] ")));
      System.setErr(new PrintStream(new o(lk, "[STDERR] ")));
    } catch (FileNotFoundException fileNotFoundException) {
      outputStream = null;
    } 
    ll = (outputStream == null) ? null : new PrintStream(outputStream, true);
  }
  
  public static void debug(String paramString) {
    if (ll != null)
      synchronized (ll) {
        ll.println("[DEBUG] " + paramString.trim());
      }  
  }
  
  public static void info(String paramString) {
    lj.println(paramString);
    if (ll != null)
      synchronized (ll) {
        ll.println("[INFO] " + paramString.trim());
      }  
  }
  
  private static String c(String paramString1, String paramString2) {
    for (byte b = 0; b < paramString1.length(); b++) {
      if (!Character.isWhitespace(paramString1.charAt(b)))
        return String.valueOf(paramString1.substring(0, b)) + paramString2 + paramString1.substring(b); 
    } 
    return "";
  }
  
  public static void warn(String paramString) {
    lj.println(c(paramString, "WARNING: "));
    if (ll != null)
      synchronized (ll) {
        ll.println("[WARNING] " + paramString.trim());
      }  
  }
  
  public static void a(String paramString, Throwable paramThrowable) {
    lj.println(c(paramString, "WARNING: "));
    if (ll != null)
      synchronized (ll) {
        ll.println("[WARNING] " + paramString.trim());
        if (paramThrowable != null) {
          ll.print("[WARNING] ");
          paramThrowable.printStackTrace(ll);
        } 
      }  
  }
  
  public static void error(String paramString, Throwable paramThrowable) {
    lk.println(c(paramString, "ERROR: "));
    if (ll != null)
      synchronized (ll) {
        ll.println("[ERROR] " + paramString.trim());
        if (paramThrowable != null) {
          ll.print("[ERROR] ");
          paramThrowable.printStackTrace(ll);
        } 
      }  
  }
  
  private static void close() {
    ll.close();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\n.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */