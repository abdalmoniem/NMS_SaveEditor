package nomanssave.util;

import java.util.ArrayList;
import java.util.List;
import nomanssave.db.l;

public class h {
  private final List values = new ArrayList();
  
  public h() {}
  
  public h(Object... paramVarArgs) {
    for (byte b = 0; b < paramVarArgs.length; b++)
      this.values.add(paramVarArgs[b]); 
  }
  
  public Object getValue(int paramInt) {
    return this.values.get(paramInt);
  }
  
  public k al(int paramInt) {
    return (k)getValue(paramInt);
  }
  
  public h am(int paramInt) {
    return (h)getValue(paramInt);
  }
  
  public String an(int paramInt) {
    return (String)getValue(paramInt);
  }
  
  public int ao(int paramInt) {
    Object object = getValue(paramInt);
    return (object == null) ? 0 : ((Number)object).intValue();
  }
  
  public long ap(int paramInt) {
    Object object = getValue(paramInt);
    return (object == null) ? 0L : ((Number)object).longValue();
  }
  
  public double aq(int paramInt) {
    Object object = getValue(paramInt);
    return (object == null) ? 0.0D : ((Number)object).doubleValue();
  }
  
  public boolean ar(int paramInt) {
    Object object = getValue(paramInt);
    return (object == null) ? false : ((Boolean)object).booleanValue();
  }
  
  void add(int paramInt, Object paramObject) {
    this.values.add(paramInt, paramObject);
  }
  
  void add(Object paramObject) {
    this.values.add(paramObject);
  }
  
  public void a(Object paramObject) {
    this.values.add(paramObject);
  }
  
  public boolean hasValue(Object paramObject) {
    return this.values.contains(paramObject);
  }
  
  public void a(int paramInt, Object paramObject) {
    this.values.set(paramInt, paramObject);
  }
  
  public int indexOf(Object paramObject) {
    return this.values.indexOf(paramObject);
  }
  
  public boolean b(Object paramObject) {
    int i = this.values.indexOf(paramObject);
    if (i < 0)
      return false; 
    this.values.remove(i);
    return true;
  }
  
  public void as(int paramInt) {
    this.values.remove(paramInt);
  }
  
  public void clear() {
    this.values.clear();
  }
  
  public int size() {
    return this.values.size();
  }
  
  public h cu() {
    h h1 = new h();
    for (byte b = 0; b < this.values.size(); b++) {
      k k = (k)this.values.get(b);
      if (k instanceof k) {
        h1.add(((k)k).cx());
      } else if (k instanceof h) {
        h1.add(((h)k).cu());
      } else {
        h1.add(k);
      } 
    } 
    return h1;
  }
  
  void a(StringBuffer paramStringBuffer, l paraml) {
    paramStringBuffer.append('[');
    for (byte b = 0; b < this.values.size(); b++) {
      if (b > 0)
        paramStringBuffer.append(','); 
      Object object = this.values.get(b);
      k.a(object, paramStringBuffer, paraml);
    } 
    paramStringBuffer.append(']');
  }
  
  void a(String paramString, StringBuffer paramStringBuffer) {
    paramStringBuffer.append('[');
    for (byte b = 0; b < this.values.size(); b++) {
      if (b > 0)
        paramStringBuffer.append(','); 
      Object object = this.values.get(b);
      paramStringBuffer.append('\n').append(paramString).append('\t');
      k.a(paramString, object, paramStringBuffer);
    } 
    if (this.values.size() > 0)
      paramStringBuffer.append('\n').append(paramString); 
    paramStringBuffer.append(']');
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof h) {
      h h1 = (h)paramObject;
      if (this.values.size() != h1.values.size())
        return false; 
      for (byte b = 0; b < this.values.size(); b++) {
        Object object = this.values.get(b);
        if (object == null) {
          if (h1.values.get(b) != null)
            return false; 
        } else if (!object.equals(h1.values.get(b))) {
          return false;
        } 
      } 
      return true;
    } 
    return false;
  }
  
  public String cv() {
    return a((l)null);
  }
  
  public String a(l paraml) {
    if (paraml != null)
      n.info("  using name mappings: " + paraml.toString()); 
    StringBuffer stringBuffer = new StringBuffer();
    a(stringBuffer, paraml);
    return stringBuffer.toString();
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    a("", stringBuffer);
    return stringBuffer.toString();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\h.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */