package nomanssave.util;

class m {
  final char[] chars;
  
  int lh;
  
  int li;
  
  m(String paramString) {
    this.chars = paramString.toCharArray();
    this.lh = 1;
    this.li = 0;
  }
  
  char cy() {
    if (this.li == this.chars.length)
      throw new i("unexpected end of file", this.lh, this.li); 
    if (this.chars[this.li] == '\n')
      this.lh++; 
    return this.chars[this.li++];
  }
  
  boolean a(char paramChar) {
    if (this.li == this.chars.length)
      throw new i("unexpected end of file", this.lh, this.li); 
    while (Character.isWhitespace(this.chars[this.li])) {
      if (++this.li == this.chars.length)
        throw new i("unexpected end of file", this.lh, this.li); 
    } 
    if (this.chars[this.li] == paramChar) {
      this.li++;
      return true;
    } 
    return false;
  }
  
  int ak(String paramString) {
    if (this.li == this.chars.length)
      throw new i("unexpected end of file", this.lh, this.li); 
    int i = paramString.indexOf(this.chars[this.li]);
    if (i >= 0)
      this.li++; 
    return i;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\m.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */