package nomanssave.util;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

class o extends OutputStream {
  final PrintStream lm;
  
  final String ln;
  
  final ByteArrayOutputStream lo;
  
  o(PrintStream paramPrintStream, String paramString) {
    this.lm = paramPrintStream;
    this.ln = paramString;
    this.lo = new ByteArrayOutputStream();
  }
  
  public void write(int paramInt) {
    this.lm.write(paramInt);
    this.lo.write(paramInt);
    if (paramInt == 10) {
      if (n.cz() != null)
        synchronized (n.cz()) {
          n.cz().write(this.ln.getBytes());
          n.cz().write(this.lo.toByteArray());
        }  
      this.lo.reset();
    } 
  }
  
  public void write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (this.lm != null)
      this.lm.write(paramArrayOfbyte, paramInt1, paramInt2); 
    for (byte b = 0; b < paramInt2; b++) {
      if (paramArrayOfbyte[paramInt1 + b] == 10) {
        this.lo.write(paramArrayOfbyte, paramInt1, b + 1);
        if (n.cz() != null)
          synchronized (n.cz()) {
            n.cz().write(this.ln.getBytes());
            n.cz().write(this.lo.toByteArray());
          }  
        this.lo.reset();
        paramInt2 -= b + 1;
        paramInt1 = b + 1;
        b = -1;
      } 
    } 
    this.lo.write(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public void flush() {
    if (this.lo.size() > 0) {
      this.lo.write(System.lineSeparator().getBytes());
      if (n.cz() != null)
        synchronized (n.cz()) {
          n.cz().write(this.ln.getBytes());
          n.cz().write(this.lo.toByteArray());
        }  
      this.lo.reset();
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\o.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */