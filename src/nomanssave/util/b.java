package nomanssave.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class b {
  private static byte[] kQ = new byte[] { 78, 77, 83, 66 };
  
  private static byte[] kR = new byte[] { 
      50, -99, -78, -55, 92, 88, -34, 74, -57, 17, 
      57, -108, -94, Byte.MAX_VALUE, 97, -79 };
  
  private static double[] a(k paramk, String paramString) {
    h h = paramk.d(paramString);
    if (h.size() != 3)
      throw new RuntimeException("Invalid " + paramString + " coordinates"); 
    return new double[] { h.aq(0), h.aq(1), h.aq(2) };
  }
  
  private static void a(k paramk, String paramString, double[] paramArrayOfdouble) {
    h h = new h();
    h.a(new Double(Double.isNaN(paramArrayOfdouble[0]) ? 0.0D : paramArrayOfdouble[0]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[1]) ? 0.0D : paramArrayOfdouble[1]));
    h.a(new Double(Double.isNaN(paramArrayOfdouble[2]) ? 0.0D : paramArrayOfdouble[2]));
    paramk.a(paramString, h);
  }
  
  public static void a(k paramk, File paramFile) {
    CipherOutputStream cipherOutputStream;
    int i = paramk.ad("BaseVersion");
    h h = paramk.d("Objects").cu();
    if (i < 3) {
      double[] arrayOfDouble1 = a(paramk, "Position");
      double[] arrayOfDouble2 = a(paramk, "Forward");
      c c = new c(arrayOfDouble1, arrayOfDouble2);
      for (byte b1 = 0; b1 < h.size(); b1++) {
        k k1 = h.al(b1);
        double[] arrayOfDouble3 = a(k1, "Position");
        double[] arrayOfDouble4 = a(k1, "Up");
        double[] arrayOfDouble5 = a(k1, "At");
        a(k1, "Position", c.d(arrayOfDouble3));
        a(k1, "Up", c.d(arrayOfDouble4));
        a(k1, "At", c.d(arrayOfDouble5));
      } 
    } 
    int j = paramk.ad("UserData");
    SecretKeySpec secretKeySpec = new SecretKeySpec(kR, "AES");
    byte[] arrayOfByte = new byte[16];
    (new SecureRandom()).nextBytes(arrayOfByte);
    IvParameterSpec ivParameterSpec = new IvParameterSpec(arrayOfByte);
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(1, secretKeySpec, ivParameterSpec);
    FileOutputStream fileOutputStream = new FileOutputStream(paramFile);
    try {
      fileOutputStream.write(kQ);
      fileOutputStream.write(new byte[] { 0, 4 });
      fileOutputStream.write(arrayOfByte);
      cipherOutputStream = new CipherOutputStream(fileOutputStream, cipher);
      cipherOutputStream.write(new byte[] { 84, 82, 85, 69 });
      cipherOutputStream.write(j >> 24 & 0xFF);
      cipherOutputStream.write(j >> 16 & 0xFF);
      cipherOutputStream.write(j >> 8 & 0xFF);
      cipherOutputStream.write(j & 0xFF);
      cipherOutputStream.write(h.cv().getBytes());
      cipherOutputStream.flush();
    } finally {
      cipherOutputStream.close();
    } 
  }
  
  public static void b(k paramk, File paramFile) {
    int i;
    CipherInputStream cipherInputStream;
    h h;
    FileInputStream fileInputStream = new FileInputStream(paramFile);
    try {
      byte[] arrayOfByte1 = new byte[8];
      if (fileInputStream.read(arrayOfByte1) != 8)
        throw new IOException("short read"); 
      if (arrayOfByte1[0] != kQ[0] || arrayOfByte1[1] != kQ[1] || arrayOfByte1[2] != kQ[2] || arrayOfByte1[3] != kQ[3])
        throw new IOException("invalid base file"); 
      i = (arrayOfByte1[4] & 0xFF) << 8 | arrayOfByte1[5] & 0xFF;
      switch (i) {
        case 2:
          throw new IOException("unsupported base file");
        case 3:
        case 4:
          break;
        default:
          throw new IOException("invalid base file");
      } 
      byte[] arrayOfByte2 = new byte[16];
      if (fileInputStream.read(arrayOfByte2) != 16)
        throw new IOException("short read"); 
      SecretKeySpec secretKeySpec = new SecretKeySpec(kR, "AES");
      IvParameterSpec ivParameterSpec = new IvParameterSpec(arrayOfByte2);
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      cipher.init(2, secretKeySpec, ivParameterSpec);
      cipherInputStream = new CipherInputStream(fileInputStream, cipher);
      if (cipherInputStream.read(arrayOfByte1, 0, 4) != 4)
        throw new IOException("short read"); 
      if (arrayOfByte1[0] != 84 || arrayOfByte1[1] != 82 || arrayOfByte1[2] != 85 || arrayOfByte1[3] != 69)
        throw new IOException("invalid base file"); 
      int m;
      if ((m = cipherInputStream.read()) < 0)
        throw new IOException("short read"); 
      int n;
      if ((n = cipherInputStream.read()) < 0)
        throw new IOException("short read"); 
      int i1;
      if ((i1 = cipherInputStream.read()) < 0)
        throw new IOException("short read"); 
      int i2;
      if ((i2 = cipherInputStream.read()) < 0)
        throw new IOException("short read"); 
      paramk.a("UserData", Integer.valueOf(m << 24 | n << 16 | i1 << 8 | i2));
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte3 = new byte[8096];
      int i3;
      while ((i3 = cipherInputStream.read(arrayOfByte3)) >= 0)
        byteArrayOutputStream.write(arrayOfByte3, 0, i3); 
      String str = new String(byteArrayOutputStream.toByteArray());
      h = k.ab(str);
    } finally {
      cipherInputStream.close();
    } 
    long l = paramk.ae("LastUpdateTimestamp");
    int j;
    for (j = 0; j < h.size(); j++) {
      k k1 = h.al(j);
      k1.set("Timestamp", new Long(l));
    } 
    if (i == 3) {
      for (j = 0; j < h.size(); j++) {
        k k1 = h.al(j);
        double[] arrayOfDouble1 = a(k1, "Position");
        arrayOfDouble1[0] = arrayOfDouble1[0] + 3.0D;
        arrayOfDouble1[2] = arrayOfDouble1[2] + 3.0D;
        a(arrayOfDouble1);
        a(k1, "Position", arrayOfDouble1);
        double[] arrayOfDouble2 = a(k1, "Up");
        a(arrayOfDouble2);
        a(k1, "Up", arrayOfDouble2);
        double[] arrayOfDouble3 = a(k1, "At");
        a(arrayOfDouble3);
        a(k1, "At", arrayOfDouble3);
      } 
      j = paramk.ad("UserData");
      h.add(0, a("^BASE_FLAG", l, j, new double[] { 0.0D, 0.0D, 0.0D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { 0.0D, 0.0D, 1.0D }));
      h.add(1, a("^MAINROOM", l, j, new double[] { -3.0D, 0.0D, 3.0D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { 0.0D, 0.0D, -1.0D }));
      h.add(2, a("^TELEPORTER", l, j, new double[] { 0.0D, 0.0D, 6.0D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { -0.7071069478988647D, 0.0D, -0.7071067094802856D }));
      h.add(3, a("^BUILDDOOR", l, j, new double[] { -9.005859375D, 0.2421875D, 2.98828125D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { -1.0D, 0.0D, 0.0D }));
      h.add(4, a("^BUILDRAMP", l, j, new double[] { -10.724609375D, 0.296875D, 2.98828125D }, new double[] { -0.2588191032409668D, 0.9659259915351868D, 2.9802322387695312E-8D }, new double[] { -0.9659258127212524D, -0.2588191628456116D, -3.2782554626464844E-7D }));
      h.add(5, a("^BUILDWINDOW", l, j, new double[] { -7.248046875D, 0.5D, -1.25D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { -0.7071069478988647D, 0.0D, -0.7071067094802856D }));
      h.add(6, a("^BUILDWINDOW", l, j, new double[] { -7.248046875D, 0.5D, 7.25D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { -0.7071069478988647D, 0.0D, 0.7071067094802856D }));
      h.add(7, a("^BUILDWINDOW", l, j, new double[] { 1.248046875D, 0.5D, -1.25D }, new double[] { 0.0D, 1.0D, 0.0D }, new double[] { 0.7071069478988647D, 0.0D, -0.7071067094802856D }));
    } 
    paramk.a("BaseVersion", Integer.valueOf(3));
    paramk.a("Objects", h);
  }
  
  private static void a(double[] paramArrayOfdouble) {
    double d = paramArrayOfdouble[0];
    paramArrayOfdouble[0] = -paramArrayOfdouble[2];
    paramArrayOfdouble[2] = d;
  }
  
  private static k a(String paramString, long paramLong, int paramInt, double[] paramArrayOfdouble1, double[] paramArrayOfdouble2, double[] paramArrayOfdouble3) {
    k k = new k();
    k.set("Timestamp", new Long(paramLong));
    k.set("ObjectID", paramString);
    k.set("UserData", Integer.valueOf(paramInt));
    h h = new h();
    h.add(new Double(paramArrayOfdouble1[0]));
    h.add(new Double(paramArrayOfdouble1[1]));
    h.add(new Double(paramArrayOfdouble1[2]));
    k.set("Position", h);
    h = new h();
    h.add(new Double(paramArrayOfdouble2[0]));
    h.add(new Double(paramArrayOfdouble2[1]));
    h.add(new Double(paramArrayOfdouble2[2]));
    k.set("Up", h);
    h = new h();
    h.add(new Double(paramArrayOfdouble3[0]));
    h.add(new Double(paramArrayOfdouble3[1]));
    h.add(new Double(paramArrayOfdouble3[2]));
    k.set("At", h);
    return k;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\b.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */