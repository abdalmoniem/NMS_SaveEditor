package nomanssave.util;

public class i extends Exception {
  private static final long serialVersionUID = -5110119339126419375L;
  
  final int lineNumber;
  
  final int lf;
  
  public i(String paramString, int paramInt1, int paramInt2) {
    super(paramString);
    this.lineNumber = paramInt1;
    this.lf = paramInt2;
  }
  
  public int getLineNumber() {
    return this.lineNumber;
  }
  
  public int cw() {
    return this.lf;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssav\\util\i.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */