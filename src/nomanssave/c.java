package nomanssave;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import nomanssave.db.d;
import nomanssave.db.e;
import nomanssave.db.k;

public class c extends JDialog {
  private e c = null;
  
  private JTextField d;
  
  private JButton e;
  
  private JComboBox f;
  
  private JComboBox g;
  
  private JComboBox h;
  
  private List i = new ArrayList();
  
  private int j;
  
  private List k = new ArrayList();
  
  private e[] l = new e[0];
  
  private static c m = null;
  
  private c(Frame paramFrame) {
    super(paramFrame);
    setResizable(false);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setTitle("Add Item");
    setModal(true);
    JPanel jPanel1 = new JPanel();
    setContentPane(jPanel1);
    jPanel1.setLayout(new BorderLayout(0, 0));
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("100px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("280px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC }, new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC }));
    JLabel jLabel1 = new JLabel("Search:");
    jPanel2.add(jLabel1, "2, 2, left, center");
    JPanel jPanel3 = new JPanel();
    jPanel3.setLayout(new BorderLayout(0, 0));
    this.d = new JTextField();
    jPanel3.add(this.d, "Center");
    this.e = new JButton("Search");
    this.e.addActionListener(new d(this));
    jPanel3.add(this.e, "East");
    jPanel2.add(jPanel3, "4, 2, fill, default");
    JLabel jLabel2 = new JLabel("Type:");
    jPanel2.add(jLabel2, "2, 4, left, center");
    this.f = new JComboBox();
    this.f.setModel(new e(this));
    jPanel2.add(this.f, "4, 4, fill, default");
    JLabel jLabel3 = new JLabel("Category:");
    jPanel2.add(jLabel3, "2, 6, left, center");
    this.g = new JComboBox();
    this.g.setModel(new f(this));
    jPanel2.add(this.g, "4, 6, fill, default");
    JLabel jLabel4 = new JLabel("Item:");
    jPanel2.add(jLabel4, "2, 8, left, center");
    this.h = new JComboBox();
    this.h.setModel(new g(this));
    jPanel2.add(this.h, "4, 8, fill, default");
    jPanel1.add(jPanel2, "Center");
    JPanel jPanel4 = new JPanel();
    jPanel4.setLayout(new FlowLayout(2));
    jPanel1.add(jPanel4, "South");
    JButton jButton1 = new JButton("Save");
    jButton1.addActionListener(new h(this));
    jPanel4.add(jButton1);
    getRootPane().setDefaultButton(jButton1);
    JButton jButton2 = new JButton("Cancel");
    jButton2.addActionListener(new i(this));
    jPanel4.add(jButton2);
    getRootPane().registerKeyboardAction(new j(this), KeyStroke.getKeyStroke(27, 0), 2);
    pack();
  }
  
  private void a() {
    k k = (k)this.f.getSelectedItem();
    this.k.clear();
    if (k == k.hF) {
      if ((this.j & 0x4) == 4) {
        this.k.add(d.gZ);
        this.k.add(d.ha);
      } 
      if ((this.j & 0x2) == 2) {
        this.k.add(d.hb);
        this.k.add(d.hc);
      } 
      if ((this.j & 0x1) == 1) {
        this.k.add(d.hd);
        this.k.add(d.he);
      } 
      if ((this.j & 0x8) == 8)
        this.k.add(d.hg); 
      if ((this.j & 0x10) == 16) {
        this.k.add(d.hh);
        this.k.add(d.hi);
      } 
      if ((this.j & 0x20) == 32) {
        this.k.add(d.hj);
        this.k.add(d.hk);
      } 
    } 
    boolean bool = ((this.j & 0x100) != 0) ? true : false;
    if (k == k.hH && (this.j & 0x80) == 128)
      if (bool) {
        this.k.add(d.gJ);
        this.k.add(d.gN);
        this.k.add(d.gO);
        this.k.add(d.gQ);
      } else {
        this.k.add(d.gJ);
        this.k.add(d.gK);
        this.k.add(d.gL);
        this.k.add(d.gM);
        this.k.add(d.gN);
        this.k.add(d.gO);
        this.k.add(d.gP);
        this.k.add(d.gQ);
      }  
    if (k == k.hG && (this.j & 0x80) == 128)
      if (bool) {
        this.k.add(d.gR);
        this.k.add(d.gS);
      } else {
        this.k.add(d.gR);
        this.k.add(d.gS);
        this.k.add(d.gT);
        this.k.add(d.gU);
        this.k.add(d.gV);
        this.k.add(d.gW);
      }  
    this.g.updateUI();
    this.g.setSelectedIndex((this.k.size() == 1) ? 0 : -1);
  }
  
  private void b() {
    k k = (k)this.f.getSelectedItem();
    d d = (d)this.g.getSelectedItem();
    boolean bool = ((this.j & 0x100) != 0) ? true : false;
    this.l = e.a(k, d, bool);
    this.h.updateUI();
    this.h.setSelectedIndex(-1);
  }
  
  private e a(int paramInt) {
    this.j = paramInt;
    k k = (k)this.f.getSelectedItem();
    d d = (d)this.g.getSelectedItem();
    e e1 = (e)this.h.getSelectedItem();
    this.i.clear();
    if ((paramInt & 0x3F) != 0)
      this.i.add(k.hF); 
    if ((paramInt & 0x80) != 0) {
      this.i.add(k.hH);
      this.i.add(k.hG);
    } 
    this.f.updateUI();
    this.f.setSelectedIndex((this.i.size() == 1) ? 0 : -1);
    int i;
    if (k != null && (i = this.i.indexOf(k)) >= 0)
      this.f.setSelectedIndex(i); 
    a();
    if (d != null && (i = this.k.indexOf(d)) >= 0)
      this.g.setSelectedIndex(i); 
    if (e1 != null)
      for (byte b = 0; b < this.l.length; b++) {
        if (e1 == this.l[b]) {
          this.h.setSelectedIndex(b);
          break;
        } 
      }  
    this.c = null;
    setLocationRelativeTo(getParent());
    setVisible(true);
    return this.c;
  }
  
  public static e a(Container paramContainer, int paramInt) {
    if (m == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      m = new c(frame);
    } 
    return m.a(paramInt);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\c.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */