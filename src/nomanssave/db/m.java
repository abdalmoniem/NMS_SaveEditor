package nomanssave.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import nomanssave.util.n;

class m {
  private final List keys = new ArrayList();
  
  private final List hM = new ArrayList();
  
  private m(InputStream paramInputStream) {
    ArrayList<String> arrayList = new ArrayList();
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(paramInputStream));
    try {
      String str;
      while ((str = bufferedReader.readLine()) != null) {
        try {
          if (str.length() == 0)
            continue; 
          int j = str.indexOf("\t");
          if (j < 0) {
            n.debug("Mapping not available: " + str);
            arrayList.add(str);
            continue;
          } 
          String str1 = str.substring(0, j);
          String str2 = str.substring(j + 1, str.length());
          int i;
          if ((i = this.keys.indexOf(str1)) >= 0) {
            if (str2.equals(this.hM.get(i))) {
              n.debug("Mapping duplicated: " + str1);
              continue;
            } 
            throw new IOException("Mapping error: " + str1);
          } 
          if ((i = this.hM.indexOf(str2)) >= 0) {
            if (str1.equals(this.keys.get(i))) {
              n.debug("Reverse duplicated: " + str2);
              continue;
            } 
            throw new IOException("Reverse error: " + str2);
          } 
          this.keys.add(str1);
          this.hM.add(str2);
        } catch (RuntimeException runtimeException) {
          n.a("Ignoring: " + str, runtimeException);
        } 
      } 
    } finally {
      bufferedReader.close();
    } 
    for (String str : arrayList) {
      if (this.keys.indexOf(str) >= 0)
        throw new IOException("Mapping error: " + str); 
      if (this.hM.indexOf(str) >= 0)
        throw new IOException("Reverse error: " + str); 
      this.keys.add(str);
      this.hM.add(str);
    } 
  }
  
  public String toString() {
    return "NMS 2.2";
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db\m.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */