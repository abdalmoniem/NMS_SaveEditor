package nomanssave.db;

public enum d {
  gJ("Fuel"),
  gK("Metal"),
  gL("Catalyst"),
  gM("Stellar"),
  gN("Flora"),
  gO("Earth"),
  gP("Exotic"),
  gQ("Special"),
  gR("Component"),
  gS("Consumable"),
  gT("Tradeable"),
  gU("Curiosity"),
  gV("Building Part"),
  gW("Procedural"),
  gX("Customisation Part"),
  gY("Emote"),
  gZ("Ship"),
  ha("Procedural"),
  hb("Weapon"),
  hc("Procedural"),
  hd("Suit"),
  he("Procedural"),
  hf("Personal"),
  hg("Freighter"),
  hh("Vehicle"),
  hi("Procedural"),
  hj("Aquatic"),
  hk("Procedural"),
  hl("Maintenance");
  
  private String name;
  
  d(String paramString1) {
    this.name = paramString1;
  }
  
  public String toString() {
    return this.name;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db\d.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */