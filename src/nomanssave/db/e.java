package nomanssave.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import nomanssave.Application;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class e {
  public static final int hn = 0;
  
  public static final int ho = 1;
  
  public static final int hp = 2;
  
  public static final int hq = 3;
  
  final String id;
  
  final k hr;
  
  final boolean hs;
  
  final String name;
  
  final d ht;
  
  final boolean special;
  
  final Integer hu;
  
  final String hv;
  
  final boolean hw;
  
  final String hx;
  
  final int hy;
  
  final String description;
  
  final j[] hz;
  
  private static final List hA = new ArrayList();
  
  private static final List hB = new ArrayList();
  
  private static final List hC = new ArrayList();
  
  static {
    InputStream inputStream = e.class.getResourceAsStream("items.xml");
    if (inputStream != null)
      try {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
        Element element = document.getDocumentElement();
        NodeList nodeList = element.getChildNodes();
        for (byte b = 0; b < nodeList.getLength(); b++) {
          Node node = nodeList.item(b);
          if (node instanceof Element && node.getNodeName().equals("substance")) {
            hA.add(new e(k.hH, (Element)node, false));
          } else if (node instanceof Element && node.getNodeName().equals("product")) {
            hB.add(new e(k.hG, (Element)node, false));
          } else if (node instanceof Element && node.getNodeName().equals("procedural-product")) {
            hB.add(new e(k.hG, (Element)node, true));
          } else if (node instanceof Element && node.getNodeName().equals("technology")) {
            hC.add(new e(k.hF, (Element)node, false));
          } else if (node instanceof Element && node.getNodeName().equals("procedural-technology")) {
            hC.add(new e(k.hF, (Element)node, true));
          } 
        } 
      } catch (ParserConfigurationException parserConfigurationException) {
      
      } catch (SAXException sAXException) {
      
      } catch (IOException iOException) {} 
    hA.sort(new f());
    hB.sort(new g());
    hC.sort(new h());
  }
  
  private e(k paramk, Element paramElement, boolean paramBoolean) {
    this.id = paramElement.getAttribute("id");
    this.hr = paramk;
    this.hs = paramBoolean;
    this.name = paramElement.getAttribute("name");
    if (paramBoolean) {
      if (paramk == k.hG) {
        this.ht = d.gW;
      } else {
        this.ht = d.valueOf("PROC_" + paramElement.getAttribute("category"));
      } 
    } else {
      this.ht = d.valueOf(paramElement.getAttribute("category"));
    } 
    this.special = paramElement.hasAttribute("special") ? Boolean.valueOf(paramElement.getAttribute("special")).booleanValue() : false;
    this.hu = paramElement.hasAttribute("chargeable") ? new Integer(paramElement.getAttribute("chargeable")) : null;
    this.hv = paramElement.getAttribute("subtitle");
    this.hw = paramElement.hasAttribute("cooking") ? Boolean.valueOf(paramElement.getAttribute("cooking")).booleanValue() : false;
    this.hx = paramElement.hasAttribute("icon") ? paramElement.getAttribute("icon") : null;
    if (paramElement.hasAttribute("multiplier")) {
      this.hy = Integer.parseInt(paramElement.getAttribute("multiplier"));
    } else {
      this.hy = 1;
    } 
    String str = null;
    NodeList nodeList = paramElement.getChildNodes();
    ArrayList<j> arrayList = new ArrayList();
    for (byte b = 0; b < nodeList.getLength(); b++) {
      Node node = nodeList.item(b);
      if (node instanceof Element) {
        paramElement = (Element)node;
        if (paramElement.getNodeName().equals("description")) {
          str = a(paramElement);
        } else if (paramElement.getNodeName().equals("requirement")) {
          arrayList.add(new j(this, paramElement, null));
        } 
      } 
    } 
    this.description = str;
    this.hz = arrayList.<j>toArray(new j[0]);
  }
  
  public String getID() {
    return this.id;
  }
  
  public k aC() {
    return this.hr;
  }
  
  public boolean aD() {
    return this.hs;
  }
  
  public String getName() {
    return this.name;
  }
  
  public d aE() {
    return this.ht;
  }
  
  public boolean aF() {
    return (!this.hs && this.ht != d.gY && this.ht != d.gX);
  }
  
  public boolean aG() {
    return (!this.hs && this.special);
  }
  
  public Integer aH() {
    return this.hu;
  }
  
  public String aI() {
    return this.hv;
  }
  
  public boolean aJ() {
    return this.hw;
  }
  
  public ImageIcon F(int paramInt) {
    switch (paramInt) {
      case 0:
        return (this.hx == null) ? null : Application.a(this.hx);
      case 3:
        return (this.hx == null) ? null : Application.a(this.hx, 20, 20);
      case 1:
        return (this.hx == null) ? null : Application.a(this.hx, 40, 40);
      case 2:
        return (this.hx == null) ? null : Application.a(this.hx, 80, 80);
    } 
    return null;
  }
  
  public ImageIcon d(int paramInt1, int paramInt2) {
    return (this.hx == null) ? null : Application.a(this.hx, paramInt1, paramInt2);
  }
  
  public int aK() {
    return this.hy;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public int aL() {
    return this.hz.length;
  }
  
  public j G(int paramInt) {
    return this.hz[paramInt];
  }
  
  public String toString() {
    return (this.name.length() == 0) ? this.id : this.name;
  }
  
  static String a(Element paramElement) {
    if (paramElement == null)
      throw new IllegalArgumentException(); 
    NodeList nodeList = paramElement.getChildNodes();
    StringBuffer stringBuffer = new StringBuffer();
    boolean bool = false;
    for (byte b = 0; b < nodeList.getLength(); b++) {
      Node node = nodeList.item(b);
      if (node.getNodeType() == 3) {
        stringBuffer.append(node.getNodeValue());
        bool = true;
      } 
    } 
    return !bool ? null : stringBuffer.toString();
  }
  
  public static e[] a(String paramString, boolean paramBoolean) {
    ArrayList<e> arrayList = new ArrayList();
    paramString = paramString.toUpperCase();
    for (e e1 : hA) {
      if (paramBoolean && !e1.hw)
        continue; 
      if (e1.name.toUpperCase().equals(paramString))
        return new e[] { e1 }; 
      if (e1.name.toUpperCase().indexOf(paramString) >= 0)
        arrayList.add(e1); 
    } 
    for (e e1 : hB) {
      if (paramBoolean && !e1.hw)
        continue; 
      if (e1.name.toUpperCase().equals(paramString))
        return new e[] { e1 }; 
      if (e1.name.toUpperCase().indexOf(paramString) >= 0)
        arrayList.add(e1); 
    } 
    if (!paramBoolean)
      for (e e1 : hC) {
        if (e1.name.toUpperCase().equals(paramString))
          return new e[] { e1 }; 
        if (e1.name.toUpperCase().indexOf(paramString) >= 0)
          arrayList.add(e1); 
      }  
    return arrayList.<e>toArray(new e[0]);
  }
  
  public static e[] a(k paramk, d paramd, boolean paramBoolean) {
    Iterator<e> iterator;
    if (paramk == k.hH) {
      iterator = hA.iterator();
    } else if (paramk == k.hG) {
      iterator = hB.iterator();
    } else if (paramk == k.hF) {
      iterator = hC.iterator();
    } else {
      return new e[0];
    } 
    ArrayList<e> arrayList = new ArrayList();
    while (iterator.hasNext()) {
      e e1 = iterator.next();
      if (e1.ht == paramd && (e1.hw || !paramBoolean))
        arrayList.add(e1); 
    } 
    return arrayList.<e>toArray(new e[0]);
  }
  
  public static e[] aM() {
    ArrayList<e> arrayList = new ArrayList();
    for (e e1 : hC) {
      if (!e1.hs && e1.ht != d.hl)
        arrayList.add(e1); 
    } 
    return arrayList.<e>toArray(new e[0]);
  }
  
  public static e[] aN() {
    ArrayList<e> arrayList = new ArrayList();
    for (e e1 : hB) {
      if (!e1.hs)
        arrayList.add(e1); 
    } 
    return arrayList.<e>toArray(new e[0]);
  }
  
  public static e v(String paramString) {
    int i;
    return ((i = hA.indexOf(new i(paramString))) >= 0) ? hA.get(i) : (((i = hB.indexOf(new i(paramString))) >= 0) ? hB.get(i) : (((i = hC.indexOf(new i(paramString))) >= 0) ? hC.get(i) : null));
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db\e.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */