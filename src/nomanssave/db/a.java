package nomanssave.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import nomanssave.structures.j;
import nomanssave.structures.k;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class a {
  final String id;
  
  final String name;
  
  final j gD;
  
  final String gE;
  
  final k gF;
  
  final boolean gG;
  
  final k[] gH;
  
  private static final List gI = new ArrayList();
  
  static {
    InputStream inputStream = a.class.getResourceAsStream("frigates.xml");
    if (inputStream != null)
      try {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
        Element element = document.getDocumentElement();
        NodeList nodeList = element.getChildNodes();
        for (byte b = 0; b < nodeList.getLength(); b++) {
          Node node = nodeList.item(b);
          if (node instanceof Element && node.getNodeName().equals("trait"))
            gI.add(new a((Element)node)); 
        } 
      } catch (ParserConfigurationException parserConfigurationException) {
      
      } catch (SAXException sAXException) {
      
      } catch (IOException iOException) {} 
    gI.sort(new b());
  }
  
  private a(Element paramElement) {
    this.id = paramElement.getAttribute("id");
    this.name = paramElement.getAttribute("name");
    String str1 = paramElement.getAttribute("type");
    this.gD = (str1 == null) ? null : j.valueOf(str1);
    this.gE = paramElement.getAttribute("strength");
    str1 = paramElement.getAttribute("primary");
    this.gF = (str1 == null) ? null : k.O(str1);
    this.gG = Boolean.parseBoolean(paramElement.getAttribute("beneficial"));
    String str2 = paramElement.getAttribute("secondary");
    ArrayList<k> arrayList = new ArrayList();
    int i = 0;
    while (i < str2.length()) {
      k k1;
      int m = str2.indexOf(",", i);
      if (m >= 0) {
        k1 = k.O(str2.substring(i, m));
        i = m + 1;
      } else {
        k1 = k.O(str2.substring(i));
        i = str2.length();
      } 
      if (k1 != null)
        arrayList.add(k1); 
    } 
    this.gH = arrayList.<k>toArray(new k[0]);
  }
  
  public String getID() {
    return this.id;
  }
  
  public String getName() {
    return this.name;
  }
  
  public j ay() {
    return this.gD;
  }
  
  public String az() {
    return this.gE;
  }
  
  public boolean aA() {
    return this.gG;
  }
  
  public String toString() {
    String str = (this.gD == j.iZ) ? "%" : "";
    return String.valueOf(this.name) + " (" + this.gE + str + " " + this.gD + ")";
  }
  
  public static a[] aB() {
    ArrayList<a> arrayList = new ArrayList();
    for (a a1 : gI) {
      if (a1.gF != null)
        arrayList.add(a1); 
    } 
    return arrayList.<a>toArray(new a[0]);
  }
  
  public static a a(k paramk) {
    for (a a1 : gI) {
      if (a1.gF == paramk)
        return a1; 
    } 
    return null;
  }
  
  public static a[] b(k paramk) {
    ArrayList<a> arrayList = new ArrayList();
    for (a a1 : gI) {
      for (byte b = 0; b < a1.gH.length; b++) {
        if (a1.gH[b] == paramk) {
          arrayList.add(a1);
          break;
        } 
      } 
    } 
    return arrayList.<a>toArray(new a[0]);
  }
  
  public static a u(String paramString) {
    int i = gI.indexOf(new c(paramString));
    return (i >= 0) ? gI.get(i) : null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db\a.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */