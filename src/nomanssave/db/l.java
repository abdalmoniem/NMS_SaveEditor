package nomanssave.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import nomanssave.util.n;

public class l {
  private static m hJ;
  
  private final m hK;
  
  private final List hL;
  
  static {
    InputStream inputStream = l.class.getResourceAsStream("jsonmap.txt");
    if (inputStream == null) {
      hJ = null;
    } else {
      try {
        hJ = new m(inputStream, null);
      } catch (IOException iOException) {
        n.error("Could not load key mapping file", iOException);
        hJ = null;
      } 
    } 
  }
  
  public static l w(String paramString) {
    return (paramString.equals("F2P") && hJ != null) ? new l(hJ) : null;
  }
  
  private l(m paramm) {
    this.hK = paramm;
    this.hL = new ArrayList();
  }
  
  public String x(String paramString) {
    int i = m.a(this.hK).indexOf(paramString);
    if (i < 0) {
      if (!this.hL.contains(paramString)) {
        n.warn("  name mapping not found: " + paramString);
        this.hL.add(paramString);
      } 
      return paramString;
    } 
    return m.b(this.hK).get(i);
  }
  
  public String y(String paramString) {
    int i = m.b(this.hK).indexOf(paramString);
    if (i < 0) {
      if (!this.hL.contains(paramString)) {
        n.warn("  reverse mapping not found: " + paramString);
        this.hL.add(paramString);
      } 
      return paramString;
    } 
    return m.a(this.hK).get(i);
  }
  
  public String toString() {
    return this.hK.toString();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db\l.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */