package nomanssave.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class n {
  final String id;
  
  final String text;
  
  private final HashMap hN;
  
  private static final List hO = new ArrayList();
  
  static {
    InputStream inputStream = n.class.getResourceAsStream("words.xml");
    if (inputStream != null)
      try {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
        Element element = document.getDocumentElement();
        NodeList nodeList = element.getChildNodes();
        for (byte b = 0; b < nodeList.getLength(); b++) {
          Node node = nodeList.item(b);
          if (node instanceof Element && node.getNodeName().equals("word"))
            hO.add(new n((Element)node)); 
        } 
      } catch (ParserConfigurationException parserConfigurationException) {
      
      } catch (SAXException sAXException) {
      
      } catch (IOException iOException) {} 
    hO.sort(new o());
  }
  
  private n(Element paramElement) {
    this.id = paramElement.getAttribute("id");
    this.text = paramElement.getAttribute("text");
    this.hN = new HashMap<>();
    NodeList nodeList = paramElement.getElementsByTagName("group");
    for (byte b = 0; b < nodeList.getLength(); b++) {
      Element element = (Element)nodeList.item(b);
      String str = element.getAttribute("group");
      p p = element.hasAttribute("race") ? p.valueOf(element.getAttribute("race")) : null;
      this.hN.put(str, p);
    } 
  }
  
  public String getID() {
    return this.id;
  }
  
  public String getText() {
    return this.text;
  }
  
  public Iterable aP() {
    return Collections.unmodifiableSet(this.hN.keySet());
  }
  
  public p z(String paramString) {
    return (p)this.hN.get(paramString);
  }
  
  public boolean a(p paramp) {
    return this.hN.containsValue(paramp);
  }
  
  public static n A(String paramString) {
    for (n n1 : hO) {
      if (n1.id.equals(paramString))
        return n1; 
    } 
    return null;
  }
  
  public static n B(String paramString) {
    for (n n1 : hO) {
      if (n1.hN.containsKey(paramString))
        return n1; 
    } 
    return null;
  }
  
  public static int aQ() {
    return hO.size();
  }
  
  public static n H(int paramInt) {
    return hO.get(paramInt);
  }
  
  public static Iterable aR() {
    return Collections.unmodifiableList(hO);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\db\n.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */