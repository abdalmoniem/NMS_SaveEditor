package nomanssave;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import nomanssave.db.e;
import nomanssave.structures.n;

class cm extends JPanel {
  private final int x;
  
  private final int y;
  
  private JCheckBoxMenuItem eg;
  
  private JMenuItem eh;
  
  private JMenuItem ei;
  
  private JMenuItem ej;
  
  private JMenuItem ek;
  
  private JMenuItem el;
  
  private JMenuItem em;
  
  private JMenuItem en;
  
  private JMenuItem eo;
  
  private JMenuItem ep;
  
  private cm(ci paramci, int paramInt1, int paramInt2) {
    this.x = paramInt1;
    this.y = paramInt2;
    setLayout(new GridBagLayout());
    JPopupMenu jPopupMenu = new JPopupMenu();
    this.eg = new JCheckBoxMenuItem("Enabled");
    this.eg.addActionListener(new cn(this, paramInt1, paramInt2));
    this.eg.setEnabled(!(!ci.a(paramci) && !dH.aw()));
    jPopupMenu.add(this.eg);
    this.eh = new JMenuItem("Enable All Slots");
    this.eh.addActionListener(new cq(this));
    this.eh.setEnabled(!(!ci.a(paramci) && !dH.aw()));
    jPopupMenu.add(this.eh);
    this.ei = new JMenuItem("Repair Slot");
    this.ei.addActionListener(new cr(this, paramInt1, paramInt2));
    this.ei.setVisible(ci.e(paramci));
    jPopupMenu.add(this.ei);
    this.ej = new JMenuItem("Repair All Slots");
    this.ej.addActionListener(new cs(this));
    this.ej.setVisible(ci.e(paramci));
    jPopupMenu.add(this.ej);
    jPopupMenu.addSeparator();
    this.ek = new JMenuItem("Item Details");
    this.ek.addActionListener(new ct(this, paramInt1, paramInt2));
    jPopupMenu.add(this.ek);
    this.el = new JMenuItem("Add Item");
    this.el.addActionListener(new cu(this, paramInt1, paramInt2));
    jPopupMenu.add(this.el);
    this.em = new JMenuItem("Repair Item");
    this.em.addActionListener(new cv(this, paramInt1, paramInt2));
    jPopupMenu.add(this.em);
    this.en = new JMenuItem("Move Item");
    this.en.addActionListener(new cw(this, paramInt1, paramInt2));
    jPopupMenu.add(this.en);
    this.eo = new JMenuItem("Fill Stack");
    this.eo.addActionListener(new cx(this, paramInt1, paramInt2));
    jPopupMenu.add(this.eo);
    this.ep = new JMenuItem("Delete Item");
    this.ep.addActionListener(new co(this, paramInt1, paramInt2));
    jPopupMenu.add(this.ep);
    setComponentPopupMenu(jPopupMenu);
    setBorder(new LineBorder(Color.DARK_GRAY));
    addMouseListener(new cp(this, paramInt1, paramInt2));
    al();
  }
  
  private boolean aj() {
    return ci.b(this.ef).j(this.x, this.y);
  }
  
  private boolean ak() {
    return ci.b(this.ef).m(this.x, this.y);
  }
  
  private void al() {
    removeAll();
    if (!ci.b(this.ef).j(this.x, this.y)) {
      this.eg.setSelected(false);
      this.ei.setEnabled(false);
      this.ek.setVisible(false);
      this.em.setVisible(false);
      this.el.setVisible(true);
      this.el.setEnabled(false);
      this.en.setVisible(false);
      this.eo.setVisible(false);
      this.ep.setVisible(false);
      setBackground(Color.GRAY);
      setToolTipText((String)null);
    } else if (ci.b(this.ef).m(this.x, this.y)) {
      this.eg.setSelected(true);
      this.ei.setEnabled(true);
      this.ek.setVisible(false);
      this.em.setVisible(false);
      this.el.setVisible(true);
      this.el.setEnabled(false);
      this.en.setVisible(false);
      this.eo.setVisible(false);
      this.ep.setVisible(false);
      setBackground(ci.ad());
      n n = ci.b(this.ef).h(this.x, this.y);
      if (n == null) {
        setToolTipText((String)null);
      } else {
        e e = e.v(n.getId());
        String str = (e == null) ? n.getId() : e.getName();
        ImageIcon imageIcon = (e == null) ? null : e.d(aO.bP, aO.bP);
        byte b = 0;
        if (imageIcon != null)
          a(imageIcon, b++); 
        Color color = (n.bS() == 0.0D) ? Color.BLACK : Color.RED;
        a(str, b++, color, false);
        a((n.bQ() < 0) ? "" : (String.valueOf(n.bQ()) + "/" + n.bR()), b++, color, true);
        setToolTipText(str);
      } 
    } else {
      this.eg.setSelected(true);
      this.ei.setEnabled(false);
      n n = ci.b(this.ef).h(this.x, this.y);
      if (n == null) {
        this.ek.setVisible(false);
        this.em.setVisible(false);
        this.el.setVisible(true);
        this.el.setEnabled(true);
        this.en.setVisible(false);
        this.eo.setVisible(false);
        this.ep.setVisible(false);
        setBackground(Color.WHITE);
        setToolTipText((String)null);
      } else {
        this.ek.setVisible(true);
        this.em.setVisible((n.bS() != 0.0D));
        this.el.setVisible(false);
        this.el.setEnabled(false);
        this.en.setVisible(true);
        this.eo.setVisible(false);
        this.ep.setVisible(true);
        String str1 = n.getType();
        if (str1.equals("Technology")) {
          setBackground(ci.ae());
          if (n.bQ() >= 0 && n.bQ() < n.bR()) {
            this.eo.setText("Recharge");
            this.eo.setVisible(true);
          } 
        } else if (str1.equals("Product")) {
          setBackground(ci.af());
          if (n.bR() > 1) {
            this.eo.setText("Fill Stack");
            this.eo.setVisible(true);
          } 
        } else if (str1.equals("Substance")) {
          setBackground(ci.ag());
          if (n.bR() > 1) {
            this.eo.setText("Fill Stack");
            this.eo.setVisible(true);
          } 
        } else {
          setBackground(ci.ah());
        } 
        e e = e.v(n.getId());
        this.ek.setEnabled((e != null));
        String str2 = (e == null) ? n.getId() : e.getName();
        ImageIcon imageIcon = (e == null) ? null : e.d(aO.bP, aO.bP);
        byte b = 0;
        if (imageIcon != null)
          a(imageIcon, b++); 
        Color color = (n.bS() == 0.0D) ? Color.BLACK : Color.RED;
        a(str2, b++, color, false);
        a((n.bQ() < 0) ? "" : (String.valueOf(n.bQ()) + "/" + n.bR()), b++, color, true);
        setToolTipText(str2);
      } 
    } 
    revalidate();
    updateUI();
  }
  
  private void a(ImageIcon paramImageIcon, int paramInt) {
    JLabel jLabel = new JLabel(paramImageIcon);
    jLabel.setPreferredSize(new Dimension(aO.bP, aO.bP));
    GridBagConstraints gridBagConstraints = new GridBagConstraints();
    gridBagConstraints.anchor = 10;
    gridBagConstraints.fill = 0;
    gridBagConstraints.insets = new Insets(5, 0, 5, 0);
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = paramInt;
    add(jLabel, gridBagConstraints);
  }
  
  private void a(String paramString, int paramInt, Color paramColor, boolean paramBoolean) {
    JLabel jLabel = new JLabel();
    jLabel.setFont(aO.bN);
    jLabel.setBackground((Color)null);
    jLabel.setBorder((Border)null);
    jLabel.setText(paramString);
    jLabel.setForeground(paramColor);
    GridBagConstraints gridBagConstraints = new GridBagConstraints();
    gridBagConstraints.anchor = 10;
    gridBagConstraints.fill = 0;
    if (paramBoolean)
      gridBagConstraints.weighty = 1.0D; 
    gridBagConstraints.insets = new Insets((paramInt == 0) ? (aO.bP + 10) : 0, 0, 0, 0);
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = paramInt;
    add(jLabel, gridBagConstraints);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\cm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */