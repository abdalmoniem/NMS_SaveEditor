package nomanssave;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class an extends JDialog {
  private at aO;
  
  private int aP;
  
  private int aQ;
  
  private at aR = null;
  
  private JTextField aS;
  
  private JTextField aT;
  
  private static an aU;
  
  private an(Frame paramFrame) {
    super(paramFrame);
    setResizable(false);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setTitle("Change Stack Sizes");
    setModal(true);
    JPanel jPanel1 = new JPanel();
    setContentPane(jPanel1);
    jPanel1.setLayout(new BorderLayout(0, 0));
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("100px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("250px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC }, new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, RowSpec.decode("20dlu"), FormFactory.LINE_GAP_ROWSPEC }));
    JLabel jLabel1 = new JLabel("Substances:");
    jPanel2.add(jLabel1, "2, 2, left, center");
    this.aS = new JTextField();
    this.aS.addFocusListener(new ao(this));
    jPanel2.add(this.aS, "4, 2, fill, default");
    JLabel jLabel2 = new JLabel("Products:");
    jPanel2.add(jLabel2, "2, 4, left, center");
    this.aT = new JTextField();
    this.aT.addFocusListener(new ap(this));
    jPanel2.add(this.aT, "4, 4, fill, default");
    JLabel jLabel3 = new JLabel("<html><font color=\"red\"><b>Please Note: No Man's Sky sometimes reverts these settings back to default.</b></font></html>");
    jPanel2.add(jLabel3, "2, 6, 3, 1, fill, center");
    jPanel1.add(jPanel2);
    JPanel jPanel3 = new JPanel();
    jPanel3.setLayout(new FlowLayout(2));
    jPanel1.add(jPanel3, "South");
    JButton jButton1 = new JButton("Save");
    jButton1.addActionListener(new aq(this));
    jPanel3.add(jButton1);
    getRootPane().setDefaultButton(jButton1);
    JButton jButton2 = new JButton("Cancel");
    jButton2.addActionListener(new ar(this));
    jPanel3.add(jButton2);
    getRootPane().registerKeyboardAction(new as(this), KeyStroke.getKeyStroke(27, 0), 2);
    pack();
  }
  
  private at a(at paramat, int paramInt1, int paramInt2) {
    this.aO = paramat;
    this.aP = paramInt1;
    this.aQ = paramInt2;
    this.aS.setText(Integer.toString(paramat.aW));
    this.aT.setText(Integer.toString(paramat.aX));
    this.aR = null;
    setLocationRelativeTo(getParent());
    setVisible(true);
    return this.aR;
  }
  
  public static at a(Container paramContainer, at paramat, int paramInt1, int paramInt2) {
    if (aU == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      aU = new an(frame);
    } 
    return aU.a(paramat, paramInt1, paramInt2);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\an.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */