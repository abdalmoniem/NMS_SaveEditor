package nomanssave;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import nomanssave.structures.o;

class dc implements ComboBoxModel {
  private o fS = null;
  
  dc(db paramdb) {}
  
  public int getSize() {
    return (db.a(this.fT) == null) ? 0 : (db.a(this.fT)).length;
  }
  
  public o A(int paramInt) {
    return db.a(this.fT)[paramInt];
  }
  
  public void addListDataListener(ListDataListener paramListDataListener) {}
  
  public void removeListDataListener(ListDataListener paramListDataListener) {}
  
  public void setSelectedItem(Object paramObject) {
    this.fS = (o)paramObject;
    if (this.fS == null) {
      db.b(this.fT).setText("");
      db.c(this.fT).setSelectedIndex(-1);
      db.d(this.fT).setText("");
      db.e(this.fT).setText("");
      db.f(this.fT).setText("");
      db.g(this.fT).setText("");
      db.h(this.fT).a(new ch[0]);
      return;
    } 
    db.b(this.fT).setText(this.fS.getName());
    db.c(this.fT).setSelectedItem(this.fS.bj());
    db.d(this.fT).setText(this.fS.bf());
    db.e(this.fT).setText(Double.toString(this.fS.bU()));
    db.f(this.fT).setText(Double.toString(this.fS.bV()));
    db.g(this.fT).setText(Double.toString(this.fS.bW()));
    db.h(this.fT).a(new ch[] { new dd(this) });
  }
  
  public Object getSelectedItem() {
    return this.fS;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dc.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */