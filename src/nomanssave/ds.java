package nomanssave;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import nomanssave.structures.v;
import nomanssave.util.h;

class ds implements ComboBoxModel {
  private v gq = null;
  
  ds(dr paramdr, Application paramApplication) {}
  
  public int getSize() {
    return (dr.a(this.gr) == null) ? 0 : (dr.a(this.gr)).length;
  }
  
  public v C(int paramInt) {
    return dr.a(this.gr)[paramInt];
  }
  
  public void addListDataListener(ListDataListener paramListDataListener) {}
  
  public void removeListDataListener(ListDataListener paramListDataListener) {}
  
  public void setSelectedItem(Object paramObject) {
    this.gq = (v)paramObject;
    if (this.gq == null) {
      dr.b(this.gr).setText("");
      dr.c(this.gr).setSelectedIndex(-1);
      dr.d(this.gr).setSelectedIndex(-1);
      dr.e(this.gr).setText("");
      dr.f(this.gr).setSelected(false);
      dr.f(this.gr).setEnabled(false);
      dr.g(this.gr).setEnabled(false);
      dr.h(this.gr).setText("");
      dr.i(this.gr).setText("");
      dr.j(this.gr).setText("");
      dr.k(this.gr).a(new ch[0]);
      return;
    } 
    dr.b(this.gr).setText(this.gq.getName());
    dr.c(this.gr).setSelectedItem(this.gq.cn());
    dr.d(this.gr).setSelectedItem(this.gq.bj());
    dr.e(this.gr).setText(this.gq.bf());
    h h = this.aL.d("PlayerStateData.ShipUsesLegacyColours");
    dr.f(this.gr).setSelected((h != null && h.ar(this.gq.getIndex())));
    dr.f(this.gr).setEnabled(true);
    dr.g(this.gr).setEnabled(true);
    dr.h(this.gr).setText(Double.toString(this.gq.bU()));
    dr.i(this.gr).setText(Double.toString(this.gq.co()));
    dr.j(this.gr).setText(Double.toString(this.gq.bm()));
    dr.k(this.gr).a(new ch[] { new dt(this), new du(this) });
    dr.l(this.gr).setEnabled(false);
    dr.m(this.gr).setEnabled(false);
    if (dr.n(this.gr) != null)
      for (byte b = 0; b < (dr.a(this.gr)).length; b++) {
        if (this.gq == dr.a(this.gr)[b] && b == dr.n(this.gr).cm()) {
          dr.l(this.gr).setEnabled(true);
          dr.m(this.gr).setEnabled(true);
        } 
      }  
  }
  
  public Object getSelectedItem() {
    return this.gq;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ds.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */