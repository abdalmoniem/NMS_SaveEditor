package nomanssave;

import java.awt.EventQueue;
import javax.swing.JOptionPane;
import nomanssave.storage.e;
import nomanssave.storage.o;
import nomanssave.storage.p;
import nomanssave.util.n;

class p implements e {
  p(Application paramApplication) {}
  
  public void e(String paramString) {
    if (paramString.startsWith("mf_")) {
      paramString = paramString.substring(3);
    } else {
      return;
    } 
    o o = (o)Application.a(this.am).getSelectedItem();
    if (o == null || !o.G(paramString))
      return; 
    p p1 = (Application.b(this.am) < 0) ? null : Application.c(this.am)[Application.b(this.am)];
    n.info(String.valueOf(o.toString()) + " modified externally (update)");
    if (p1 instanceof aa) {
      EventQueue.invokeLater(() -> {
            n.info("Updating file list...");
            synchronized (Application.a(this.am)) {
              p[] arrayOfP = paramo.aV();
              Application.a(this.am, new p[arrayOfP.length + 1]);
              Application.c(this.am)[0] = paramp;
              System.arraycopy(arrayOfP, 0, Application.c(this.am), 1, arrayOfP.length);
              Application.a(this.am, 0);
              Application.a(this.am, true);
            } 
            Application.d(this.am).updateUI();
            n.info("Finished.");
          });
    } else if (Application.b(this.am) == 0) {
      Application.a(this.am, () -> {
            int i = JOptionPane.showConfirmDialog(Application.e(this.am), "Save file has been modified externally. Would you like to reload?\nNOTE: All changes made in the editor will be lost.", "Reload File", 0);
            n.info("Updating file list...");
            if (i == 0) {
              synchronized (Application.a(this.am)) {
                Application.a(this.am, paramo.aV());
                Application.a(this.am, ((Application.c(this.am)).length > 0) ? 0 : -1);
              } 
              Application.f(this.am);
            } else {
              n.info("Creating temporary save file...");
              synchronized (Application.a(this.am)) {
                p[] arrayOfP = paramo.aV();
                Application.a(this.am, new p[arrayOfP.length + 1]);
                Application.c(this.am)[0] = new aa(this.am, paramp, (Application.g(this.am) == null) ? null : Application.g(this.am).cv());
                System.arraycopy(arrayOfP, 0, Application.c(this.am), 1, arrayOfP.length);
                Application.a(this.am, 0);
                Application.a(this.am, true);
              } 
              Application.d(this.am).updateUI();
            } 
            n.info("Finished.");
          });
    } else {
      EventQueue.invokeLater(() -> {
            n.info("Updating file list...");
            synchronized (Application.a(this.am)) {
              Application.a(this.am, paramo.aV());
              Application.a(this.am, -1);
              for (byte b = 0; b < (Application.c(this.am)).length; b++) {
                if (paramp != null && Application.c(this.am)[b].B().equals(paramp.B())) {
                  Application.a(this.am, b);
                  break;
                } 
              } 
              if (paramp != null && Application.b(this.am) < 0) {
                n.info("Creating temporary save file...");
                p[] arrayOfP = new p[(Application.c(this.am)).length + 1];
                arrayOfP[0] = new aa(this.am, paramp, (Application.g(this.am) == null) ? null : Application.g(this.am).cv());
                System.arraycopy(Application.c(this.am), 0, arrayOfP, 1, (Application.c(this.am)).length);
                Application.a(this.am, arrayOfP);
                Application.a(this.am, 0);
                Application.a(this.am, true);
              } 
            } 
            Application.d(this.am).updateUI();
            n.info("Finished.");
          });
    } 
  }
  
  public void f(String paramString) {
    if (paramString.startsWith("mf_")) {
      paramString = paramString.substring(3);
    } else {
      return;
    } 
    o o = (o)Application.a(this.am).getSelectedItem();
    if (o == null || !o.G(paramString))
      return; 
    p p1 = (Application.b(this.am) < 0) ? null : Application.c(this.am)[Application.b(this.am)];
    n.info(String.valueOf(o.toString()) + " modified externally (delete)");
    if (p1 instanceof aa) {
      EventQueue.invokeLater(() -> {
            n.info("Updating file list...");
            synchronized (Application.a(this.am)) {
              p[] arrayOfP = paramo.aV();
              Application.a(this.am, new p[arrayOfP.length + 1]);
              Application.c(this.am)[0] = paramp;
              System.arraycopy(arrayOfP, 0, Application.c(this.am), 1, arrayOfP.length);
              Application.a(this.am, 0);
              Application.a(this.am, true);
            } 
            Application.d(this.am).updateUI();
            n.info("Finished.");
          });
    } else if (p1 != null && paramString.equals(p1.B())) {
      Application.a(this.am, () -> {
            int i = JOptionPane.showConfirmDialog(Application.e(this.am), "Save file has been deleted externally. Would you like to reload?\nNOTE: All changes made in the editor will be lost.", "Reload File", 0);
            n.info("Updating file list...");
            if (i == 0) {
              synchronized (Application.a(this.am)) {
                Application.a(this.am, paramo.aV());
                Application.a(this.am, ((Application.c(this.am)).length > 0) ? 0 : -1);
              } 
              Application.f(this.am);
            } else {
              n.info("Creating temporary save file...");
              synchronized (Application.a(this.am)) {
                p[] arrayOfP = paramo.aV();
                Application.a(this.am, new p[arrayOfP.length + 1]);
                Application.c(this.am)[0] = new aa(this.am, paramp, (Application.g(this.am) == null) ? null : Application.g(this.am).cv());
                System.arraycopy(arrayOfP, 0, Application.c(this.am), 1, arrayOfP.length);
                Application.a(this.am, 0);
                Application.a(this.am, true);
              } 
              Application.d(this.am).updateUI();
            } 
            n.info("Finished.");
          });
    } else {
      EventQueue.invokeLater(() -> {
            n.info("Updating file list...");
            synchronized (Application.a(this.am)) {
              Application.a(this.am, paramo.aV());
              Application.a(this.am, -1);
              for (byte b = 0; b < (Application.c(this.am)).length; b++) {
                if (paramp != null && Application.c(this.am)[b].B().equals(paramp.B())) {
                  Application.a(this.am, b);
                  break;
                } 
              } 
              if (paramp != null && Application.b(this.am) < 0) {
                n.info("Creating temporary save file...");
                p[] arrayOfP = new p[(Application.c(this.am)).length + 1];
                arrayOfP[0] = new aa(this.am, paramp, (Application.g(this.am) == null) ? null : Application.g(this.am).cv());
                System.arraycopy(Application.c(this.am), 0, arrayOfP, 1, (Application.c(this.am)).length);
                Application.a(this.am, arrayOfP);
                Application.a(this.am, 0);
                Application.a(this.am, true);
              } 
            } 
            Application.d(this.am).updateUI();
            n.info("Finished.");
          });
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\p.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */