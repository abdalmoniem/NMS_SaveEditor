package nomanssave;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import nomanssave.structures.f;
import nomanssave.structures.g;
import nomanssave.structures.m;

public class bm extends dG {
  private static final double cC = 1000.0D;
  
  private ab cD;
  
  private JComboBox cE;
  
  private JComboBox cF;
  
  private ab cG;
  
  private ab cH;
  
  private ab cI;
  
  private JTextField aD;
  
  private JButton aE;
  
  private JButton aF;
  
  private ci cJ;
  
  private f cK;
  
  bm(Application paramApplication) {
    n("Freighter");
    this.cD = new bn(this, paramApplication);
    a("Name", this.cD);
    this.cE = new JComboBox();
    this.cE.setModel(new bp(this, paramApplication));
    a("Type", this.cE);
    this.cF = new JComboBox();
    this.cF.setModel(new bq(this, paramApplication));
    a("Class", this.cF);
    this.cG = new br(this, paramApplication);
    a("Home Seed", this.cG);
    this.cH = new bs(this, paramApplication);
    a("Model Seed", this.cH);
    n("Base Stats");
    this.cI = new bt(this, paramApplication);
    a("Hyperdrive", this.cI);
    Q();
    n("Base Info");
    this.aD = new JTextField();
    this.aD.setEnabled(false);
    a("Items", this.aD);
    JPanel jPanel = new JPanel();
    this.aE = new JButton("Backup");
    this.aE.addActionListener(new bu(this, paramApplication));
    jPanel.add(this.aE);
    this.aF = new JButton("Restore");
    this.aF.addActionListener(new bv(this, paramApplication));
    jPanel.add(this.aF);
    a(jPanel);
    this.cJ = new ci(paramApplication);
    b(this.cJ);
  }
  
  void q() {
    this.cJ.q();
  }
  
  void r() {
    this.cJ.r();
  }
  
  void s() {
    this.cJ.s();
  }
  
  void u() {
    this.cJ.u();
  }
  
  void a(m paramm) {
    this.cJ.a(paramm);
  }
  
  f R() {
    return this.cK;
  }
  
  void a(f paramf) {
    if (paramf == null) {
      this.cK = null;
      this.cD.setText("");
      this.cE.setSelectedIndex(-1);
      this.cE.updateUI();
      this.cF.setSelectedIndex(-1);
      this.cG.setText("");
      this.cH.setText("");
      this.cI.setText("");
      this.aD.setText("");
      this.aE.setEnabled(false);
      this.aF.setEnabled(false);
      this.cJ.a(new ch[0]);
    } else {
      this.cK = paramf;
      this.cD.setText(paramf.getName());
      this.cE.setSelectedIndex(-1);
      this.cE.updateUI();
      this.cE.setSelectedItem(paramf.bg());
      this.cF.setSelectedItem(paramf.bj());
      this.cG.setText(paramf.bh());
      this.cH.setText(paramf.bi());
      this.cI.setText(Double.toString(paramf.bm()));
      g g = paramf.bn();
      if (g == null) {
        this.aD.setText("");
        this.aE.setEnabled(false);
        this.aF.setEnabled(false);
      } else {
        this.aD.setText(Integer.toString(g.bb()));
        this.aE.setEnabled(true);
        this.aF.setEnabled(true);
      } 
      this.cJ.a(new ch[] { new bw(this, paramf), new bo(this, paramf) });
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\bm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */