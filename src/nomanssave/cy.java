package nomanssave;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.LayoutManager;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import nomanssave.db.e;
import nomanssave.db.j;
import nomanssave.structures.n;
import nomanssave.util.n;
import nomanssave.util.p;

public class cy extends JDialog {
  private JTextField et;
  
  private JLabel eu;
  
  private JTextField ev;
  
  private JTextField ew;
  
  private JLabel ex;
  
  private JTextField ey;
  
  private ab ez;
  
  private JLabel eA;
  
  private ab eB;
  
  private JTextField eC;
  
  private JTextField eD;
  
  private JTextArea eE;
  
  private JTextArea eF;
  
  private e eG;
  
  private n eH;
  
  private Integer eI;
  
  private Integer eJ;
  
  public static cy eK = null;
  
  private cy(Frame paramFrame) {
    super(paramFrame);
    setSize(600, 480);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setTitle("Item Details");
    setModal(true);
    JPanel jPanel1 = new JPanel();
    jPanel1.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("100px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("100px"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC }, new RowSpec[] { 
            FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, 
            FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, RowSpec.decode("64px"), FormFactory.LINE_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.LINE_GAP_ROWSPEC }));
    jPanel1.add(new JLabel("Type:"), "2, 2, left, center");
    this.et = new JTextField();
    this.et.setEditable(false);
    jPanel1.add(this.et, "4, 2, fill, default");
    this.eu = new JLabel("");
    jPanel1.add(this.eu, "6, 2, 1, 7, center, fill");
    jPanel1.add(new JLabel("Category:"), "2, 4, left, center");
    this.ev = new JTextField();
    this.ev.setEditable(false);
    jPanel1.add(this.ev, "4, 4, fill, default");
    jPanel1.add(new JLabel("Name:"), "2, 6, left, center");
    this.ew = new JTextField();
    this.ew.setEditable(false);
    jPanel1.add(this.ew, "4, 6, fill, default");
    jPanel1.add(new JLabel("ID:"), "2, 8, left, center");
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { ColumnSpec.decode("default:grow"), FormFactory.DEFAULT_COLSPEC, ColumnSpec.decode("100px") }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC }));
    this.ey = new JTextField();
    this.ey.setEditable(false);
    jPanel2.add(this.ey, "1, 1");
    this.ex = new JLabel("#");
    jPanel2.add(this.ex, "2, 1");
    this.ez = new cz(this);
    this.ez.setEditable(false);
    jPanel2.add(this.ez, "3, 1");
    jPanel1.add(jPanel2, "4, 8, fill, default");
    this.eA = new JLabel("Quantity:");
    jPanel1.add(this.eA, "2, 10, left, center");
    jPanel2 = new JPanel();
    jPanel2.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { ColumnSpec.decode("100px"), FormFactory.DEFAULT_COLSPEC, ColumnSpec.decode("100px") }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC }));
    this.eB = new cA(this);
    this.eB.setEditable(false);
    jPanel2.add(this.eB, "1, 1");
    jPanel2.add(new JLabel("/"), "2, 1");
    this.eC = new JTextField();
    this.eC.setEditable(false);
    jPanel2.add(this.eC, "3, 1");
    jPanel1.add(jPanel2, "4, 10, fill, default");
    jPanel1.add(new JLabel("Subtitle:"), "2, 12, left, center");
    this.eD = new JTextField();
    this.eD.setEditable(false);
    jPanel1.add(this.eD, "4, 12, 3, 1, fill, default");
    jPanel1.add(new JLabel("Build Cost:"), "2, 14, left, top");
    JScrollPane jScrollPane = new JScrollPane();
    jScrollPane.setBorder(this.eD.getBorder());
    jScrollPane.setBackground(this.eD.getBackground());
    this.eE = new JTextArea();
    this.eE.setEditable(false);
    this.eE.setBorder((Border)null);
    this.eE.setBackground((Color)null);
    this.eE.setFont(this.eD.getFont());
    jScrollPane.setViewportView(this.eE);
    jPanel1.add(jScrollPane, "4, 14, 3, 1, fill, fill");
    jPanel1.add(new JLabel("Description:"), "2, 16, left, top");
    this.eF = new JTextArea();
    this.eF.setEditable(false);
    this.eF.setWrapStyleWord(true);
    this.eF.setLineWrap(true);
    this.eF.setBorder(this.eD.getBorder());
    this.eF.setBackground(this.eD.getBackground());
    this.eF.setFont(this.eD.getFont());
    jPanel1.add(this.eF, "4, 16, 3, 1, fill, fill");
    setContentPane(jPanel1);
    getRootPane().registerKeyboardAction(new cB(this), KeyStroke.getKeyStroke(27, 0), 2);
    addWindowListener(new cC(this));
  }
  
  private void a(n paramn, e parame) {
    this.eH = paramn;
    this.eG = parame;
    this.eI = null;
    this.eJ = null;
    this.et.setText(paramn.getType());
    this.eu.setIcon((parame == null) ? null : parame.F(2));
    String str = paramn.getId();
    if (parame != null && str.startsWith(String.valueOf(parame.getID()) + "#")) {
      String str1 = str.substring(parame.getID().length() + 1);
      this.ey.setText(parame.getID());
      this.ez.setText(str1);
      this.ex.setVisible(true);
      this.ez.setVisible(true);
      try {
        int i = p.b(str1, 0, 99999);
        this.eI = new Integer(i);
        this.ez.setEditable(true);
      } catch (RuntimeException runtimeException) {
        n.warn("Error detected in item id: " + str);
        this.eI = null;
        this.ez.setEditable(false);
      } 
    } else {
      this.ey.setText(str);
      this.ez.setText("");
      this.ex.setVisible(false);
      this.ez.setVisible(false);
    } 
    if (paramn.getType().equals("Technology") && paramn.bQ() >= 0 && paramn.bQ() < paramn.bR()) {
      this.eA.setText("Charge:");
      this.eJ = Integer.valueOf(paramn.bQ());
      this.eB.setText(Integer.toString(paramn.bQ()));
      this.eC.setText(Integer.toString(paramn.bR()));
      this.eB.setEditable(true);
    } else if ((paramn.getType().equals("Product") || paramn.getType().equals("Substance")) && paramn.bR() > 1) {
      this.eA.setText("Quantity:");
      this.eJ = Integer.valueOf(paramn.bQ());
      this.eB.setText(Integer.toString(paramn.bQ()));
      this.eC.setText(Integer.toString(paramn.bR()));
      this.eB.setEditable(true);
    } else {
      this.eA.setText("Quantity:");
      this.eB.setText("1");
      this.eC.setText("1");
      this.eB.setEditable(false);
    } 
    this.ew.setText((parame == null) ? "[Unknown]" : parame.getName());
    this.ev.setText((parame == null) ? "[Unknown]" : parame.aE().toString());
    this.eD.setText((parame == null) ? "" : parame.aI());
    StringBuffer stringBuffer = new StringBuffer();
    if (parame == null || parame.aL() == 0) {
      stringBuffer.append("N/A");
    } else {
      for (byte b = 0; b < parame.aL(); b++) {
        j j = parame.G(b);
        if (b > 0)
          stringBuffer.append("\n"); 
        e e1 = e.v(j.getID());
        if (e1 != null) {
          stringBuffer.append(String.valueOf(e1.getName()) + " (x" + j.aO() + ")");
        } else {
          stringBuffer.append(String.valueOf(j.getID()) + " (x" + j.aO() + ")");
        } 
      } 
    } 
    this.eE.setText(stringBuffer.toString());
    this.eE.setCaretPosition(0);
    this.eF.setText((parame == null) ? "" : parame.getDescription());
    setLocationRelativeTo(getParent());
    setVisible(true);
  }
  
  public static void a(Container paramContainer, n paramn) {
    e e1 = e.v(paramn.getId());
    if (eK == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      eK = new cy(frame);
    } 
    eK.a(paramn, e1);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\cy.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */