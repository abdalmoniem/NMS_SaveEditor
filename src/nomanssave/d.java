package nomanssave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import nomanssave.db.e;

class d implements ActionListener {
  d(c paramc) {}
  
  public void actionPerformed(ActionEvent paramActionEvent) {
    String str = c.a(this.n).getText();
    boolean bool = ((c.b(this.n) & 0x100) != 0) ? true : false;
    e[] arrayOfE = e.a(str, bool);
    if (arrayOfE.length == 0) {
      c.c(this.n).setSelectedIndex(-1);
      c.d(this.n).setSelectedIndex(-1);
      c.e(this.n).setSelectedIndex(-1);
      c.c(this.n).updateUI();
      c.d(this.n).updateUI();
      c.e(this.n).updateUI();
      JOptionPane.showOptionDialog(this.n, "Item not found.", "Warning", 0, 2, null, new Object[] { "OK" }, null);
    } else if (arrayOfE.length == 1) {
      c.c(this.n).setSelectedItem(arrayOfE[0].aC());
      c.d(this.n).setSelectedItem(arrayOfE[0].aE());
      c.e(this.n).setSelectedItem(arrayOfE[0]);
      c.c(this.n).updateUI();
      c.d(this.n).updateUI();
      c.e(this.n).updateUI();
    } else {
      c.c(this.n).setSelectedItem(arrayOfE[0].aC());
      c.d(this.n).setSelectedItem(arrayOfE[0].aE());
      c.e(this.n).setSelectedIndex(-1);
      for (byte b = 1; b < arrayOfE.length; b++) {
        if (arrayOfE[0].aC() != arrayOfE[b].aC()) {
          c.c(this.n).setSelectedIndex(-1);
          c.d(this.n).setSelectedIndex(-1);
          break;
        } 
        if (arrayOfE[0].aE() != arrayOfE[b].aE())
          c.d(this.n).setSelectedIndex(-1); 
      } 
      c.c(this.n).updateUI();
      c.d(this.n).updateUI();
      c.e(this.n).updateUI();
      JOptionPane.showOptionDialog(this.n, "Multiple items found. Please refine your search.", "Warning", 0, 2, null, new Object[] { "OK" }, null);
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\d.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */