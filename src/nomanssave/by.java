package nomanssave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

class by implements ActionListener {
  by(bx parambx, Application paramApplication) {}
  
  public void actionPerformed(ActionEvent paramActionEvent) {
    if (bx.b(this.dB) < 0)
      return; 
    if (JOptionPane.showConfirmDialog(this.dB, "Are you sure you want to delete this frigate?", "Delete", 2) != 0)
      return; 
    bx.a(this.dB, this.aL.h(bx.c(this.dB)[bx.b(this.dB)].getIndex()));
    if ((bx.c(this.dB)).length > 0) {
      bx.e(this.dB).setRowSelectionInterval(0, 0);
    } else {
      bx.e(this.dB).clearSelection();
    } 
    bx.e(this.dB).updateUI();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\by.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */