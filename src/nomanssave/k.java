package nomanssave;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import nomanssave.db.e;

public class k extends JDialog {
  private final JTable r;
  
  private final TableRowSorter s;
  
  private e[] t;
  
  private ArrayList u = null;
  
  private static k v = null;
  
  private k(Frame paramFrame) {
    super(paramFrame);
    setSize(aO.bL * 2, aO.bL + aO.bK);
    setResizable(false);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setModal(true);
    JPanel jPanel1 = new JPanel();
    setContentPane(jPanel1);
    jPanel1.setLayout(new BorderLayout(0, 0));
    JScrollPane jScrollPane = new JScrollPane();
    this.r = new JTable();
    this.r.setShowHorizontalLines(false);
    this.r.setShowVerticalLines(false);
    this.r.setFont(aO.bG);
    this.r.setRowHeight(aO.bJ);
    this.r.setSelectionMode(2);
    this.r.setModel(new l(this));
    this.r.getColumnModel().getColumn(0).setMaxWidth(24);
    this.s = new TableRowSorter<>(this.r.getModel());
    this.s.setSortable(0, false);
    this.r.setRowSorter(this.s);
    jScrollPane.setViewportView(this.r);
    jPanel1.add(jScrollPane);
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout(new FlowLayout(2));
    jPanel1.add(jPanel2, "South");
    JButton jButton1 = new JButton("Add");
    jButton1.addActionListener(new m(this));
    jPanel2.add(jButton1);
    getRootPane().setDefaultButton(jButton1);
    JButton jButton2 = new JButton("Cancel");
    jButton2.addActionListener(new n(this));
    jPanel2.add(jButton2);
    getRootPane().registerKeyboardAction(new o(this), KeyStroke.getKeyStroke(27, 0), 2);
  }
  
  private String[] c() {
    this.r.clearSelection();
    this.s.setSortKeys(Collections.emptyList());
    this.s.sort();
    this.r.updateUI();
    this.u = null;
    setLocationRelativeTo(getParent());
    setVisible(true);
    return (this.u == null) ? new String[0] : (String[])this.u.toArray((Object[])new String[0]);
  }
  
  public static String[] b(Container paramContainer) {
    if (v == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      v = new k(frame);
    } 
    v.t = e.aM();
    v.setTitle("Add Known Technologies");
    return v.c();
  }
  
  public static String[] c(Container paramContainer) {
    if (v == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      v = new k(frame);
    } 
    v.t = e.aN();
    v.setTitle("Add Known Products");
    return v.c();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\k.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */