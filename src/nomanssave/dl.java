package nomanssave;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import nomanssave.storage.b;

public class dl extends JDialog {
  private final JList fV;
  
  private dq[] fW;
  
  private int fC;
  
  private static dl fX = null;
  
  private dl(Frame paramFrame) {
    super(paramFrame);
    setSize(300, 200);
    setResizable(false);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setTitle("Save File As");
    setModal(true);
    JPanel jPanel1 = new JPanel();
    setContentPane(jPanel1);
    jPanel1.setLayout(new BorderLayout(0, 0));
    JScrollPane jScrollPane = new JScrollPane();
    this.fV = new JList();
    this.fV.setSelectionMode(0);
    this.fV.setModel(new dm(this));
    jScrollPane.setViewportView(this.fV);
    jPanel1.add(jScrollPane);
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout(new FlowLayout(2));
    jPanel1.add(jPanel2, "South");
    JButton jButton1 = new JButton("Replace/Save");
    jButton1.addActionListener(new dn(this));
    jPanel2.add(jButton1);
    getRootPane().setDefaultButton(jButton1);
    JButton jButton2 = new JButton("Cancel");
    jButton2.addActionListener(new do(this));
    jPanel2.add(jButton2);
    getRootPane().registerKeyboardAction(new dp(this), KeyStroke.getKeyStroke(27, 0), 2);
  }
  
  private int a(b paramb, int paramInt) {
    this.fW = new dq[5];
    for (byte b1 = 0; b1 < this.fW.length; b1++)
      this.fW[b1] = new dq(this, b1, paramb.I(b1)); 
    this.fV.updateUI();
    this.fV.setSelectedIndex(paramInt);
    this.fC = -1;
    setLocationRelativeTo(getParent());
    setVisible(true);
    return this.fC;
  }
  
  public static int a(Container paramContainer, b paramb, int paramInt) {
    if (fX == null) {
      Frame frame = JOptionPane.getFrameForComponent(paramContainer);
      fX = new dl(frame);
    } 
    return fX.a(paramb, paramInt);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */