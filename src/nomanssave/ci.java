package nomanssave;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import nomanssave.db.d;
import nomanssave.db.e;
import nomanssave.structures.m;
import nomanssave.structures.n;
import nomanssave.util.n;

public class ci extends JScrollPane implements dI {
  private static final Color dQ = new Color(255, 240, 240);
  
  private static final Color dR = new Color(255, 255, 240);
  
  private static final Color dS = new Color(240, 255, 250);
  
  private static final Color dT = new Color(240, 250, 255);
  
  private static final Color dU = new Color(240, 255, 255);
  
  private final Application cQ;
  
  private final boolean dV;
  
  private final boolean dW;
  
  private final JPanel dX;
  
  private final JComboBox dY;
  
  private final JButton dZ;
  
  private final JButton ea;
  
  private ch[] eb;
  
  private m ec;
  
  ci(Application paramApplication) {
    this(paramApplication, true, true);
  }
  
  ci(Application paramApplication, boolean paramBoolean1, boolean paramBoolean2) {
    this.cQ = paramApplication;
    this.dV = paramBoolean1;
    this.dW = paramBoolean2;
    JPanel jPanel1 = new JPanel();
    jPanel1.setLayout(new BorderLayout());
    JPanel jPanel2 = new JPanel();
    jPanel2.setLayout(new FlowLayout());
    this.dX = new JPanel();
    this.dX.setLayout(new GridBagLayout());
    jPanel1.setPreferredSize(new Dimension(aO.bO * 8 + 20, aO.bO * 6 + 50));
    jPanel1.add(jPanel2, "North");
    jPanel1.add(this.dX, "Center");
    setViewportView(jPanel1);
    this.eb = new ch[0];
    this.dY = new JComboBox();
    this.dY.setVisible(false);
    this.dY.setModel(new cj(this));
    jPanel2.add(this.dY);
    this.dZ = new JButton("Resize Inventory");
    this.dZ.setVisible(false);
    this.dZ.addActionListener(new ck(this, paramApplication));
    jPanel2.add(this.dZ);
    this.ea = new JButton("Change Stack Size");
    this.ea.setVisible(false);
    this.ea.addActionListener(new cl(this, paramApplication));
    jPanel2.add(this.ea);
    dH.a(this);
  }
  
  public void a(boolean paramBoolean) {
    ch ch1 = (ch)this.dY.getSelectedItem();
    this.dZ.setVisible((ch1 == null) ? false : (!(!paramBoolean && !ch1.G())));
    this.ea.setVisible((ch1 == null) ? false : (!(!paramBoolean && !ch1.aa())));
    synchronized (this.dX.getTreeLock()) {
      for (byte b = 0; b < this.dX.getComponentCount(); b++) {
        Component component = this.dX.getComponent(b);
        if (component instanceof cm) {
          cm cm = (cm)component;
          cm.b(cm).setEnabled(!(!paramBoolean && !this.dV));
          cm.f(cm).setEnabled(!(!paramBoolean && !this.dV));
        } 
      } 
    } 
  }
  
  void a(m paramm) {
    if (this.ec == paramm)
      ac(); 
  }
  
  void q() {
    boolean bool = false;
    for (byte b = 0; b < this.eb.length; b++) {
      m m1 = this.eb[b].F();
      if (m1 != null && m1.bM()) {
        n.info(m1 + ": technology recharged");
        if (this.ec == m1)
          ac(); 
        bool = true;
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void r() {
    boolean bool = false;
    for (byte b = 0; b < this.eb.length; b++) {
      m m1 = this.eb[b].F();
      if (m1 != null && m1.bN()) {
        n.info(m1 + ": items refilled");
        if (this.ec == m1)
          ac(); 
        bool = true;
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void s() {
    if (!this.dV)
      return; 
    boolean bool = false;
    for (byte b = 0; b < this.eb.length; b++) {
      m m1 = this.eb[b].F();
      if (m1 != null && m1.bO()) {
        n.info(m1 + ": all slots enabled");
        if (this.ec == m1)
          ac(); 
        bool = true;
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void t() {
    if (!this.dW)
      return; 
    boolean bool = false;
    for (byte b = 0; b < this.eb.length; b++) {
      m m1 = this.eb[b].F();
      if (m1 != null && m1.bL()) {
        n.info(m1 + ": all slots repaired");
        if (this.ec == m1)
          ac(); 
        bool = true;
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void u() {
    boolean bool = false;
    for (byte b = 0; b < this.eb.length; b++) {
      m m1 = this.eb[b].F();
      if (m1 != null) {
        if (m1.bE()) {
          n.info(m1 + ": inventory expanded");
          if (this.ec == m1)
            ac(); 
          bool = true;
        } 
      } else if (this.eb[b].O()) {
        m1 = this.eb[b].P();
        if (m1 != null) {
          n.info(m1 + ": inventory expanded");
          if (this.dY.getSelectedIndex() == b) {
            this.ec = m1;
            ac();
          } 
          bool = true;
        } 
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void ab() {
    int i = this.dY.getSelectedIndex();
    if (i >= 0) {
      this.ec = this.eb[i].F();
      ac();
    } 
  }
  
  private void ac() {
    synchronized (this.dX.getTreeLock()) {
      this.dX.removeAll();
      if (this.ec != null) {
        Dimension dimension = new Dimension(aO.bO, aO.bO);
        for (byte b = 0; b < this.ec.getHeight(); b++) {
          for (byte b1 = 0; b1 < this.ec.getWidth(); b1++) {
            cm cm = new cm(this, b1, b, null);
            cm.setMinimumSize(dimension);
            cm.setMaximumSize(dimension);
            cm.setPreferredSize(dimension);
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.fill = 1;
            gridBagConstraints.insets = new Insets(-1, -1, 0, 0);
            gridBagConstraints.gridx = b1;
            gridBagConstraints.gridy = b;
            this.dX.add(cm, gridBagConstraints);
          } 
        } 
      } 
    } 
    this.dX.revalidate();
    this.dX.updateUI();
  }
  
  void a(ch[] paramArrayOfch) {
    this.eb = paramArrayOfch;
    this.ec = null;
    this.dY.updateUI();
    if (paramArrayOfch.length == 0) {
      this.dY.setVisible(false);
      this.dZ.setVisible(false);
      this.ea.setVisible(false);
      ac();
    } else {
      this.dY.setVisible((paramArrayOfch.length != 1));
      this.dZ.setVisible(false);
      this.ea.setVisible(false);
      this.dY.setSelectedIndex(0);
    } 
  }
  
  private cm b(int paramInt1, int paramInt2) {
    synchronized (this.dX.getTreeLock()) {
      for (byte b = 0; b < this.dX.getComponentCount(); b++) {
        Component component = this.dX.getComponent(b);
        if (component instanceof cm) {
          cm cm = (cm)component;
          if (cm.g(cm) == paramInt1 && cm.h(cm) == paramInt2)
            return cm; 
        } 
      } 
    } 
    return null;
  }
  
  private void a(cm paramcm) {
    e e = c.a(this, this.ec.bD());
    if (e != null) {
      this.ec.a(cm.g(paramcm), cm.h(paramcm), e);
      n.info(this.ec + ": item added " + cm.g(paramcm) + "," + cm.h(paramcm) + "," + e.getID());
      this.cQ.f();
      cm.c(paramcm);
    } 
  }
  
  private void a(n paramn, cm paramcm) {
    char c;
    e e = e.v(paramn.getId());
    if (e == null) {
      if ("Product".equals(paramn.getType()) || "Substance".equals(paramn.getType())) {
        c = '';
      } else {
        this.cQ.c("Item details not found!");
        return;
      } 
    } else {
      switch (ai()[e.aE().ordinal()]) {
        case 21:
        case 22:
          c = '\001';
          break;
        case 17:
        case 18:
          c = '\004';
          break;
        case 19:
        case 20:
          c = '\002';
          break;
        case 25:
        case 26:
          c = '\020';
          break;
        case 27:
        case 28:
          c = ' ';
          break;
        case 24:
          c = '\b';
          break;
        default:
          c = '';
          break;
      } 
    } 
    List<m> list = this.cQ.f(c);
    int i = list.indexOf(this.ec);
    int j = cV.a(this, list, i);
    if (j != i) {
      m m1 = list.get(j);
      n.info("Moving item from " + this.ec + " to " + m1);
      if (this.ec.a(cm.g(paramcm), cm.h(paramcm), m1)) {
        this.cQ.f();
        cm.c(paramcm);
        this.cQ.a(m1);
      } 
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ci.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */