package nomanssave;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import nomanssave.structures.m;
import nomanssave.structures.u;
import nomanssave.structures.v;
import nomanssave.util.n;

public class dr extends dG {
  private static final int bS = 500;
  
  private static final int bT = 200;
  
  private static final double fF = 1000.0D;
  
  private static final double gb = 1000.0D;
  
  private static final double cC = 1000.0D;
  
  private final Application cQ;
  
  private JComboBox gc;
  
  private ab gd;
  
  private JComboBox ge;
  
  private JComboBox gf;
  
  private ab gg;
  
  private JCheckBox gh;
  
  private JButton cS;
  
  private ab gi;
  
  private ab gj;
  
  private ab gk;
  
  private ab gl;
  
  private ab gm;
  
  private ci gn;
  
  private v[] go;
  
  private u gp;
  
  dr(Application paramApplication) {
    this.cQ = paramApplication;
    this.gc = new JComboBox();
    this.gc.setModel(new ds(this, paramApplication));
    a("Ship", true, this.gc);
    this.gd = new dy(this, paramApplication);
    a("Name", this.gd);
    this.ge = new JComboBox();
    this.ge.setModel(new dz(this, paramApplication));
    a("Type", this.ge);
    this.gf = new JComboBox();
    this.gf.setModel(new dA(this, paramApplication));
    a("Class", this.gf);
    this.gg = new dB(this, paramApplication);
    a("Seed", this.gg);
    this.gh = new JCheckBox("Use Old Colours");
    this.gh.setEnabled(false);
    this.gh.addActionListener(new dC(this, paramApplication));
    a((String)null, this.gh);
    n("Base Stats");
    this.gi = new dD(this, paramApplication);
    a("Health", this.gi);
    this.gj = new dE(this, paramApplication);
    a("Shield", this.gj);
    this.gk = new dF(this, paramApplication);
    a("Damage", this.gk);
    this.gl = new dv(this, paramApplication);
    a("Shields", this.gl);
    this.gm = new dw(this, paramApplication);
    a("Hyperdrive", this.gm);
    Q();
    JPanel jPanel = new JPanel();
    this.cS = new JButton("Delete Ship");
    this.cS.addActionListener(new dx(this, paramApplication));
    jPanel.add(this.cS);
    a(jPanel);
    this.gn = new ci(paramApplication);
    b(this.gn);
  }
  
  void q() {
    boolean bool = false;
    for (byte b = 0; b < this.go.length; b++) {
      m m = this.go[b].F();
      if (m.bM()) {
        n.info(m + ": technology recharged");
        bool = true;
      } 
      this.gn.a(m);
      if ((m = this.go[b].bk()) != null) {
        if (m.bM()) {
          n.info(m + ": technology recharged");
          bool = true;
        } 
        this.gn.a(m);
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void r() {
    boolean bool = false;
    for (byte b = 0; b < this.go.length; b++) {
      m m = this.go[b].F();
      if (m.bN()) {
        n.info(m + ": items refilled");
        bool = true;
      } 
      this.gn.a(m);
      if ((m = this.go[b].bk()) != null) {
        if (m.bN()) {
          n.info(m + ": items refilled");
          bool = true;
        } 
        this.gn.a(m);
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void s() {
    boolean bool = false;
    for (byte b = 0; b < this.go.length; b++) {
      m m = this.go[b].F();
      if (m.bO()) {
        n.info(m + ": all slots enabled");
        bool = true;
      } 
      this.gn.a(m);
      if ((m = this.go[b].bk()) != null) {
        if (m.bO()) {
          n.info(m + ": all slots enabled");
          bool = true;
        } 
        this.gn.a(m);
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void t() {
    boolean bool = false;
    for (byte b = 0; b < this.go.length; b++) {
      m m = this.go[b].F();
      if (m.bL()) {
        n.info(m + ": all slots repaired");
        bool = true;
      } 
      this.gn.a(m);
      if ((m = this.go[b].bk()) != null) {
        if (m.bL()) {
          n.info(m + ": all slots repaired");
          bool = true;
        } 
        this.gn.a(m);
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void u() {
    boolean bool = false;
    for (byte b = 0; b < this.go.length; b++) {
      m m = this.go[b].F();
      if (m.bE()) {
        n.info(m + ": inventory expanded");
        bool = true;
      } 
      this.gn.a(m);
      if ((m = this.go[b].bk()) != null) {
        if (m.bE()) {
          n.info(m + ": inventory expanded");
          bool = true;
        } 
        this.gn.a(m);
      } else {
        m = this.go[b].bl();
        n.info(m + ": inventory created");
        bool = true;
        this.gn.ab();
      } 
    } 
    if (bool)
      this.cQ.f(); 
  }
  
  void a(m paramm) {
    this.gn.a(paramm);
  }
  
  v[] av() {
    return this.go;
  }
  
  void a(v[] paramArrayOfv, u paramu) {
    this.go = paramArrayOfv;
    this.gp = paramu;
    if (paramArrayOfv.length == 0) {
      this.gc.setSelectedIndex(-1);
    } else {
      byte b = (paramu == null) ? 0 : paramu.cm();
      if (b >= paramArrayOfv.length)
        b = 0; 
      this.gc.setSelectedIndex(b);
    } 
    if (paramu == null) {
      this.gi.setText("");
      this.gj.setText("");
    } else {
      this.gi.setText(Long.toString(paramu.cb()));
      this.gj.setText(Long.toString(paramu.cc()));
    } 
    this.gc.updateUI();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dr.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */