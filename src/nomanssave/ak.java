package nomanssave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import nomanssave.structures.c;
import nomanssave.structures.d;
import nomanssave.util.n;

class ak implements ActionListener {
  ak(ad paramad, Application paramApplication) {}
  
  public void actionPerformed(ActionEvent paramActionEvent) {
    c c = (c)ad.j(this.aK).getSelectedItem();
    if (c == null)
      return; 
    List<d> list = c.bd();
    if (list.size() == 0) {
      this.aL.c("Cannot move base computer.\nPlease ensure that your base has a suitable Signal Booster / Blueprint Analyser / Beacon placed where you want your base computer to be.");
      return;
    } 
    int i;
    if ((i = cQ.a(this.aL.e(), list)) < 0)
      return; 
    d d = list.get(i);
    n.info("Attempting to swap base computer with " + d.toString() + "...");
    if (c.a(d)) {
      n.info("Base computer relocated: " + c.getName());
      this.aL.f();
    } else {
      n.info("Base computer not moved.");
      this.aL.c("An error occurred while attempting to move base computer.");
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ak.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */