package nomanssave;

import java.io.File;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileView;
import nomanssave.storage.b;
import nomanssave.storage.o;
import nomanssave.util.s;

class q extends FileView {
  q(Application paramApplication, ImageIcon paramImageIcon1, ImageIcon paramImageIcon2) {}
  
  private String b(File paramFile) {
    String str = paramFile.getName();
    if (!str.startsWith("st_"))
      return null; 
    try {
      long l = Long.parseLong(str.substring(3));
      return s.g(l);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  public Icon getIcon(File paramFile) {
    if (paramFile.isFile())
      return this.an; 
    String str = b(paramFile);
    return (str == null) ? null : this.ao;
  }
  
  public String getName(File paramFile) {
    if (paramFile.isFile()) {
      b b = Application.a(this.am, paramFile.getParentFile());
      o o = b.E(paramFile.getName());
      return (o == null) ? paramFile.getName() : ("[" + o.toString() + "] " + paramFile.getName());
    } 
    String str = b(paramFile);
    return (str == null) ? paramFile.getName() : ("[" + str + "] " + paramFile.getName());
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\q.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */