package nomanssave;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import nomanssave.util.n;

class ck implements ActionListener {
  ck(ci paramci, Application paramApplication) {}
  
  public void actionPerformed(ActionEvent paramActionEvent) {
    if (ci.b(this.ef) != null) {
      Dimension dimension = aZ.a(this.ef, ci.b(this.ef).getSize(), ci.b(this.ef).bF(), ci.b(this.ef).bG());
      if (dimension != null && ci.b(this.ef).a(dimension)) {
        n.info(ci.b(this.ef) + ": resized to " + dimension.width + "x" + dimension.height);
        ci.d(this.ef);
        this.aL.f();
      } 
      return;
    } 
    ch ch = (ch)ci.i(this.ef).getSelectedItem();
    if (ch != null && ch.O()) {
      Dimension dimension1 = new Dimension(1, 1);
      Dimension dimension2 = new Dimension(8, 6);
      Dimension dimension3 = aZ.a(this.ef, dimension1, dimension1, dimension2);
      if (dimension3 != null) {
        ci.a(this.ef, ch.a(dimension3.width, dimension3.height));
        n.info(ci.b(this.ef) + ": created " + dimension3.width + "x" + dimension3.height);
        ci.d(this.ef);
        this.aL.f();
      } 
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ck.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */