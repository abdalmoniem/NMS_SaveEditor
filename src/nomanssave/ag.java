package nomanssave;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import nomanssave.structures.c;

class ag implements ComboBoxModel {
  c aM = null;
  
  ag(ad paramad) {}
  
  public int getSize() {
    return (ad.a(this.aK) == null) ? 0 : ad.a(this.aK).ba();
  }
  
  public c l(int paramInt) {
    return ad.a(this.aK).N(paramInt);
  }
  
  public void addListDataListener(ListDataListener paramListDataListener) {}
  
  public void removeListDataListener(ListDataListener paramListDataListener) {}
  
  public void setSelectedItem(Object paramObject) {
    this.aM = (c)paramObject;
    if (this.aM == null) {
      ad.e(this.aK).setText("");
      ad.f(this.aK).setText("");
      ad.f(this.aK).setEnabled(false);
      ad.g(this.aK).setEnabled(false);
      ad.h(this.aK).setEnabled(false);
      ad.i(this.aK).setEnabled(false);
    } else {
      ad.e(this.aK).setText(Integer.toString(this.aM.bb()));
      ad.f(this.aK).setText(this.aM.getName());
      ad.f(this.aK).setEnabled(true);
      ad.g(this.aK).setEnabled(true);
      ad.h(this.aK).setEnabled(true);
      ad.i(this.aK).setEnabled(true);
    } 
  }
  
  public Object getSelectedItem() {
    return this.aM;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ag.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */