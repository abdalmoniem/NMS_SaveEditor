package nomanssave;

import nomanssave.structures.o;
import nomanssave.util.n;
import nomanssave.util.p;

class dh extends ab {
  dh(db paramdb, Application paramApplication) {}
  
  protected String i(String paramString) {
    o o = (o)db.i(this.fT).getSelectedItem();
    if (o == null)
      return ""; 
    double d = o.bU();
    try {
      double d1 = p.a(paramString, 0.0D, 1000.0D);
      if (d1 != d) {
        n.info("Setting weapon base damage: " + d1);
        o.c(d1);
        this.aL.f();
      } 
      return Double.toString(d1);
    } catch (RuntimeException runtimeException) {
      return Double.toString(d);
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\dh.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */