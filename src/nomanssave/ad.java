package nomanssave;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import nomanssave.structures.b;
import nomanssave.structures.m;

public class ad extends dG {
  private JComboBox ay = new JComboBox();
  
  private JTextField az;
  
  private ab aA;
  
  private JComboBox aB;
  
  private ab aC;
  
  private JTextField aD;
  
  private JButton aE;
  
  private JButton aF;
  
  private JButton aG;
  
  private ci aH;
  
  private b aI;
  
  ad(Application paramApplication) {
    this.ay.setModel(new ae(this));
    a("Base NPC", true, this.ay);
    this.az = new JTextField();
    this.az.setEnabled(false);
    a("Race", this.az);
    this.aA = new af(this, paramApplication);
    this.aA.setEnabled(false);
    a("Seed", this.aA);
    Q();
    this.aB = new JComboBox();
    this.aB.setModel(new ag(this));
    a("Base Info", true, this.aB);
    this.aC = new ah(this, paramApplication);
    a("Name", this.aC);
    this.aD = new JTextField();
    this.aD.setEnabled(false);
    a("Items", this.aD);
    JPanel jPanel = new JPanel();
    this.aE = new JButton("Backup");
    this.aE.addActionListener(new ai(this, paramApplication));
    jPanel.add(this.aE);
    this.aF = new JButton("Restore");
    this.aF.addActionListener(new aj(this, paramApplication));
    jPanel.add(this.aF);
    this.aG = new JButton("Move Base Computer");
    this.aG.addActionListener(new ak(this, paramApplication));
    jPanel.add(this.aG);
    a(jPanel);
    this.aH = new ci(paramApplication);
    b(this.aH);
  }
  
  void q() {
    this.aH.q();
  }
  
  void r() {
    this.aH.r();
  }
  
  void s() {
    this.aH.s();
  }
  
  void u() {
    this.aH.u();
  }
  
  void a(m paramm) {
    this.aH.a(paramm);
  }
  
  b E() {
    return this.aI;
  }
  
  void a(b paramb) {
    ch[] arrayOfCh;
    this.aI = paramb;
    if (paramb == null) {
      arrayOfCh = new ch[0];
      this.ay.setSelectedIndex(-1);
      this.aB.setSelectedIndex(-1);
    } else {
      arrayOfCh = new ch[paramb.aX() + 1];
      for (byte b1 = 0; b1 < paramb.aX(); b1++)
        arrayOfCh[b1] = new am(this, b1); 
      arrayOfCh[paramb.aX()] = new al(this, paramb);
      this.ay.setSelectedIndex((paramb.aZ() == 0) ? -1 : 0);
      this.aB.setSelectedIndex((paramb.ba() == 0) ? -1 : 0);
    } 
    this.ay.updateUI();
    this.aB.updateUI();
    this.aH.a(arrayOfCh);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\ad.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */