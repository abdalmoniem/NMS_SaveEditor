package nomanssave;

import nomanssave.structures.l;
import nomanssave.util.n;
import nomanssave.util.p;

class cc extends ab {
  final l dL;
  
  final int type;
  
  cc(bX parambX, l paraml, boolean paramBoolean) {
    this.dL = paraml;
    switch (Z()[paraml.ordinal()]) {
      case 24:
        this.type = 1;
        break;
      default:
        this.type = 0;
        break;
    } 
    setEnabled(paramBoolean);
  }
  
  protected String i(String paramString) {
    String str;
    if (bX.a(this.dK) == null)
      return ""; 
    switch (this.type) {
      case 0:
        str = Integer.toString(bX.a(this.dK).a(this.dL));
        break;
      case 1:
        str = Double.toString(bX.a(this.dK).b(this.dL));
        break;
      default:
        return "";
    } 
    if (paramString.equals(str))
      return paramString; 
    try {
      int i;
      double d;
      switch (this.type) {
        case 0:
          i = p.b(paramString, 0, 2147483647);
          n.info("Setting global stat (" + this.dL.getID() + "): " + i);
          bX.a(this.dK).a(this.dL, i);
          paramString = Integer.toString(i);
          break;
        case 1:
          d = Double.parseDouble(paramString);
          n.info("Setting global stat (" + this.dL.getID() + "): " + d);
          bX.a(this.dK).a(this.dL, d);
          paramString = Double.toString(d);
          break;
      } 
      bX.a(this.dK, this.dL, paramString);
      if (this.dL == l.jG || this.dL == l.jH) {
        i = bX.a(this.dK).a(l.jG) + bX.a(this.dK).a(l.jH);
        n.info("Setting global stat (" + l.jM.getID() + "): " + i);
        bX.a(this.dK).a(l.jM, i);
        bX.a(this.dK, l.jM, Integer.toString(i));
      } 
      if (this.dL == l.jD || this.dL == l.jF || this.dL == l.jE) {
        i = bX.a(this.dK).a(l.jD) + bX.a(this.dK).a(l.jF) + bX.a(this.dK).a(l.jE);
        n.info("Setting global stat (" + l.jN.getID() + "): " + i);
        bX.a(this.dK).a(l.jN, i);
        bX.a(this.dK, l.jN, Integer.toString(i));
      } 
      bX.b(this.dK).f();
      return paramString;
    } catch (RuntimeException runtimeException) {
      return str;
    } 
  }
  
  void Y() {
    String str;
    if (bX.a(this.dK) == null) {
      str = "";
    } else {
      switch (this.type) {
        case 0:
          str = Integer.toString(bX.a(this.dK).a(this.dL));
          break;
        case 1:
          str = Double.toString(bX.a(this.dK).b(this.dL));
          break;
        default:
          str = "";
          break;
      } 
    } 
    setText(str);
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\cc.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */