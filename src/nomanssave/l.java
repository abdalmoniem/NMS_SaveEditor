package nomanssave;

import javax.swing.ImageIcon;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

class l implements TableModel {
  l(k paramk) {}
  
  public void addTableModelListener(TableModelListener paramTableModelListener) {}
  
  public Class getColumnClass(int paramInt) {
    return (Class)((paramInt == 0) ? ImageIcon.class : String.class);
  }
  
  public int getColumnCount() {
    return 4;
  }
  
  public String getColumnName(int paramInt) {
    switch (paramInt) {
      case 0:
        return "";
      case 1:
        return "Name";
      case 2:
        return "Category";
      case 3:
        return "ID";
    } 
    return null;
  }
  
  public int getRowCount() {
    return (k.a(this.w) == null) ? 0 : (k.a(this.w)).length;
  }
  
  public Object getValueAt(int paramInt1, int paramInt2) {
    switch (paramInt2) {
      case 0:
        return k.a(this.w)[paramInt1].F(3);
      case 1:
        return k.a(this.w)[paramInt1].getName();
      case 2:
        return k.a(this.w)[paramInt1].aE().toString();
      case 3:
        return k.a(this.w)[paramInt1].getID();
    } 
    return null;
  }
  
  public boolean isCellEditable(int paramInt1, int paramInt2) {
    return false;
  }
  
  public void removeTableModelListener(TableModelListener paramTableModelListener) {}
  
  public void setValueAt(Object paramObject, int paramInt1, int paramInt2) {}
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\l.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */