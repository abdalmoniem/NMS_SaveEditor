package nomanssave.storage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import nomanssave.util.n;

public class d implements p {
  private final File ij;
  
  private Integer ik;
  
  private d(b paramb, String paramString) {
    this.ij = new File(b.b(paramb), paramString);
    this.ik = null;
  }
  
  public String B() {
    return this.ij.getName();
  }
  
  public long lastModified() {
    return this.ij.lastModified();
  }
  
  public String C() {
    ZipFile zipFile = new ZipFile(this.ij);
    try {
      ZipEntry zipEntry = zipFile.getEntry("saveinfo.txt");
      if (zipEntry == null)
        throw new IOException("Invalid backup file"); 
      Properties properties = new Properties();
      properties.load(zipFile.getInputStream(zipEntry));
      String str1 = properties.getProperty("ManifestFile");
      String str2 = properties.getProperty("StorageFile");
      if (str1 == null || str2 == null)
        throw new IOException("Invalid backup file"); 
      try {
        this.ik = new Integer(properties.getProperty("ArchiveNumber"));
      } catch (NumberFormatException numberFormatException) {
        throw new IOException("Invalid backup file");
      } 
      zipEntry = zipFile.getEntry(str1);
      if (zipEntry == null)
        throw new IOException("Invalid backup file"); 
      InputStream inputStream = zipFile.getInputStream(zipEntry);
      int j = 0;
      byte[] arrayOfByte = new byte[1024];
      int i;
      while ((i = inputStream.read(arrayOfByte, j, arrayOfByte.length - j)) >= 0)
        j += i; 
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrayOfByte, 0, j);
      zipEntry = zipFile.getEntry(str2);
      if (zipEntry == null)
        throw new IOException("Invalid backup file"); 
    } finally {
      zipFile.close();
    } 
  }
  
  public String g(String paramString) {
    String str1;
    String str2;
    int i;
    if (this.ik == null)
      throw new IOException("Unspecified archive number"); 
    switch (this.ik.intValue()) {
      case 0:
        str1 = "save.hg";
        str2 = "save2.hg";
        i = 1;
        break;
      case 1:
        str1 = "save2.hg";
        str2 = "save.hg";
        i = 0;
        break;
      case 2:
      case 4:
      case 6:
      case 8:
        str1 = "save" + (this.ik.intValue() + 1) + ".hg";
        str2 = "save" + (this.ik.intValue() + 2) + ".hg";
        i = this.ik.intValue() + 1;
        break;
      case 3:
      case 5:
      case 7:
      case 9:
        str1 = "save" + (this.ik.intValue() + 1) + ".hg";
        str2 = "save" + this.ik.intValue() + ".hg";
        i = this.ik.intValue() - 1;
        break;
      default:
        throw new IOException("Invalid archive number: " + this.ik.intValue());
    } 
    File file1 = new File(b.a(this.ih), "mf_" + str1);
    File file2 = new File(b.a(this.ih), str1);
    File file3 = new File(b.a(this.ih), "mf_" + str2);
    File file4 = new File(b.a(this.ih), str2);
    long l = System.currentTimeMillis();
    if (file2.exists()) {
      String str;
      n.info("Creating backup...");
      if (this.ik.intValue() == 0) {
        str = "backup." + l + ".zip";
      } else {
        str = "backup" + (this.ik.intValue() + 1) + "." + l + ".zip";
      } 
      b.a(this.ih, this.ik.intValue(), file2, file1, str);
      l++;
    } 
    if (file4.exists()) {
      String str;
      n.info("Creating backup...");
      if (this.ik.intValue() == 0) {
        str = "backup." + l + ".zip";
      } else {
        str = "backup" + (this.ik.intValue() + 1) + "." + l + ".zip";
      } 
      b.a(this.ih, i, file4, file3, str);
    } 
    n.info("Writing new save file...");
    b.b(this.ih, this.ik.intValue(), file2, file1, paramString);
    file4.delete();
    file3.delete();
    n.info("Finished.");
    return file2.getName();
  }
  
  public String toString() {
    return this.ij.getName();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\storage\d.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */