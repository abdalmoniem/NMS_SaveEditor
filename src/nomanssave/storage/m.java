package nomanssave.storage;

import java.io.File;
import java.io.FileInputStream;
import nomanssave.util.n;

public class m implements p {
  private final int io;
  
  private File ip;
  
  private File iq;
  
  private m(b paramb, int paramInt, String paramString) {
    this.io = paramInt;
    this.ip = new File(b.a(paramb), "mf_" + paramString);
    this.iq = new File(b.a(paramb), paramString);
  }
  
  private m(b paramb, int paramInt, File paramFile1, File paramFile2) {
    this.io = paramInt;
    this.ip = paramFile1;
    this.iq = paramFile2;
  }
  
  public String B() {
    return this.iq.getName();
  }
  
  public long lastModified() {
    return this.ip.lastModified();
  }
  
  public String C() {
    FileInputStream fileInputStream = new FileInputStream(this.ip);
    try {
      FileInputStream fileInputStream1 = new FileInputStream(this.iq);
    } finally {
      fileInputStream.close();
    } 
  }
  
  public String g(String paramString) {
    if (this.iq.exists()) {
      String str;
      n.info("Creating backup...");
      if (this.io == 0) {
        str = "backup." + System.currentTimeMillis() + ".zip";
      } else {
        str = "backup" + (this.io + 1) + "." + System.currentTimeMillis() + ".zip";
      } 
      b.a(this.ih, this.io, this.iq, this.ip, str);
    } 
    n.info("Writing new save file...");
    b.b(this.ih, this.io, this.iq, this.ip, paramString);
    n.info("Finished.");
    return this.iq.getName();
  }
  
  public String toString() {
    return this.iq.getName();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\storage\m.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */