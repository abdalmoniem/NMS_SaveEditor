package nomanssave.storage;

import nomanssave.util.n;

public enum a {
  hW, hX, hY, hZ;
  
  public static a I(int paramInt) {
    if (paramInt == 0)
      return null; 
    switch (paramInt & 0xF00) {
      case 512:
        return hW;
      case 1024:
        return hY;
      case 1536:
        return hX;
      case 2560:
        return hZ;
    } 
    n.info("Version unknown: " + Integer.toString(paramInt));
    return null;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\storage\a.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */