package nomanssave.storage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import nomanssave.util.i;
import nomanssave.util.k;
import nomanssave.util.n;

public class b {
  private final File ib;
  
  private final File ic;
  
  private ArrayList ie;
  
  private n if;
  
  private e ig;
  
  public b(File paramFile1, File paramFile2) {
    this.ib = paramFile1;
    this.ic = paramFile2;
    this.ie = new ArrayList();
    for (byte b1 = 0; b1 < 5; b1++) {
      int i = b1 * 2;
      File file = new File(paramFile1, "save" + ((i == 0) ? "" : (String)Integer.valueOf(i + 1)) + ".hg");
      if (!file.exists()) {
        file = new File(paramFile1, "save" + (i + 2) + ".hg");
        if (!file.exists())
          continue; 
      } 
      this.ie.add(new f(this, b1, null));
      continue;
    } 
    if (!paramFile2.exists()) {
      n.info("Moving backup files...");
      paramFile2.mkdir();
      paramFile1.listFiles(new c(this, paramFile2));
    } 
    this.ig = null;
    this.if = new n(this);
  }
  
  public void close() {
    this.ie.clear();
    this.if.interrupt();
  }
  
  public void a(e parame) {
    synchronized (this) {
      this.ig = parame;
    } 
  }
  
  private void C(String paramString) {
    synchronized (this) {
      if (this.ig != null)
        this.ig.f(paramString); 
    } 
  }
  
  private void D(String paramString) {
    synchronized (this) {
      if (this.ig != null)
        this.ig.e(paramString); 
    } 
  }
  
  public File aS() {
    return this.ib;
  }
  
  public File aT() {
    return this.ic;
  }
  
  public int aU() {
    return this.ie.size();
  }
  
  public o J(int paramInt) {
    return this.ie.get(paramInt);
  }
  
  public a I(int paramInt) {
    for (o o : this.ie) {
      if (o instanceof f && f.a((f)o) == paramInt)
        return ((f)o).x(); 
    } 
    return null;
  }
  
  public int a(o paramo) {
    for (o o1 : this.ie) {
      if (o1 instanceof f && o1 == paramo)
        return f.a((f)o1); 
    } 
    return -1;
  }
  
  public o K(int paramInt) {
    int i = 0;
    for (byte b1 = 0; b1 < this.ie.size(); b1++) {
      o o = this.ie.get(b1);
      if (o instanceof f) {
        if (f.a((f)o) == paramInt) {
          f.b((f)o);
          return o;
        } 
        if (f.a((f)o) < paramInt)
          i = b1 + 1; 
      } 
    } 
    f f = new f(this, paramInt, null);
    if (i == this.ie.size()) {
      this.ie.add(f);
    } else {
      this.ie.add(i, f);
    } 
    return f;
  }
  
  public o E(String paramString) {
    for (o o : this.ie) {
      if (o.G(paramString))
        return o; 
    } 
    return null;
  }
  
  public boolean F(String paramString) {
    for (o o : this.ie) {
      if (o.F(paramString))
        return true; 
    } 
    return false;
  }
  
  private static int c(File paramFile) {
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte = new byte[1024];
      FileInputStream fileInputStream = new FileInputStream(paramFile);
      try {
        int i;
        while ((i = fileInputStream.read(arrayOfByte)) >= 0)
          byteArrayOutputStream.write(arrayOfByte, 0, i); 
      } finally {
        fileInputStream.close();
      } 
      arrayOfByte = byteArrayOutputStream.toByteArray();
      if (arrayOfByte.length == 0)
        return 0; 
      String str = new String(arrayOfByte, 0, arrayOfByte.length - 1, "UTF-8");
      Object object = k.b(str, "Version");
      return (object == null && !(object instanceof Number)) ? 0 : ((Number)object).intValue();
    } catch (IOException iOException) {
      return 0;
    } catch (i i) {
      return 0;
    } 
  }
  
  private static a d(File paramFile) {
    return a.I(c(paramFile));
  }
  
  private static a e(File paramFile) {
    try {
      ZipFile zipFile = new ZipFile(paramFile);
      try {
        ZipEntry zipEntry = zipFile.getEntry("saveinfo.txt");
        if (zipEntry == null)
          throw new IOException("Invalid backup file"); 
        Properties properties = new Properties();
        properties.load(zipFile.getInputStream(zipEntry));
        String str = properties.getProperty("GameMode");
        return (str == null) ? null : a.valueOf(str);
      } finally {
        zipFile.close();
      } 
    } catch (IOException iOException) {
      return null;
    } 
  }
  
  private void a(int paramInt, File paramFile1, File paramFile2, String paramString) {
    a a = d(paramFile1);
    Properties properties = new Properties();
    properties.setProperty("ArchiveNumber", Integer.toString(paramInt));
    properties.setProperty("ManifestFile", paramFile2.getName());
    properties.setProperty("StorageFile", paramFile1.getName());
    properties.setProperty("LastModified", Long.toString(paramFile1.lastModified()));
    if (a != null)
      properties.setProperty("GameMode", a.toString()); 
    File file = new File(this.ic, paramString);
    ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file));
    try {
      byte[] arrayOfByte = new byte[1024];
      ZipEntry zipEntry = new ZipEntry(paramFile2.getName());
      zipOutputStream.putNextEntry(zipEntry);
      FileInputStream fileInputStream1 = new FileInputStream(paramFile2);
      try {
        int i;
        while ((i = fileInputStream1.read(arrayOfByte)) >= 0)
          zipOutputStream.write(arrayOfByte, 0, i); 
      } finally {
        fileInputStream1.close();
      } 
      zipEntry = new ZipEntry(paramFile1.getName());
      zipOutputStream.putNextEntry(zipEntry);
      FileInputStream fileInputStream2 = new FileInputStream(paramFile1);
      try {
        int i;
        while ((i = fileInputStream2.read(arrayOfByte)) >= 0)
          zipOutputStream.write(arrayOfByte, 0, i); 
      } finally {
        fileInputStream2.close();
      } 
      zipEntry = new ZipEntry("saveinfo.txt");
      zipOutputStream.putNextEntry(zipEntry);
      properties.store(zipOutputStream, "");
    } finally {
      zipOutputStream.close();
    } 
    file.setLastModified(paramFile2.lastModified());
  }
  
  private void b(int paramInt, File paramFile1, File paramFile2, String paramString) {
    FileOutputStream fileOutputStream = new FileOutputStream(paramFile2);
    try {
      FileOutputStream fileOutputStream1 = new FileOutputStream(paramFile1);
      try {
        q.a(fileOutputStream, fileOutputStream1, paramInt, paramString);
      } finally {
        fileOutputStream1.close();
      } 
    } finally {
      fileOutputStream.close();
    } 
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\storage\b.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */