package nomanssave.storage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import nomanssave.util.r;

public class q {
  private static final int ir = 2000;
  
  private static final int is = 2001;
  
  static String a(InputStream paramInputStream1, InputStream paramInputStream2, int paramInt, Object paramObject) {
    r r = a(paramInputStream1, paramInt);
    byte[] arrayOfByte = a(r, paramInputStream2, paramObject);
    return new String(arrayOfByte, 0, arrayOfByte.length - 1, "UTF-8");
  }
  
  static void a(OutputStream paramOutputStream1, OutputStream paramOutputStream2, int paramInt, String paramString) {
    byte[] arrayOfByte1 = paramString.getBytes("UTF-8");
    byte[] arrayOfByte2 = new byte[arrayOfByte1.length + 1];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, arrayOfByte1.length);
    arrayOfByte2[arrayOfByte1.length] = 0;
    r r = a(paramInt, arrayOfByte2);
    a(paramOutputStream1, r);
    a(paramOutputStream2, arrayOfByte2);
  }
  
  private static r a(int paramInt, byte[] paramArrayOfbyte) {
    r r = new r(paramInt);
    MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
    r.iu = messageDigest.digest(paramArrayOfbyte);
    r.it = 2001;
    r.iv = a(r.iu, paramArrayOfbyte);
    return r;
  }
  
  private static long[] a(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    byteArrayOutputStream.write(paramArrayOfbyte1, 0, paramArrayOfbyte1.length);
    byteArrayOutputStream.write(paramArrayOfbyte2, 0, paramArrayOfbyte2.length);
    long[] arrayOfLong = { 96176015842230784L, -8446744073709551617L };
    r.a(byteArrayOutputStream.toByteArray(), arrayOfLong);
    return arrayOfLong;
  }
  
  private static void a(OutputStream paramOutputStream, r paramr) {
    long l1 = (paramr.io + 2) ^ 0x1422CB8CL;
    long l2 = rotateLeft(l1, 13) * 5L + 3864292196L & 0xFFFFFFFFL;
    byte[] arrayOfByte1 = "NAESEVADNAYRTNRG".getBytes("US-ASCII");
    for (byte b1 = 0; b1 < 4; b1++)
      arrayOfByte1[b1] = (byte)(int)(l2 >> b1 * 8); 
    long[] arrayOfLong1 = a(arrayOfByte1);
    long[] arrayOfLong2 = new long[26];
    arrayOfLong2[0] = 4008636094L;
    arrayOfLong2[1] = paramr.it;
    arrayOfLong2[2] = paramr.iv[0] & 0xFFFFFFFFL;
    arrayOfLong2[3] = paramr.iv[0] >>> 32L & 0xFFFFFFFFL;
    arrayOfLong2[4] = paramr.iv[1] & 0xFFFFFFFFL;
    arrayOfLong2[5] = paramr.iv[1] >>> 32L & 0xFFFFFFFFL;
    for (byte b2 = 0; b2 < paramr.iu.length; b2 += 4)
      arrayOfLong2[6 + b2 / 4] = paramr.iu[b2] & 0xFFL | (paramr.iu[b2 + 1] & 0xFFL) << 8L | (paramr.iu[b2 + 2] & 0xFFL) << 16L | (paramr.iu[b2 + 3] & 0xFFL) << 24L; 
    long l3 = 0L;
    long l4 = 0L;
    for (byte b3 = 0; b3 < 8; b3++) {
      l3 += -1640531527L;
      int i = (int)(l3 >> 2L & 0x3L);
      byte b4 = 0;
      long l = 0L;
      byte b5 = 0;
      while (b5 < 25) {
        l = arrayOfLong2[b4 + 1] >> 3L ^ (l4 & 0xFFFFFFFL) << 4L;
        l += arrayOfLong2[b4 + 1] * 4L & 0xFFFFFFFFL ^ l4 >> 5L;
        l ^= (l4 ^ arrayOfLong1[b5 & 0x3 ^ i]) + (arrayOfLong2[b4 + 1] ^ l3);
        arrayOfLong2[b4] = arrayOfLong2[b4] + l & 0xFFFFFFFFL;
        l4 = arrayOfLong2[b4];
        b5++;
        b4++;
      } 
      l = arrayOfLong2[0] >> 3L ^ (l4 & 0xFFFFFFFL) << 4L;
      l += arrayOfLong2[0] * 4L & 0xFFFFFFFFL ^ l4 >> 5L;
      l ^= (l4 ^ arrayOfLong1[i ^ 0x1]) + (arrayOfLong2[0] ^ l3);
      arrayOfLong2[arrayOfLong2.length - 1] = arrayOfLong2[arrayOfLong2.length - 1] + l & 0xFFFFFFFFL;
      l4 = arrayOfLong2[arrayOfLong2.length - 1];
    } 
    byte[] arrayOfByte2 = a(arrayOfLong2, 0, arrayOfLong2.length);
    paramOutputStream.write(arrayOfByte2);
  }
  
  private static void a(OutputStream paramOutputStream, byte[] paramArrayOfbyte) {
    paramOutputStream.write(paramArrayOfbyte);
  }
  
  private static long rotateLeft(long paramLong, int paramInt) {
    long l = (long)Math.pow(2.0D, (32 - paramInt)) - 1L;
    return (paramLong & l) << paramInt | paramLong >>> 32 - paramInt;
  }
  
  private static r a(InputStream paramInputStream, int paramInt) {
    byte[] arrayOfByte1 = new byte[104];
    if (paramInputStream.read(arrayOfByte1) != arrayOfByte1.length)
      throw new IOException("Invalid metadata file. Expected a file of size 104"); 
    long[] arrayOfLong1 = a(arrayOfByte1);
    long l1 = (paramInt + 2) ^ 0x1422CB8CL;
    long l2 = rotateLeft(l1, 13) * 5L + 3864292196L & 0xFFFFFFFFL;
    byte[] arrayOfByte2 = "NAESEVADNAYRTNRG".getBytes("US-ASCII");
    for (byte b1 = 0; b1 < 4; b1++)
      arrayOfByte2[b1] = (byte)(int)(l2 >> b1 * 8); 
    long[] arrayOfLong2 = a(arrayOfByte2);
    long l3 = -239350328L;
    for (byte b2 = 0; b2 < 8; b2++) {
      int i = (int)(l3 >>> 2L & 0x3L);
      long l4 = arrayOfLong1[0];
      int j = arrayOfLong1.length - 1;
      long l5 = 0L;
      for (byte b = 25; b > 0; b--) {
        l5 = l4 >> 3L ^ (arrayOfLong1[j - 1] & 0xFFFFFFFL) << 4L;
        l5 += l4 * 4L & 0xFFFFFFFFL ^ arrayOfLong1[j - 1] >> 5L;
        l5 ^= (arrayOfLong1[j - 1] ^ arrayOfLong2[b & 0x3 ^ i]) + (l4 ^ l3);
        arrayOfLong1[j] = arrayOfLong1[j] - l5 & 0xFFFFFFFFL;
        l4 = arrayOfLong1[j--];
      } 
      j = arrayOfLong1.length - 1;
      l5 = l4 >> 3L ^ (arrayOfLong1[j] & 0xFFFFFFFL) << 4L;
      l5 += l4 * 4L & 0xFFFFFFFFL ^ arrayOfLong1[j] >> 5L;
      l5 ^= (arrayOfLong1[j] ^ arrayOfLong2[i]) + (l4 ^ l3);
      arrayOfLong1[0] = arrayOfLong1[0] - l5 & 0xFFFFFFFFL;
      l3 += 1640531527L;
    } 
    if (arrayOfLong1[0] != 4008636094L)
      throw new IOException("Invalid metadata header: " + Long.toHexString(arrayOfLong1[0])); 
    r r = new r(paramInt);
    r.it = (int)arrayOfLong1[1];
    if (r.it != 2000 && r.it != 2001)
      throw new IOException("Invalid or unsupported format in metadata header"); 
    r.iv = new long[] { (arrayOfLong1[3] & 0xFFFFFFFFL) << 32L | arrayOfLong1[2] & 0xFFFFFFFFL, (arrayOfLong1[5] & 0xFFFFFFFFL) << 32L | arrayOfLong1[4] & 0xFFFFFFFFL };
    r.iu = a(arrayOfLong1, 6, 8);
    return r;
  }
  
  private static long[] a(byte[] paramArrayOfbyte) {
    long[] arrayOfLong = new long[paramArrayOfbyte.length / 4];
    for (byte b = 0; b < paramArrayOfbyte.length; b += 4)
      arrayOfLong[b / 4] = paramArrayOfbyte[b] & 0xFFL | (paramArrayOfbyte[b + 1] & 0xFFL) << 8L | (paramArrayOfbyte[b + 2] & 0xFFL) << 16L | (paramArrayOfbyte[b + 3] & 0xFFL) << 24L; 
    return arrayOfLong;
  }
  
  private static byte[] a(long[] paramArrayOflong, int paramInt1, int paramInt2) {
    byte[] arrayOfByte = new byte[paramInt2 * 4];
    for (byte b = 0; b < paramInt2; b++) {
      arrayOfByte[b * 4] = (byte)(int)(paramArrayOflong[paramInt1 + b] & 0xFFL);
      arrayOfByte[b * 4 + 1] = (byte)(int)(paramArrayOflong[paramInt1 + b] >> 8L & 0xFFL);
      arrayOfByte[b * 4 + 2] = (byte)(int)(paramArrayOflong[paramInt1 + b] >> 16L & 0xFFL);
      arrayOfByte[b * 4 + 3] = (byte)(int)(paramArrayOflong[paramInt1 + b] >> 24L & 0xFFL);
    } 
    return arrayOfByte;
  }
  
  private static byte[] a(r paramr, InputStream paramInputStream, Object paramObject) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte1 = new byte[1024];
    int i;
    while ((i = paramInputStream.read(arrayOfByte1)) >= 0)
      byteArrayOutputStream.write(arrayOfByte1, 0, i); 
    arrayOfByte1 = byteArrayOutputStream.toByteArray();
    MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
    byte[] arrayOfByte2 = messageDigest.digest(arrayOfByte1);
    if (paramr.iu.length != arrayOfByte2.length)
      throw new IOException("Invalid sha256 length"); 
    for (byte b1 = 0; b1 < arrayOfByte2.length; b1++) {
      if (paramr.iu[b1] != arrayOfByte2[b1])
        throw new IOException("Invalid sha256 value[" + b1 + "]" + Integer.toHexString(paramr.iu[b1] & 0xFF) + "=" + Integer.toHexString(arrayOfByte2[b1] & 0xFF)); 
    } 
    if (paramr.it == 2000)
      throw new IOException("Cannot decrypt version 1.0 files"); 
    long[] arrayOfLong = a(paramr.iu, arrayOfByte1);
    for (byte b2 = 0; b2 < arrayOfLong.length; b2++) {
      if (paramr.iv[b2] != arrayOfLong[b2])
        throw new IOException("Invalid key value[" + b2 + "]" + Long.toHexString(paramr.iv[b2]) + "=" + Long.toHexString(arrayOfLong[b2])); 
    } 
    return arrayOfByte1;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\storage\q.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */