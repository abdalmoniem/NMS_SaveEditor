package nomanssave.storage;

import java.io.File;
import java.util.ArrayList;
import nomanssave.Application;
import nomanssave.util.n;

class f implements o {
  private final int fZ;
  
  private f(b paramb, int paramInt) {
    this.fZ = paramInt;
  }
  
  public boolean G(String paramString) {
    switch (this.fZ) {
      case 0:
        return paramString.equals("save.hg") ? true : (paramString.equals("save2.hg") ? true : ((paramString.startsWith("backup.") && paramString.endsWith(".zip")) ? true : ((paramString.startsWith("backup2.") && paramString.endsWith(".zip")))));
    } 
    int i = this.fZ * 2;
    return paramString.equals("save" + (i + 1) + ".hg") ? true : (paramString.equals("save" + (i + 2) + ".hg") ? true : ((paramString.startsWith("backup" + (i + 1) + ".") && paramString.endsWith(".zip")) ? true : ((paramString.startsWith("backup" + (i + 2) + ".") && paramString.endsWith(".zip")))));
  }
  
  public boolean F(String paramString) {
    switch (this.fZ) {
      case 0:
        return paramString.equals("save.hg") ? (((new File(b.a(this.ih), "save.hg")).lastModified() > (new File(b.a(this.ih), "save2.hg")).lastModified())) : (paramString.equals("save2.hg") ? (((new File(b.a(this.ih), "save2.hg")).lastModified() > (new File(b.a(this.ih), "save.hg")).lastModified())) : false);
    } 
    int i = this.fZ * 2;
    return paramString.equals("save" + (i + 1) + ".hg") ? (((new File(b.a(this.ih), "save" + (i + 1) + ".hg")).lastModified() > (new File(b.a(this.ih), "save" + (i + 2) + ".hg")).lastModified())) : (paramString.equals("save" + (i + 2) + ".hg") ? (((new File(b.a(this.ih), "save" + (i + 2) + ".hg")).lastModified() > (new File(b.a(this.ih), "save" + (i + 1) + ".hg")).lastModified())) : false);
  }
  
  public p[] aV() {
    n.info("Loading saves for Slot " + (this.fZ + 1) + "...");
    ArrayList arrayList = new ArrayList();
    if (this.fZ == 0) {
      b.a(this.ih).listFiles(new g(this, arrayList));
      b.b(this.ih).listFiles(new h(this, arrayList));
    } else {
      int i = this.fZ * 2;
      b.a(this.ih).listFiles(new i(this, i, arrayList));
      b.b(this.ih).listFiles(new j(this, i, arrayList));
    } 
    arrayList.sort(new k(this));
    return (p[])arrayList.toArray((Object[])new p[0]);
  }
  
  public a x() {
    File file1;
    File file2;
    if (this.fZ == 0) {
      file1 = new File(b.a(this.ih), "save.hg");
      file2 = new File(b.a(this.ih), "save2.hg");
    } else {
      int i = this.fZ * 2;
      file1 = new File(b.a(this.ih), "save" + (i + 1) + ".hg");
      file2 = new File(b.a(this.ih), "save" + (i + 2) + ".hg");
    } 
    a a;
    return (file1.exists() && (a = b.f(file1)) != null) ? a : ((file2.exists() && (a = b.f(file2)) != null) ? a : null);
  }
  
  public String toString() {
    File file1;
    File file2;
    if (this.fZ == 0) {
      file1 = new File(b.a(this.ih), "save.hg");
      file2 = new File(b.a(this.ih), "save2.hg");
    } else {
      int i = this.fZ * 2;
      file1 = new File(b.a(this.ih), "save" + (i + 1) + ".hg");
      file2 = new File(b.a(this.ih), "save" + (i + 2) + ".hg");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Slot " + (this.fZ + 1) + " - ");
    a a;
    if (file1.exists() && (a = b.f(file1)) != null) {
      stringBuilder.append(a.toString());
      stringBuilder.append(" - " + Application.b(file1.lastModified()));
    } else if (file2.exists() && (a = b.f(file2)) != null) {
      stringBuilder.append(a.toString());
      stringBuilder.append(" - " + Application.b(file2.lastModified()));
    } else {
      stringBuilder.append("[EMPTY]");
    } 
    return stringBuilder.toString();
  }
  
  private void aW() {
    File file1;
    File file2;
    File file3;
    File file4;
    if (this.fZ == 0) {
      boolean bool = false;
      file1 = new File(b.a(this.ih), "save.hg");
      file2 = new File(b.a(this.ih), "save2.hg");
      file3 = new File(b.a(this.ih), "mf_save.hg");
      file4 = new File(b.a(this.ih), "mf_save2.hg");
    } else {
      int i = this.fZ * 2;
      file1 = new File(b.a(this.ih), "save" + (i + 1) + ".hg");
      file2 = new File(b.a(this.ih), "save" + (i + 2) + ".hg");
      file3 = new File(b.a(this.ih), "mf_save" + (i + 1) + ".hg");
      file4 = new File(b.a(this.ih), "mf_save" + (i + 2) + ".hg");
    } 
    if (file1.exists()) {
      n.info("  deleted: " + file1.getName());
      file1.delete();
    } 
    if (file2.exists()) {
      n.info("  deleted: " + file2.getName());
      file2.delete();
    } 
    if (file3.exists()) {
      n.info("  deleted: " + file3.getName());
      file3.delete();
    } 
    if (file4.exists()) {
      n.info("  deleted: " + file4.getName());
      file4.delete();
    } 
    File[] arrayOfFile = b.b(this.ih).listFiles(new l(this));
    for (byte b1 = 0; b1 < arrayOfFile.length; b1++) {
      n.info("  deleted: " + arrayOfFile[b1].getName());
      arrayOfFile[b1].delete();
    } 
  }
  
  public String H(String paramString) {
    File file1;
    File file2;
    File file3;
    File file4;
    int i = this.fZ * 2;
    if (this.fZ == 0) {
      file1 = new File(b.a(this.ih), "save.hg");
      file2 = new File(b.a(this.ih), "mf_save.hg");
      file3 = new File(b.a(this.ih), "save2.hg");
      file4 = new File(b.a(this.ih), "mf_save2.hg");
    } else {
      file1 = new File(b.a(this.ih), "save" + (i + 1) + ".hg");
      file2 = new File(b.a(this.ih), "mf_save" + (i + 1) + ".hg");
      file3 = new File(b.a(this.ih), "save" + (i + 2) + ".hg");
      file4 = new File(b.a(this.ih), "mf_save" + (i + 2) + ".hg");
    } 
    long l = System.currentTimeMillis();
    if (file1.exists()) {
      String str;
      n.info("Creating backup1...");
      if (i == 0) {
        str = "backup." + l + ".zip";
      } else {
        str = "backup" + (i + 1) + "." + l + ".zip";
      } 
      b.a(this.ih, i, file1, file2, str);
      l++;
    } 
    if (file3.exists()) {
      n.info("Creating backup2...");
      String str = "backup" + (i + 2) + "." + l + ".zip";
      b.a(this.ih, i + 1, file3, file4, str);
      file3.delete();
      file4.delete();
    } 
    n.info("Writing new save file...");
    b.b(this.ih, i, file1, file2, paramString);
    n.info("Finished.");
    return file1.getName();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\storage\f.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */