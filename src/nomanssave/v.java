package nomanssave;

import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListDataListener;
import nomanssave.storage.o;

class v implements ComboBoxModel {
  o aq = null;
  
  v(Application paramApplication) {}
  
  public int getSize() {
    return (Application.k(this.am) == null) ? 0 : Application.k(this.am).aU();
  }
  
  public o i(int paramInt) {
    return Application.k(this.am).J(paramInt);
  }
  
  public void addListDataListener(ListDataListener paramListDataListener) {}
  
  public void removeListDataListener(ListDataListener paramListDataListener) {}
  
  public void setSelectedItem(Object paramObject) {
    if (Application.i(this.am)) {
      Application.a(this.am).hidePopup();
      int i = JOptionPane.showConfirmDialog(Application.e(this.am), "Save data before switching slots?", "Save", 1);
      if (i == 0) {
        Application.j(this.am);
      } else {
        if (i == 2)
          return; 
        Application.a(this.am, false);
      } 
    } 
    this.aq = (o)paramObject;
    Application.n(this.am);
  }
  
  public Object getSelectedItem() {
    return this.aq;
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\v.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */