package nomanssave;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import nomanssave.db.a;
import nomanssave.structures.i;

public class bx extends JPanel implements dI {
  private static final int cP = 50;
  
  private final Application cQ;
  
  private JTable cR;
  
  private JButton cS;
  
  private JButton cT;
  
  private bj cU;
  
  private ab cV;
  
  private JComboBox cW;
  
  private JTextField cX;
  
  private JComboBox cY;
  
  private ab cZ;
  
  private ab da;
  
  private ab db;
  
  private ab dc;
  
  private ab dd;
  
  private ab de;
  
  private ab df;
  
  private ab dg;
  
  private ab dh;
  
  private bj di;
  
  private JComboBox dj;
  
  private JComboBox dk;
  
  private JComboBox dl;
  
  private JComboBox dm;
  
  private JComboBox dn;
  
  private ab do;
  
  private ab dp;
  
  private ab dq;
  
  private ab dr;
  
  private JLabel ds;
  
  private JButton dt;
  
  private a[] du;
  
  private i[] dv;
  
  private int dw;
  
  private static final Color dx = Color.BLUE;
  
  private static final Color dy = new Color(0, 128, 255);
  
  private static final Color dz = Color.RED;
  
  private static final Color dA = new Color(255, 100, 100);
  
  bx(Application paramApplication) {
    this.cQ = paramApplication;
    GridLayout gridLayout = new GridLayout(1, 3);
    setLayout(gridLayout);
    JScrollPane jScrollPane = new JScrollPane();
    jScrollPane.setMinimumSize(new Dimension(300, 0));
    jScrollPane.setMaximumSize(new Dimension(300, 2147483647));
    jScrollPane.setPreferredSize(new Dimension(300, 0));
    JPanel jPanel1 = new JPanel();
    jPanel1.setLayout(new BorderLayout());
    jPanel1.add(jScrollPane, "Center");
    JPanel jPanel2 = new JPanel();
    this.cS = new JButton("Delete");
    this.cS.setEnabled(false);
    this.cS.addActionListener(new by(this, paramApplication));
    jPanel2.add(this.cS);
    this.cT = new JButton("Copy");
    this.cT.setEnabled(false);
    this.cT.addActionListener(new bJ(this, paramApplication));
    jPanel2.add(this.cT);
    jPanel1.add(jPanel2, "South");
    add(jPanel1);
    this.cR = new JTable();
    this.cR.setShowHorizontalLines(false);
    this.cR.setShowVerticalLines(false);
    this.cR.setFont(aO.bG);
    this.cR.setRowHeight(aO.bJ);
    this.cR.setSelectionMode(0);
    this.cR.setModel(new bN(this));
    this.cR.getColumnModel().getColumn(2).setMaxWidth(60);
    this.cR.getSelectionModel().addListSelectionListener(new bO(this, paramApplication));
    jScrollPane.setViewportView(this.cR);
    this.cU = new bj();
    this.cU.setVisible(false);
    add(this.cU);
    this.cU.n("Frigate Info");
    this.cV = new bQ(this, paramApplication);
    this.cU.a("Name", this.cV);
    this.cW = new JComboBox();
    this.cW.setModel(new bR(this, paramApplication));
    this.cU.a("Type", this.cW);
    this.cX = new JTextField();
    this.cX.setEditable(false);
    this.cU.a("Class", this.cX);
    this.cY = new JComboBox();
    this.cY.setModel(new bS(this, paramApplication));
    this.cU.a("NPC Race", this.cY);
    this.cZ = new bT(this, paramApplication);
    this.cU.a("Home Seed", this.cZ);
    this.da = new bU(this, paramApplication);
    this.cU.a("Model Seed", this.da);
    this.cU.n("Totals");
    this.do = new bz(this, paramApplication);
    this.cU.a("Expeditions", this.do);
    this.dp = new bA(this, paramApplication);
    this.cU.a("Successful", this.dp);
    this.dq = new bB(this, paramApplication);
    this.cU.a("Failed", this.dq);
    this.dr = new bC(this, paramApplication);
    this.cU.a("Damaged", this.dr);
    this.cU.Q();
    JPanel jPanel3 = new JPanel();
    jPanel3.setLayout((LayoutManager)new FormLayout(new ColumnSpec[] { FormFactory.GLUE_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.GLUE_COLSPEC }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC }));
    this.ds = new JLabel("");
    jPanel3.add(this.ds, "2,1");
    this.dt = new JButton("Repair");
    this.dt.addActionListener(new bD(this, paramApplication));
    JPanel jPanel4 = new JPanel();
    jPanel4.add(this.dt);
    jPanel3.add(jPanel4, "2,2");
    this.cU.a(jPanel3);
    this.di = new bj();
    this.di.setVisible(false);
    add(this.di);
    this.di.n("Stats");
    this.db = new bE(this, paramApplication);
    this.di.a("Combat", this.db);
    this.dc = new bF(this, paramApplication);
    this.di.a("Exploration", this.dc);
    this.dd = new bG(this, paramApplication);
    this.di.a("Mining", this.dd);
    this.de = new bH(this, paramApplication);
    this.di.a("Diplomacy", this.de);
    this.df = new bI(this, paramApplication);
    this.di.a("Fuel Burn Rate", this.df);
    this.dg = new bK(this, paramApplication);
    this.di.a("Fuel Capacity", this.dg);
    this.dh = new bL(this, paramApplication);
    this.di.a("Speed", this.dh);
    this.di.n("Traits");
    a[] arrayOfA = a.aB();
    this.dj = new JComboBox();
    this.dj.setEnabled(false);
    this.dj.setModel(new bM(this, arrayOfA));
    this.di.a(this.dj);
    bW bW = new bW(this, null);
    this.dk = new JComboBox();
    this.dk.setModel(new bV(this, 1));
    this.dk.setRenderer(bW);
    this.di.a(this.dk);
    this.dl = new JComboBox();
    this.dl.setModel(new bV(this, 2));
    this.dl.setRenderer(bW);
    this.di.a(this.dl);
    this.dm = new JComboBox();
    this.dm.setModel(new bV(this, 3));
    this.dm.setRenderer(bW);
    this.di.a(this.dm);
    this.dn = new JComboBox();
    this.dn.setModel(new bV(this, 4));
    this.dn.setRenderer(bW);
    this.di.a(this.dn);
    dH.a(this);
  }
  
  public void a(boolean paramBoolean) {
    if (this.cR.getSelectedRow() >= 0)
      this.cT.setEnabled(!(this.dv.length >= 30 && !dH.aw())); 
  }
  
  void a(i[] paramArrayOfi) {
    this.dv = paramArrayOfi;
    this.du = null;
    if (paramArrayOfi.length > 0) {
      this.cR.setRowSelectionInterval(0, 0);
    } else {
      this.cR.clearSelection();
    } 
    this.cR.updateUI();
  }
}


/* Location:              C:\Users\hifna\Desktop\NMS Save Editor\NMSSaveEditor.jar!\nomanssave\bx.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */